<?php
if(CONNECT_FORCED_HTTPS == true){
	if(!$_SERVER['HTTPS'] || $_SERVER['HTTPS'] == 'off'){
		header('HTTP/1.1 301 Moved Permanently');
		header('Location: '.URL);
		exit();
	}
}