<?php

class adminpanel extends Plugin {

    public $library = array(cacheLib);

    function run($arguments) {
        $account = null;
        if (cluAccount::getBooleanConnect()) {
            $account = cluAccount::getAccount();
            if ($account->getBooleanGroup("ADM")) {
                if (isset($arguments['action'])) {
                    cluServer::$data->setName('Панель администратора');
                    if ($arguments['action'] == 'users') {
                        $mysql = $account->umysql;
                        $results = $mysql->prepare("SELECT * FROM users");
                        $results->execute();
                        $results = $results->fetchAll(PDO::FETCH_ASSOC);
                        if ($results) {
                            cluServer::$data->render('admin/userlist', ['model' => $results]);
                        }
                    } else if ($arguments['action'] == 'jscreative') {
                        cluServer::$data->render('admin/jscreative');
                    } else if ($arguments['action'] == 'banlist') {
                        $mysql = $account->umysql;
                        $results = $mysql->prepare("SELECT users.id as usersid,users.banid,users.name,banned.id as bannedid,banned.id_user,banned.id_moder,banned.reason,banned.permanet FROM banned,users WHERE banned.id_user = users.id OR banned.id_moder = users.id");
                        $results->execute();
                        $results = $results->fetchAll(PDO::FETCH_ASSOC);
                        echo 'Список банов:<br>';
                        if ($results) {
                            for ($i = 0; $i < count($results); $i += 2) {
                                if ($results[$i + 1]['banid'] == $results[$i + 1]['bannedid'])
                                    echo '<font color=green>';
                                echo '<a href="?type=showprofile&userid=' . $results[$i]['id_moder'] . '">' . $results[$i]['name'] . '</a> забанил <a href="?type=showprofile&userid=' . $results[$i + 1]['id_user'] . '">' . $results[$i + 1]['name'] . '</a>';
                                if ($results[$i]['reason'])
                                    echo ' по причине "' . $results[$i]['reason'] . '"';
                                if ($results[$i]['permanet'])
                                    echo ' навсегда';
                                if ($results[$i + 1]['banid'] == $results[$i + 1]['bannedid'])
                                    echo '(действителен)</font>';
                                else
                                    echo "(Не действителен)";
                                echo '<br>';
                            }
                        }
                        else {
                            echo 'Записей в таблице банов нет.<br>';
                        }
                        echo '<a href="?type=adminpanel">Назад</a>';
                    } else if ($arguments['action'] == 'adminlog') {
                        $mysql = $account->umysql;
                        $results = $mysql->prepare("SELECT users.id as usersid,users.name,adminlog.id as adminlogid,adminlog.id_user,adminlog.id_admin,adminlog.text,adminlog.actionid,adminlog.dtime FROM adminlog,users WHERE adminlog.id_user = users.id OR adminlog.id_admin = users.id");
                        $results->execute();
                        $results = $results->fetchAll(PDO::FETCH_ASSOC);
                        echo 'Действия администраторов:<br>';
                        if ($results) {
                            for ($i = 0; $i < count($results); $i += 1) {
                                if ($results[$i]['id_admin'] != $results[$i]['id_user']) {
                                    echo '<a href="?type=showprofile&userid=' . $results[$i]['id_admin'] . '">' . $results[$i + 1]['name'] . '</a> ' . $results[$i]['text'] . ' <a href="?type=showprofile&userid=' . $results[$i + 1]['id_user'] . '">' . $results[$i]['name'] . '</a>. Дата: "' . $results[$i]['dtime'] . '". Действие: ' . $results[$i]['actionid'] . '<br>';
                                    $i += 1;
                                } else {
                                    echo '<a href="?type=showprofile&userid=' . $results[$i]['id_admin'] . '">' . $results[$i]['name'] . '</a> ' . $results[$i]['text'] . ' <a href="?type=showprofile&userid=' . $results[$i]['id_user'] . '">' . $results[$i]['name'] . '</a>. Дата: "' . $results[$i]['dtime'] . '". Действие: ' . $results[$i]['actionid'] . '<br>';
                                }
                            }
                        } else {
                            echo 'Записей в таблице действий администраторов нет.<br>';
                        }
                        echo '<a href="?type=adminpanel">Назад</a>';
                    } else if ($arguments['action'] == 'phpinfo') {
                        if ($account->getBooleanGroup("SUPERUSER"))
                            echo phpinfo();
                        else
                            echo 'Доступ к информации о сервере имеет только суперпользователь!';
                    } else if ($arguments['action'] == 'cache') {
                        cluServer::$data->render('admin/cache');
                    } else {
                        echo 'Вы попали на страницу, которая не существует или не реализована.<br>
                            <a href="?type=adminpanel">Назад</a>';
                    }
                } else {
                    cluServer::$data->setName('Панель администратора');
                    cluServer::$data->render('admin/index');
                }
            } else {
                cluServer::$data->setName('Панель администратора');
                print403Error();
            }
        } else {
            cluServer::$data->setName('Панель администратора');
            print403Error();
        }
    }

}
