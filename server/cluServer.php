<?php

// #ULIN PROJECT
// #ULIN TECH
//
//
class Action {
    
}

class Plugin {

    public $library;
    public $vlibrary;
    public $name;
    public $args;
    public $checkArguments;
    public $checkArgumentsNoNull;
    public $flags;
    public $space;

    function run($arguments) {
        
    }

    function stop() {
        
    }

}

class cluServer {

    public static $plugin;
    public static $account;
    private static $libs;
    public static $connect;
    public static $data;

    public static function loadPlugin($space, $namePlugin, $file, $arguments = null, $loadRun = true) {
        try {
            if ($file != null) {
                $booleanLoad = false;
                if (@include($file)) {
                    $booleanLoad = true;
                }
                if ($booleanLoad == false) {
                    //createCrash('cluServer')->setMain('notLoadPlugin')->notExistsPluginFile($file);
                    return null;
                    //ЛОМАЕТСЯ АРХИТЕКТУРА
                }
            }
            //ГРУЖЕНО
            if (method_exists($namePlugin, 'run')) {
                cluServer::$plugin = new $namePlugin();
            } else {
                    return null;
                    //НЕ ОБНАРУЖЕНО ВХОДА
                //
				
            }
            //КЛАСС ОБНАРУЖЕН И ЗАГРУЖЕН
            //ЗАЩИТА CONSOLE ПЛАГИНОВ
            if ($space == PASC) {
                //CONSOLE
                //
				//if($arguments){ уязвимость, плагин без аргументов может выполнится и его пропустят, даже если плагин этого не желал.
                //уязвимость работает только в консольном пространстве и серверском, так как визуальные плагины не пользуются обязательныеми аргументами
                if (cluServer::$plugin->checkArguments) {
                    foreach (cluServer::$plugin->checkArguments as $v) {
                        if (isset($arguments[$v]) == false) {
                            createCrash('cluServer')->setMain('notLoadPlugin')->notExistsArgumentForPlugin($v);
                            return null;
                        }
                    }
                    unset(cluServer::$plugin->checkArguments);
                }
                if (cluServer::$plugin->checkArgumentsNoNull) {
                    foreach (cluServer::$plugin->checkArgumentsNoNull as $v) {
                        if (isset($arguments[$v]) == false || empty($arguments[$v])) {
                            createCrash('cluServer')->setMain('notLoadPlugin')->notExistsArgumentOrArgumentNullForPlugin($v);
                            return null;
                        }
                    }
                    unset(cluServer::$plugin->checkArgumentsNoNull);
                }
            }
            //
            //
			//GLOBAL ARGUMENTS
            cluServer::$plugin->name = $namePlugin;
            cluServer::$plugin->args = &$arguments;
            cluServer::$plugin->space = $space;
            if (cluServer::$plugin->library) {
                if (is_array(cluServer::$plugin->library)) {
                    foreach (cluServer::$plugin->library as $v) {
                        cluServer::loadLibrary($v);
                    }
                } else {
                    cluServer::loadLibrary(cluServer::$plugin->library);
                }
                unset(cluServer::$plugin->library);
            }
            $autoLoadSpace = true;
            if (cluServer::$plugin->flags) {
                foreach (cluServer::$plugin->flags as $name => $v) {
                    if ($name == 'autoLoadSpace') {
                        $autoLoadSpace = $v;
                        continue;
                    }
                }
            }
            cluServer::$data == null;
            if ($autoLoadSpace) {
                cluServer::initData();
            }
            if ($loadRun) {
                cluServer::$plugin->run($arguments);
            }
            return cluServer::$plugin;
        } catch (Exception $e) {
            createCrash('cluServer')->setMain('notLoadPlugin')->pluginException($e);
        } catch (cluCrash $c) {
            createCrash($c)->setMain(array('pluginException', 'notLoadPlugin'))->main();
        }
    }

    static function LoadAction($name = 'index') {
        $file = HomeDirectory . DirAction . $name . '.php';
        try {
            $load = @include($file);
            return true;
        } catch (Exception $e) {
            
        }
        createCrash('cluServer')->notLoadedLibrary($name);
        return false;
    }

    public static function initData(...$args) {
        if (cluServer::$data == null) {
            if (cluServer::$plugin->space == PASC) {
                include(HomeDirectory . DirSpaces . LIB_SPACE_CONSOLE . '.php');
                cluServer::$data = new consoleData();
                cluServer::$data->initData(...$args);
                return cluServer::$data;
            }
            if (cluServer::$plugin->space == PASS) {
                include(HomeDirectory . DirSpaces . LIB_SPACE_SERVER . '.php');
                cluServer::$data = new consoleData();
                cluServer::$data->initData(...$args);
                return cluServer::$data;
            }
            if (cluServer::$plugin->space == PASV) {
                include(HomeDirectory . DirSpaces . LIB_SPACE_VISIBLE . '.php');
                cluServer::$data = new visibleData();
                cluServer::$data->initData(...$args);
                return cluServer::$data;
            }
        }
        //return cluServer::$data;
        return false;
    }

    public static function stop(&$thread = null) {
        //if(isset($GLOBALS['MAILINIT'])){
        //	closeMail();
        //}
        $result = null;
        if (cluServer::$data) {
            $result = cluServer::$data->createData();
        }
        if (COMPRESS == true) {
            if ($result) {
                if (isset($_COOKIE['COMPRESSTYPE']) || isset($_SERVER['HTTP_ACCEPT_ENCODING'])) {
                    $compresstype = null;
                    loadLibrary(COMPRESS_LIBRARY, false);
                    if (isset($_COOKIE['COMPRESSTYPE'])) {
                        $compresstype = $_COOKIE['COMPRESSTYPE'];
                    }
                    if (empty($compresstype) || compress::getBooleanMod($arguments['COMPRESSTYPE']) == false) {
                        if (isset($_SERVER['HTTP_ACCEPT_ENCODING'])) {
                            $t = explode(', ', $_SERVER['HTTP_ACCEPT_ENCODING']);
                            foreach ($t as $v) {
                                if (compress::getBooleanMod($v)) {
                                    $compresstype = $v;
                                    break;
                                }
                            }
                        }
                        if (empty($compresstype)) {
                            $compresstype = COMPRESSTYPE;
                        }
                    }
                    $result = (new compress($compresstype, COMPRESSLVL, false))->compress($result);
                    if (empty($result) == false) {
                        header('Content-Encoding: ' . $compresstype);
                        header('Vary: Accept-Encoding');
                    }
                }
            }
        }
        if ($thread == null) {
            if (empty($result) == false) {
                exit($result);
            }
            exit();
        }
        if (empty($result) == false) {
            return $result;
        }
        return null;
    }

    public static function setVisibleErrors($bool, $notice = false) {
        if ($bool) {
            ini_set('display_errors', 'On');
            ini_set('display_startup_errors', 'On');
            if ($notice) {
                error_reporting(E_ALL & ~E_STRICT);
            } else {
                error_reporting(E_ALL & ~E_NOTICE & ~E_STRICT);
            }
        } else {
            ini_set('display_errors', 'Off');
            ini_set('display_startup_errors', 'Off');
        }
    }

    public static function getAllLibrary() {
        return cluServer::$libs;
    }

    public static function booleanLoadLibrary($value) {
        if (cluServer::$libs) {
            foreach (cluServer::$libs as $v) {
                if ($v == $value)
                    return true;
            }
        }
        return false;
    }

    public static function loadLibrary($name, $crasher = RUN_CRASH_TEST_PLUGIN == true) {
        if (TEST_CPLUGIN == true) {
            if (cluServer::booleanLoadLibrary($name)) {
                if ($crasher) {
                    createCrash('cluServer')->setMain('notLoadLibrary')->libraryAlreadyConnect($name);
                }
                return false;
            }
        }
        $url = HomeDirectory . DirLibrary . $name . '.php';
        try {
            if (@include($url)) {
                cluServer::$libs[] = $name;
                return true;
            }
        } catch (Exception $e) {
            
        }
        createCrash('cluServer')->notLoadLibrary($name);
        return false;
    }

    private static $date = null;

    public static function getDate() {
        if (cluServer::$date == null) {
            cluServer::$date = date('Y-m-d H:i:s');
        }
        return cluServer::$date;
    }

}

function createCrash($nameLib = null) {
    if ($nameLib == null)
        $nameLib = $GLOBALS['PLUGINACTIV'];
    $load = false;
    try {
        if (@include(HomeDirectory . DirCrasher . $nameLib . '.php')) {
            $load = true;
        }
    } catch (Exception $e) {
        
    }
    if ($load == false) {
        try {
            if (@include(HomeDirectory . DirCrasher . 'main' . '.php')) {
                $load = true;
            }
        } catch (Exception $e) {
            
        }
    }
    if ($load == false) {
        exit('CRASHER = NULL, not isset /crash/main.php');
        //возможность зацикла
        return null;
    }
    try {
        $object = new crash();
        $visible = null;
        if ($object->library) {
            if (is_array($object->library)) {
                foreach ($object->library as $v) {
                    cluServer::loadLibrary($v);
                }
            } else {
                cluServer::loadLibrary($object->library);
            }
            unset($object->library);
        }
        $dontRunStopInterpretator = false;
        if (isset($object->dontRunStopInterpretator)) {
            if ($object->dontRunStopInterpretator == true) {
                $dontRunStopInterpretator = true;
            } else {
                $dontRunStopInterpretator = false;
            }
            unset($object->dontRunStopInterpretator);
        }
        if (cluServer::$plugin->space == PASV) {
            if (isset($object->vlibrary)) {
                if (cluServer::$data != null) {
                    if (is_array($object->vlibrary)) {
                        foreach ($object->vlibrary as $v) {
                            cluServer::$data->loadLibrary($v);
                        }
                    } else {
                        cluServer::loadLibrary($object->vlibrary);
                    }
                }
                unset($object->vlibrary);
            }
            if (isset($object->visible)) {
                $object->visible = $visible;
            }
            if (method_exists($object, 'vInit'))
                $object->vInit();
        }else {
            if (method_exists($object, 'init'))
                $object->init();
        }
        return new crashBuilder($object, $visible, $dontRunStopInterpretator);
    } catch (Exception $e) {
        exit('CRASHER = NULL, error object ' . $nameLib . ' or main');
        //возможность зацикла
        return null;
    }
    return null;
}

class cluCrash extends Exception {

    public function __construct($name = null) {
        
    }

}

class cluConnect {

    public $ip;
    private $ipVersion;
    private $isLocalhost;

    const IPV4 = 'IPV4';
    const IPV6 = 'IPV6';

    function __construct($ip) {
        $this->ip = $ip;
    }

    public function getIpVersion() {
        if ($this->ipVersion == null) {
            if (filter_var($this->ip, FILTER_VALIDATE_IP, FILTER_FLAG_IPV4)) {
                $this->ipVersion = cluConnect::IPV4;
            } else {
                if (filter_var($this->ip, FILTER_VALIDATE_IP, FILTER_FLAG_IPV6)) {
                    $this->ipVersion = cluConnect::IPV6;
                } else {
                    $this->ipVersion = false;
                }
            }
        }
        return $this->ipVersion;
    }

    public function isValid() {
        return $this->getIpVersion() != false;
    }

    public function isInvalid() {
        return $this->getIpVersion() === false;
    }

    public function isLocalhost() {
        if ($this->isLocalhost == null) {
            if (php_sapi_name() === 'cli-server' || ($this->ip == '127.0.0.1' || $this->ip == '::1' || $this->ip == 'fe80::1')) {
                $this->isLocalhost = true;
            }
        }
        return $this->isLocalhost;
    }

    public static function createThisConnect($detectIpAuto = false) {
        if (cluServer::$connect == null) {
            cluServer::$connect = new cluConnect(cluConnect::getThisIp($detectIpAuto));
        }
        return cluServer::$connect;
    }

    private static function getThisIp($detectIpAuto = false) {
        if ($detectIpAuto || DETECT_IP == true) {
            if (isset($_SERVER['HTTP_CLIENT_IP']) && $_SERVER['HTTP_CLIENT_IP'])
                return $_SERVER['HTTP_CLIENT_IP'];
            if (isset($_SERVER['HTTP_X_FORWARDED_FOR']) && $_SERVER['HTTP_X_FORWARDED_FOR'])
                return $_SERVER['HTTP_X_FORWARDED_FOR'];
            return $_SERVER['REMOTE_ADDR'];
        }
        if (DETECT_IP == false) {
            if (DETECT_IP_HEADER_NOTER == true && (isset($_SERVER[DETECT_IP_HEADER]) == false || !$_SERVER[DETECT_IP_HEADER]))
                return cluConnect::getThisIp(true);
            return $_SERVER[DETECT_IP_HEADER];
        }
        return $_SERVER['REMOTE_ADDR'];
    }

}

class crashBuilder {

    private $crashLib;
    //Object crash
    private $addPrefix;
    //String addPrefix, vDefault
    private $dontRunStopInterpretator;
    //Boolean dont run stop
    private $visible;
    //Object visible, не используется, пока
    private $main;

    //String дополнительный main
    function __construct($crashLib, $visible, $dontRunStopInterpretator) {
        $this->crashLib = $crashLib;
        if (cluServer::$plugin->space == PASV) {
            $this->addPrefix = 'v';
        } else {
            $this->addPrefix = null;
        }
        $this->dontRunStopInterpretator = $dontRunStopInterpretator;
        $this->visible = $visible;
    }

    public function setMain($main) {
        $this->main = $main;
        return $this;
    }

    public function addMain($main) {
        $this->main[] = $main;
        return $this;
    }

    public function __call($name, $args) {
        //var_dump($args);
        //exit();
        $logger = new Logger();
        $logger->err('main:'.$this->main.' func:'.$name.' args:'.json_encode($args),'createCrash');
        $boolThisStop = $this->dontRunStopInterpretator == false || $closure == 'stop';
        if ($result = $this->load($this->crashLib, $name, $args) == null) {
            if ($this->main) {
                if (is_array($this->main)) {
                    foreach ($this->main as $v) {
                        if ($result = $this->load($this->crashLib, $v, $args) != null) {
                            break;
                        }
                    }
                } else {
                    $result = $this->load($this->crashLib, $this->main, $args);
                }
            }
            if ($result == null) {
                $result = $this->load($this->crashLib, 'main', $args);
                //if($result = $this->load($this->crashLib, 'main', $args) == null){
                //	//
                //}
            }
        }
        if ($result != null) {
            if ($boolThisStop) {
                cluServer::stop();
            }
        }
        return $this;
        //return call_user_func_array($this->{$closure}, $args);
    }

    private function load($object, $name, $args) {
        if ($this->addPrefix != null) {
            $name = $this->addPrefix . ucfirst($name);
        }
        if (method_exists($this->crashLib, $name)) {
            if (method_exists(cluServer::$plugin, 'onError')) {
                cluServer::$plugin->onError($name, $args);
            }
            if ($args) {
                call_user_func_array(array($this->crashLib, $name), $args);
            } else {
                $this->crashLib->{$name}();
            }
            return $this;
        }
        return null;
    }

}

//CLUSERVER
//
// #ULIN PROJECT 2017
// #ULIN TECH
//
?>
