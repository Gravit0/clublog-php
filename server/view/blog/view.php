<?php
$rating = $model['rating'];
$ratingcolor = 'black';
if ($rating > 0)
    $ratingcolor = 'green';
if ($rating < 0)
    $ratingcolor = 'red';
?>
<?php
function recursionComment($arr, $account, $id_user_post, $visible, $recval = 0) {
    foreach ($arr as $value) {
        if ($recval == 0) {
            ?>
            <div class="blogcomment"><div class="blogcommentuser">
                    <a href="?type=showprofile&userid=<?= $value['id_user'] ?>" style="color: #<?= $value['colord'] ?>;"><?= $value['name'] ?></a>
        <?php } else { ?>
                    <div class="blogcomment" style="margin-left:'<?= (10 + $recval * 10) ?>'px;"><div class="blogcommentuser">
                            <a href="?type=showprofile&userid=<?= $value['id_user'] ?>" style="color: #<?= $value['colord'] ?>;"><?= $value['name'] ?></a>';
                        <?php } ?>
                        <font class="commentid" style="display: none;">
                        <?= $value['id_comment'] ?>
                        </font>
                        <?php
                        if ($value['id_user'] == $id_user_post)
                            echo ' [Автор]';
                        if (cluServer::$account) {
                            if (cluServer::$account->getArray()['id'] == $value['id_user'])
                                echo '[Вы]';
                        }
                        if ($value['visible'] == 0) {
                            echo '[ожидает модерации]';
                            if ($visible == 0) {
                                ?>
                                <?= img_apirequest('ok.png', '?type=blogapi&command=comment_ok&id=' . $value['id_comment'], "blogaction nofloat"); ?>
                                <?=
                                img_apirequest_custom(array(
                                    'func' => 'api_deletecomment',
                                    'param' => $value['id_comment'],
                                    'imgurl' => 'del-w.png',
                                    'class' => "blogaction nofloat"));
                                ?>
                                <?php
                            }
                        }
                        if (cluServer::$account)
                            if ((cluServer::$account->getBooleanGroup('ADM') || $account->getBooleanGroup('MODER')) && $value['visible'] == 1) {
                                echo img_apirequest_custom(array(
                                    'func' => 'api_deletecomment',
                                    'param' => $value['id_comment'],
                                    'imgurl' => 'del-w.png',
                                    'class' => "blogaction nofloat"));
                            }
                        echo '</div><div class="blogcommenttext">' . $value['text'] . '</div></div>';
                        if (isset($value['parentObj'])) {
                            recursionComment($value['parentObj'], $account, $id_user_post, $visible, $recval + 1);
                        }
                    }
                    //endforeach;
                }
                ?>
                <div id="blog_header"><?= $model['header'] ?></div> 
                <?= $model['text'] ?>
                <div id=postpost>
                    <div id="blogautor">
                        Автор: <a style="color:#<?= $model['colord'] ?>; text-decoration: none;" href="?type=showprofile&userid=<?= $id_user_post ?>">
                            <?= $model['name'] ?>
                        </a>
                    </div>
                    <div class="blogpostrating">
                        <?=
                        img_apirequest_custom(array(
                            'func' => 'api_updaterating',
                            'param' => $page . ',true',
                            'imgurl' => 'up.png',
                            'class' => "blogaction"));
                        ?>
                        <font id="rating" style="color: <?= $ratingcolor ?>;"><?= $rating ?></font> 
                        <?=
                        img_apirequest_custom(array(
                            'func' => 'api_updaterating',
                            'param' => $page . ',false',
                            'imgurl' => 'down.png',
                            'class' => "blogaction")
                        );
                        ?>
                    </div>
                    <?php
                    if (cluServer::$account)
                    if (cluServer::$account->getBooleanGroup("ADM")) {
                        ?>
                        <div id=adminblock>
                            <?= img_apirequest('del.png', '?type=blogapi&command=post_rm&id=' . $page, "blogaction"); ?>
                        </div>
                    <?php } ?>
                </div>
                <div id="blogcommentblock">Комментарии (<font id="comment_num"><?= $comments_num ?></font>)
                    <?php recursionComment($comment, cluServer::$account, $id_user_post, $visible); ?>
                </div>
                <?php if (cluServer::$account) if (!cluServer::$account->getBooleanGroup('READONLY')) {
                    ?>
                    <br>Написать комментарий:<br><textarea id="newcomment" name="comment" cols="40" rows="3" required></textarea><br>
                    <input type="submit" onclick="api_sendcomment(' <?= $page ?> ')" value="Отправить">
                <?php } ?>
