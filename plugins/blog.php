<?php
function &recursionFindComment(&$arraycomment, $commentid) {
    $freearray = null;
    foreach ($arraycomment as &$value) {
        if (isset($value['parentObj'])) {
            $val = &recursionFindComment($value['parentObj'], $commentid);
            if ($val) {
                return $val;
            }
        }
        if ($value['id_comment'] == $commentid)
            return $value;
    }
    return $freearray;
}
function recursionCommentCreate($results) {
    $arraycomment = array();
    foreach ($results as $value) {
        if ($value['parent'] == 0) {
            $arraycomment[$value['id_comment']] = $value;
        } else {
            $recval = &recursionFindComment($arraycomment, $value['parent']);
            if ($recval) {
                $recval['parentObj'][$value['id_comment']] = $value;
            } else {
                $arraycomment[$value['parent']]['parentObj'][$value['id_comment']] = $value;
            }
        }
    }
    return $arraycomment;
}
const AllowBlogSandbox = false;
class blog extends Plugin {
    public $library = array();
    function run($arguments) {
        cluServer::$data->setName('Блог');
        cluServer::$data->LoadLibrary('blog');
        //LoadJavaScript("blog.js");
        if (!isset($arguments['limit']))
            $arguments['limit'] = 30;
        $account = null;
        if (cluAccount::getBooleanConnect()) {
            $account = cluAccount::getAccount();
            $mysql = $account->umysql;
        } else {
            $mysql = new cluMysql();
        }
        if(isset($arguments['r']))
        {
            if($arguments['r'] == 'newpost')
            {
                cluServer::$data->render('blog/newpost');
                cluServer::stop();
            }
        }
        if (isset($arguments['page'])) {
            $results = $mysql->prepare("SELECT * FROM blog,users WHERE blog.id = :name and users.id = blog.id_user LIMIT 1;");
            $results->bindParam(':name', $arguments['page'], PDO::PARAM_STR);
            $results->execute();
            $results = $results->fetchAll(PDO::FETCH_ASSOC);
            //echo '<pre>'.print_r($results).'</pre>';
            if ($results) {
                $results = $results['0'];
                $oldresults = $results;
                //v_do($results['header']);
                $id_user_post = $results['id_user'];
                $visible = 1;
                if ($account) {
                    if ($account->getArray()['id'] === $id_user_post || $account->getBooleanGroup('ADM') || $account->getBooleanGroup('MODER')) {
                        $visible = 0;
                    }
                }
                if ($account) {
                    if ($visible == 1) {
                        $results = $mysql->prepare('SELECT * FROM comments, users WHERE ( comments.id_post = :name ) and ( comments.id_user = users.id ) and (( comments.visible = 1 )  or ( comments.id_user = :userid ) )');
                        $results->bindParam(':name', $arguments['page'], PDO::PARAM_STR);
                        $results->bindParam(':userid', $account->getArray()['id'], PDO::PARAM_STR);
                    } else {
                        $results = $mysql->prepare('SELECT * FROM comments, users WHERE ( comments.id_post = :name ) and ( comments.id_user = users.id )');
                        $results->bindParam(':name', $arguments['page'], PDO::PARAM_STR);
                    }
                } else
                    $results = $mysql->prepare('SELECT * FROM comments, users WHERE ( comments.id_post = :name ) and ( comments.id_user = users.id ) and ( comments.visible = ' . $visible . ' )');
                $results->bindParam(':name', $arguments['page'], PDO::PARAM_STR);
                $results->execute();
                $results = $results->fetchAll(PDO::FETCH_ASSOC);
                $arraycomment = null;
                //echo '<input value="Голосовать!" onclick="noticeSetup(\'test\');" type="button" />';
                if ($results) {
                    //echo 'Комментарии ('.sizeof($results).')';
                    $arraycomment = recursionCommentCreate($results);
                }
                cluServer::$data->render('blog/view', [
                        'model' => $oldresults
                        , 'comments_num' => sizeof($results)
                        , 'id_user_post' => $id_user_post
                        , 'page' => $arguments['page']
                        , 'visible' => $visible
                        , 'comment' => $arraycomment]);
            } else {
                //v_up();
                //v_404err();
                //v_down();
                echo print404Error();
            }
        } else {
            $results = $mysql->prepare("SELECT * FROM blog LIMIT 30;");
            //$results->bindParam(':limit', $arguments['limit'], PDO::PARAM_INT);
            $results->execute();
            $results = $results->fetchAll(PDO::FETCH_ASSOC);
            if ($results) {
                //echo '<pre>'.print_r($results).'</pre>';
                cluServer::$data->render('blog/index', [
                    'model' => $results]);
            } else {
                echo '<font size=24>Упс :( Похоже записей в блоге нет</font>';
            }
        }
    }
}
