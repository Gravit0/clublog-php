-- phpMyAdmin SQL Dump
-- version 4.7.0
-- https://www.phpmyadmin.net/
--
-- Хост: localhost
-- Время создания: Апр 19 2017 г., 14:20
-- Версия сервера: 5.7.17-11-log
-- Версия PHP: 7.1.4

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База данных: `newsite`
--

-- --------------------------------------------------------

--
-- Структура таблицы `ac`
--

CREATE TABLE `ac` (
  `id` int(11) NOT NULL,
  `idUser` int(11) NOT NULL,
  `idItems` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='таблица учителей';

-- --------------------------------------------------------

--
-- Структура таблицы `addUser`
--

CREATE TABLE `addUser` (
  `id` int(11) NOT NULL,
  `email` text NOT NULL,
  `keyStr` text NOT NULL,
  `pass` text NOT NULL,
  `name` text NOT NULL,
  `login` text NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `adminlog`
--

CREATE TABLE `adminlog` (
  `id` bigint(20) NOT NULL,
  `id_admin` bigint(20) NOT NULL,
  `id_user` bigint(20) DEFAULT NULL,
  `actionid` int(11) DEFAULT NULL,
  `text` text,
  `dtime` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `banned`
--

CREATE TABLE `banned` (
  `id` bigint(20) NOT NULL,
  `id_user` bigint(20) NOT NULL,
  `id_moder` bigint(20) NOT NULL,
  `unbandata` datetime DEFAULT NULL,
  `reason` text,
  `permanet` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `blog`
--

CREATE TABLE `blog` (
  `id` int(11) NOT NULL,
  `id_user` int(11) NOT NULL,
  `category` int(11) NOT NULL DEFAULT '1',
  `header` varchar(128) NOT NULL,
  `brieftext` text NOT NULL,
  `text` longtext NOT NULL,
  `rating` int(11) NOT NULL DEFAULT '0',
  `visible` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=MyISAM DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

--
-- Дамп данных таблицы `blog`
--

INSERT INTO `blog` (`id`, `id_user`, `category`, `header`, `brieftext`, `text`, `rating`, `visible`) VALUES
(1, 3, 1, 'Ваша первая запись', 'Краткий текст записи', 'Этот текст - сама новость.', 11, 1),
(2, 3, 1, 'Ваша вторая запись', 'Краткий текст записи', 'Этот текст - сама новость.', 2, 1),
(3, 3, 1, 'Новый пост', 'Кр.Текст', '123', 1, 1);

-- --------------------------------------------------------

--
-- Структура таблицы `comments`
--

CREATE TABLE `comments` (
  `id_comment` bigint(20) NOT NULL,
  `id_user` int(11) NOT NULL,
  `id_post` int(11) NOT NULL,
  `text` text NOT NULL,
  `visible` tinyint(1) NOT NULL,
  `parent` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `comments`
--

INSERT INTO `comments` (`id_comment`, `id_user`, `id_post`, `text`, `visible`, `parent`) VALUES
(1, 3, 1, 'Комментарий к первой записи', 1, 0),
(2, 2, 1, 'Восстань и сияй', 0, 0);

-- --------------------------------------------------------

--
-- Структура таблицы `karma`
--

CREATE TABLE `karma` (
  `id` bigint(20) NOT NULL,
  `id_user` bigint(20) NOT NULL,
  `id_giveuser` bigint(20) NOT NULL,
  `goloc` tinyint(1) NOT NULL DEFAULT '1',
  `comment` text
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `pages`
--

CREATE TABLE `pages` (
  `id` int(11) NOT NULL,
  `pagename` varchar(64) NOT NULL,
  `pageinfo` varchar(128) NOT NULL,
  `id_user` int(11) NOT NULL,
  `text` longtext NOT NULL,
  `createtime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `pages`
--

INSERT INTO `pages` (`id`, `pagename`, `pageinfo`, `id_user`, `text`, `createtime`) VALUES
(1, 'o_has', 'О нас', 11, 'Радел \"О Нас\"', '2016-07-11 07:45:12'),
(2, 'products', 'Продукты', 11, 'Раздел продукты.', '2016-07-11 08:34:43'),
(3, 'repo', 'Разработчикам', 11, 'Эта страница будет содержать репозитории проектов', '2016-07-11 08:36:33'),
(4, 'rules', 'Правила использования сервиса', 11, 'Правила использования сервиса:\r\nТут будут правила пользования сервисом', '2016-07-20 14:41:10'),
(5, 'rules2', 'Политика конфиденциальности', 11, 'Политика конфиденциальности', '2016-07-20 14:42:25');

-- --------------------------------------------------------

--
-- Структура таблицы `rating`
--

CREATE TABLE `rating` (
  `id` bigint(20) NOT NULL,
  `id_user` bigint(20) NOT NULL,
  `id_post` bigint(20) NOT NULL,
  `goloc` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `required`
--

CREATE TABLE `required` (
  `id` int(11) NOT NULL,
  `idCreated` int(11) NOT NULL,
  `idType` int(11) NOT NULL,
  `data` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `str` text NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `resetPass`
--

CREATE TABLE `resetPass` (
  `id` int(11) NOT NULL,
  `email` text NOT NULL,
  `keygen` text NOT NULL,
  `time` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='Восстановлнение пароля';

-- --------------------------------------------------------

--
-- Структура таблицы `uniques`
--

CREATE TABLE `uniques` (
  `id` int(11) NOT NULL,
  `active` tinyint(1) NOT NULL DEFAULT '1',
  `type` varchar(1) NOT NULL DEFAULT '0',
  `unique` text NOT NULL,
  `country` varchar(2) NOT NULL,
  `ip` varbinary(16) NOT NULL,
  `init` text NOT NULL,
  `initV` text NOT NULL,
  `lastDate` datetime NOT NULL,
  `idUser` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `uniques`
--

INSERT INTO `uniques` (`id`, `active`, `type`, `unique`, `country`, `ip`, `init`, `initV`, `lastDate`, `idUser`) VALUES
(1, 1, '0', '14e80201178c79f326903740406f6b2c', '', 0x7f000001, 'Firefox', '52.0', '2017-04-19 14:44:50', 3),
(2, 1, '0', 'cc89050dfc6fe1920bb28d0c82f33e9b', '', 0x7f000001, 'Firefox', '52.0', '2017-04-12 15:28:43', 3);

-- --------------------------------------------------------

--
-- Структура таблицы `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `group` text NOT NULL,
  `name` text CHARACTER SET utf8 COLLATE utf8_unicode_ci,
  `nameUser` text,
  `pass` text CHARACTER SET utf8 COLLATE utf8_unicode_ci,
  `pass2` text,
  `timeZone` varchar(40) NOT NULL,
  `karma` int(11) DEFAULT NULL,
  `karmavotes` int(11) NOT NULL DEFAULT '0',
  `invites` int(11) NOT NULL DEFAULT '0',
  `colord` varchar(8) NOT NULL DEFAULT '212121',
  `avatar` varchar(3) DEFAULT NULL,
  `comments` int(11) NOT NULL DEFAULT '0',
  `boy` tinyint(1) NOT NULL DEFAULT '1',
  `birthdate` datetime DEFAULT NULL,
  `last_online` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `last_comment` datetime DEFAULT NULL,
  `email` text NOT NULL,
  `banid` bigint(20) NOT NULL DEFAULT '0',
  `sessionprivasebrowser` tinyint(1) NOT NULL DEFAULT '1',
  `sessionprivaseip` tinyint(1) NOT NULL DEFAULT '1',
  `autocloserconnect` tinyint(1) NOT NULL DEFAULT '0',
  `blockIPV4` tinyint(1) NOT NULL DEFAULT '0',
  `blockIPV6` tinyint(1) NOT NULL DEFAULT '0',
  `autocloserhourconnect` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `users`
--

INSERT INTO `users` (`id`, `group`, `name`, `nameUser`, `pass`, `pass2`, `timeZone`, `karma`, `karmavotes`, `invites`, `colord`, `avatar`, `comments`, `boy`, `birthdate`, `last_online`, `last_comment`, `email`, `banid`, `sessionprivasebrowser`, `sessionprivaseip`, `autocloserconnect`, `blockIPV4`, `blockIPV6`, `autocloserhourconnect`) VALUES
(3, ',POSTS,COMMENT,MODER,ADM,SUPERUSER', 'Gravit', 'Гравит', '$2y$10$hQnY0EncaY5LxIw9jseek.S8eekNkaQg4xo9njU5MCN6RTHoSmSXW', NULL, 'Asia/Barnaul', 0, 0, 20, 'FF0000', '0', 1, 1, '2000-10-03 00:00:00', '2017-04-19 14:44:50', '2017-04-16 10:52:10', 'xellgf2013@yandex.ru', 0, 1, 1, 0, 0, 0, 0),
(2, ',NOLOGIN', 'anonymous', 'Anonymous', 'nologin', NULL, '', 0, 0, 0, '212121', NULL, 0, 1, NULL, '2017-03-26 18:17:47', NULL, '', 0, 1, 1, 0, 0, 0, 0),
(1, 'SUPERUSER,COMMENT,READONLY,NOLOGIN', 'root', 'Суперпользователь', 'nologin', NULL, '', 0, 0, 0, '212121', NULL, 0, 1, NULL, '2017-03-26 18:23:00', NULL, '', 0, 1, 1, 0, 0, 0, 0);

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `ac`
--
ALTER TABLE `ac`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `addUser`
--
ALTER TABLE `addUser`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `adminlog`
--
ALTER TABLE `adminlog`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `banned`
--
ALTER TABLE `banned`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `blog`
--
ALTER TABLE `blog`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `comments`
--
ALTER TABLE `comments`
  ADD PRIMARY KEY (`id_comment`);

--
-- Индексы таблицы `karma`
--
ALTER TABLE `karma`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `pages`
--
ALTER TABLE `pages`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `rating`
--
ALTER TABLE `rating`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `required`
--
ALTER TABLE `required`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `resetPass`
--
ALTER TABLE `resetPass`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `uniques`
--
ALTER TABLE `uniques`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `id` (`id`);

--
-- Индексы таблицы `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT для сохранённых таблиц
--

--
-- AUTO_INCREMENT для таблицы `ac`
--
ALTER TABLE `ac`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблицы `addUser`
--
ALTER TABLE `addUser`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;
--
-- AUTO_INCREMENT для таблицы `adminlog`
--
ALTER TABLE `adminlog`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблицы `banned`
--
ALTER TABLE `banned`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблицы `blog`
--
ALTER TABLE `blog`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT для таблицы `comments`
--
ALTER TABLE `comments`
  MODIFY `id_comment` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT для таблицы `karma`
--
ALTER TABLE `karma`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблицы `pages`
--
ALTER TABLE `pages`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT для таблицы `rating`
--
ALTER TABLE `rating`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблицы `required`
--
ALTER TABLE `required`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT для таблицы `resetPass`
--
ALTER TABLE `resetPass`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=27;
--
-- AUTO_INCREMENT для таблицы `uniques`
--
ALTER TABLE `uniques`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT для таблицы `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
