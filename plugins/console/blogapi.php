<?php
class blogapi extends Plugin {
    public $library = array(); //ЗАГРУЗКА БИБЛИОТЕК ДЛЯ СКРИПТА
    function run($arguments) {
        cluServer::LoadAction('blog');
        $account = null;
        if (cluAccount::getBooleanConnect()) {
            $account = cluAccount::getAccount();
            $mysql = $account->umysql;
        } else {
            returnNotice(RAUE, 'Вы не авторизированы!', 'red');
            cluServer::stop();
        }
        if (isset($arguments['command'])) {
            if ($arguments['command'] == 'comment_ok') {
                if ($account == null)
                    return print403Error();
                if ($account->getBooleanGroup('MODER')) {
                    blogAction::acceptComment($account->umysql, $arguments['id']);
                    returnOK();
                } else {
                    returnNotice(RGE, 'У вас недостаточно прав!', 'red');
                }
            } else if ($arguments['command'] == 'comment_rm') {
                if ($account == null)
                    return print403Error();
                if ($account->getBooleanGroup('MODER')) {
                    if (blogAction::deleteComment($account->umysql, $arguments['id'])) {
                        returnOK();
                    } else {
                        returnNotice('0', 'Комментарий уже удален!', 'blue');
                    }
                } else {
                    returnNotice(RGE, 'У вас недостаточно прав!', 'red');
                }
            } else if ($arguments['command'] == 'post_rm') {
                if ($account == null)
                    return print403Error();
                if ($account->getBooleanGroup('ADM')) {
                    blogAction::deletePost($account->umysql, $arguments['id']);
                    returnOK();
                } else {
                    returnNotice(RGE, 'У вас недостаточно прав!', 'red');
                }
            } else if ($arguments['command'] == 'newpost') {
                if ($account == null)
                    return print403Error();
                $accarray = $account->getArray();
                if ($account->GetBooleanGroup("READONLY")) {
                    echo 'Ваш аккаунт имеет статус ReadOnly. Вам комментировать запрещено.';
                } else {
                    $mysqld = $account->umysql;
                    $mysql = null;
                    $mysql = $mysqld->prepare('INSERT INTO `blog` (
					`id_user` ,
					`text` ,
					`brieftext` ,
					`header`
					)
					VALUES (
					:id, :text, :minitext, :header
					);');
                    print_r($arguments);
                    $text = str_replace("\n", '<br>', $arguments['text']);
                    $mysql->bindParam(':text', $text, PDO::PARAM_STR);
                    $mysql->bindParam(':id', $accarray['id'], PDO::PARAM_INT);
                    $mysql->bindParam(':minitext', $arguments['minitext'], PDO::PARAM_STR);
                    $mysql->bindParam(':header', $arguments['header'], PDO::PARAM_STR);
                    $mysql->execute();
                    returnOK();
                }
            } else if ($arguments['command'] == 'newcomment') {
                if ($account == null)
                    return print403Error();
                if ($account->GetBooleanGroup("READONLY")) {
                    returnNotice('0', 'Ваш аккаунт имеет статус ReadOnly. Вам комментировать запрещено.', 'red');
                    cluServer::stop();
                }
                $mysqld = $account->umysql;
                $mysql = null;
                if (strlen($arguments['comment']) < 5) {
                    returnNotice('0', 'Слишком мало символов!', 'red');
                    cluServer::stop();
                }
                $visible = null;
                if ($account->GetBooleanGroup("COMMENT")) {
                    $visible = 1;
                } else {
                    $visible = 0;
                }
                $text = str_replace(array('<', '>', "\n"), array('&lt;', '&gt;', "<br>"), $arguments['comment']);
                blogAction::newComment($mysqld, $arguments['page'], $account->getArray()['id'], $text, $visible, 0);
                returnOK();
            } else if ($arguments['command'] == 'rating_up') {
                $results = $mysql->prepare("SELECT * FROM rating WHERE rating.id_user = :userid and rating.id_post = :postid LIMIT 1;");
                $results->bindParam(':postid', $arguments['id'], PDO::PARAM_STR);
                $accarray = $account->getArray();
                $results->bindParam(':userid', $accarray['id'], PDO::PARAM_STR);
                $results->execute();
                $results = $results->fetchAll(PDO::FETCH_ASSOC);
                if ($results) {
                    returnNotice('0', 'Вы уже голосовали за этот пост!', 'red');
                } else {
                    $mysqld = $account->umysql;
                    $mysql = null;
                    $mysql = $mysqld->prepare('INSERT INTO `rating` (
                        `id_user` ,
                        `id_post` ,
                        `goloc`
                        )
                        VALUES (
                        :id, :post, \'1\'
                        );');
                    //print_r($arguments);
                    $mysql->bindParam(':id', $accarray['id'], PDO::PARAM_INT);
                    $mysql->bindParam(':post', $arguments['id'], PDO::PARAM_INT);
                    $mysql->execute();
                    $mysql = $mysqld->prepare('UPDATE `blog` SET `rating` = `rating` + 1 WHERE `id` = :post');
                    //print_r($arguments);
                    $mysql->bindParam(':post', $arguments['id'], PDO::PARAM_INT);
                    $mysql->execute();
                    returnOK();
                }
            } else if ($arguments['command'] == 'rating_down') {
                $results = $mysql->prepare("SELECT * FROM rating WHERE rating.id_user = :userid and rating.id_post = :postid LIMIT 1;");
                $results->bindParam(':postid', $arguments['id'], PDO::PARAM_STR);
                $accarray = $account->getArray();
                $results->bindParam(':userid', $accarray['id'], PDO::PARAM_STR);
                $results->execute();
                $results = $results->fetchAll(PDO::FETCH_ASSOC);
                if ($results) {
                    returnNotice('0', 'Вы уже голосовали за этот пост!', 'red');
                } else {
                    $mysqld = $account->umysql;
                    $mysql = null;
                    $mysql = $mysqld->prepare('INSERT INTO `rating` (
                        `id_user` ,
                        `id_post` ,
                        `goloc`
                        )
                        VALUES (
                        :id, :post, \'0\'
                        );');
                    //print_r($arguments);
                    $mysql->bindParam(':id', $accarray['id'], PDO::PARAM_INT);
                    $mysql->bindParam(':post', $arguments['id'], PDO::PARAM_INT);
                    $mysql->execute();
                    $mysql = $mysqld->prepare('UPDATE `blog` SET `rating` = `rating` - 1 WHERE `id` = :post');
                    //print_r($arguments);
                    $mysql->bindParam(':post', $arguments['id'], PDO::PARAM_INT);
                    $mysql->execute();
                    returnOK();
                }
            }
        }
    }
}
