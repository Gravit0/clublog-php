<?php
class indexAction extends Action
{
    static function adminlog($account, $text, $iduser = null, $idaction = null) {
        $mysqld = $account->umysql;
        $mysql = $mysqld->prepare("INSERT INTO `adminlog` (
					`id_admin` ,
					`id_user` ,
					`actionid` ,
					`text`
					)
					VALUES (
					:id, :iduser,:actionid, :text
					);");
        $mysql->bindParam(':id', $account->getArray()['id'], PDO::PARAM_INT);
        $mysql->bindParam(':iduser', $iduser, PDO::PARAM_INT);
        $mysql->bindParam(':actionid', $idaction, PDO::PARAM_INT);
        $mysql->bindParam(':text', $text, PDO::PARAM_STR);
        $mysql->execute();
    }
    static function getUserData($mysqld, $userid, $cache = null) {
        if ($cache) {
            $res = $cache->get(cacheUserPrefix . $userid);
            if ($res)
                return $res;
        }
        $results = $mysqld->prepare('SELECT * FROM users WHERE `id` = :name LIMIT 1;');
        $results->bindParam(':name', $userid, PDO::PARAM_STR);
        $results->execute();
        $results = $results->fetchAll(PDO::FETCH_ASSOC);
        if ($cache) {
            $res = $cache->set(cacheUserPrefix . $userid, $results);
        }
        return $results;
    }
    static function updateUser($cache, $userid) {
        if ($cache) {
            $cache->update(cacheUserPrefix . $userid);
        }
    }
}
