<?php
class page extends Plugin {
	public $library=array();
	function run($arguments){
			$mysql=new cluMysql();
			$results=$mysql->prepare("SELECT * FROM pages WHERE `pagename` = :name LIMIT 1;");
			$results->bindParam(':name', $arguments['page'], PDO::PARAM_STR);
			$results->execute();
			$results = $results->fetchAll(PDO::FETCH_ASSOC);
			//echo '<pre>'.print_r($results).'</pre>';
			if($results)
			{
				$results=$results['0'];
				cluServer::$data->setName($results['pageinfo']);
                                cluServer::$data->render('page',['text'=>$results['text']]);
			}
			else
			{
				//v_up();
				//v_404err();
				//v_down();
			}
	}
}
