<?php
if ($_SERVER['DOCUMENT_ROOT'] . $_SERVER['PHP_SELF'] == __FILE__)
    exit('');
function code($arg) {
    $reply = '';
    if (!is_array($arg))
        $arg = array($arg);
    if ($arg != array()) {
        foreach ($arg as $name => $value) {
            if (!is_array($value))
                $value = str_replace(array('<', '>'), array('&lt;', '&gt;'), $value);
            $name = str_replace(array('<', '>'), array('&lt;', '&gt;'), $name);
            if (is_array($value)) {
                $value = code($value);
            }
            $reply = $reply . '<' . $name . '>' . $value . '</' . $name . '>';
        }
    }
    return $reply;
}
function encode($s) {
    if (!getCluTegsBoolean($s))
        return array();
    $mas = array();
    while (true) {
        if (preg_match('/<(.+?)>(.*?)<\\/\\1>/', $s, $arr)) {
            $s = substr($s, strlen($arr[0]));
            if (getCluTegsBoolean($arr[2]))
                $arr[2] = cluTegs::encode($arr[2]);
            $mas[$arr[1]] = str_replace(array('&lt;', '&gt;'), array('<', '>'), $arr[2]);
        } else
            break;
    }
    return $mas;
}
function getCluTegsBoolean($s) {
    if (preg_match('/<(.+?)>(.*?)<\\/\\1>/', $s, $n))
        return true;
    return false;
}
