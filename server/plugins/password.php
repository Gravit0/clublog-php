<?php
function generateStr($length = 8){
  $chars = 'abdefhiknrstyzABDEFGHKNQRSTYZ23456789';
  $numChars = strlen($chars);
  $string = '';
  for ($i = 0; $i < $length; $i++) {
    $string .= substr($chars, rand(1, $numChars) - 1, 1);
  }
  return $string;
}
function hashPassword($pass)
{
    $result=password_hash($pass,PASSWORD_DEFAULT);
    return $result;
}
class password extends Plugin {
	function run($arguments){
        if($arguments['type'] == 'gen')
        {
            echo TextColor("Вычисление хеша пароля(для подстановки в БД).\n",31);
            echo hashPassword($arguments['pass']);
            echo "\n";
		}
		else if($arguments['type'] == 'verify')
        {
            echo TextColor("Верификация хеша пароля.\n",31);
            echo 'Пароль:'.$arguments['pass']."\n";
            echo 'Хеш:'.$arguments['hash']."\n";
            if(password_verify($arguments['pass'],$arguments['hash']))
                echo "Пароль верен хешу\n";
            else
                echo "Пароль неверен хешу\n";
		}
		else if($arguments['type'] == 'test')
        {
            echo TextColor("Верификация хеша пароля.\n",31);
            echo 'Пароль:'.$arguments['pass']."\n";
            $hash=password_hash($arguments['pass'],PASSWORD_DEFAULT);
            echo 'Хеш:'.$hash."\n";
            if(password_verify($arguments['pass'],$hash))
                echo "Пароль верен хешу\n";
            else
                echo "Пароль неверен хешу\n";
		}
	}
}
