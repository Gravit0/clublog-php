<?php
//if(!defined('HomeDirectory') || HomeDirectory.$_SERVER['PHP_SELF'] == __FILE__) exit('');
if (cluAccountAutoCreate == true) {
    if (cluAccount::getConnectUnique()) {
        //КЛЮЧ КАКОЙ-ТО ЕСТЬ
        //
		cluAccount::getAccount();
    }
}
final class cluAccount {
    public $umysql;
    //PDO
    public $cache;
    //ARRAY, ДАННЫЕ ПОЛЬЗОВАТЕЛЕЙ
    private $array;
    //ARRAY, ДАННЫЕ ПОЛЬЗОВАТЕЛЯ
    private $unique;
    //ARRAY, МУСКУЛ КУКИ ПОЛЬЗОВАТЕЛЯ
    private $groups;
    //ARRAY, ГРУППЫ ПОЛЬЗОВАТЕЛЯ
    private $guest;
    //BOOLEAN - ТЫ ГОСТЬ?
    private $cacheGroups;
    //ARRAY, ГРУППЫ ПОЛЬЗОВАТЕЛЕЙ, ДРОБЛЕННЫЕ
    function __construct(&$array, $umysql, &$unique, $boolGuest = false) {
        if (!$unique || !$array) {
            createCrash()->account(RUE);
            return;
        }
        $this->array = &$array;
        $this->cache = null;
        $this->umysql = $umysql;
        $this->unique = &$unique;
        $this->guest = $boolGuest;
        //GROUPS
        $group = $this->array['group'];
        if ($group) {
            $this->groups = explode(',', $group);
            //if($this->groups){
            if (cluServer::$plugin)
            if (cluServer::$plugin->space == PASV) {
                if (in_array('RESPASS', $this->groups)) {
                    if (cluServer::$plugin->name != 'vresetpass') {
                        $this->umysql = null;
                        $this->guest = true;
                        createHeaderLocation(defaultPlugin, array('value' => 'Для доступа требуются повышенные права.'));
                        return;
                    }
                }
                if (in_array('EDITC', $this->groups)) {
                    if (cluServer::$plugin->name != 'veditc') {
                        $this->umysql = null;
                        $this->guest = true;
                        createHeaderLocation(defaultPlugin, array('value' => 'Для доступа требуются повышенные права.'));
                        return;
                    }
                }
            }
            //}
        }
    }
    public function getUnique() {
        return $this->unique;
    }
    public function unsetUnique() {
        $this->unique = null;
        return $this;
    }
    public function getUniqueStr() {
        return $this->unique['unique'];
    }
    public function getGuestUniquesArray($unique) {
        $get = array();
        $get[] = array('id' => $unique['id'], 'active' => 1, 'type' => 0, 'unique' => $unique['unique'], 'init' => $unique['init'], 'initV' => $unique['initV'], 'lastDate' => DATE, 'ip' => $unique['ip'], 'country' => $unique['country']);
        $get[] = array('id' => -9, 'active' => 1, 'type' => 0, 'unique' => '0XXX', 'init' => 'Firefox', 'initV' => '47.0', 'lastDate' => '2016-07-24 15:19:15', 'ip' => $unique['ip'], 'country' => $unique['country']);
        $get[] = array('id' => -8, 'active' => 1, 'type' => 0, 'unique' => '0XXX', 'init' => 'Firefox', 'initV' => '47.0', 'lastDate' => '2016-07-24 15:19:15', 'ip' => $unique['ip'], 'country' => $unique['country']);
        $get[] = array('id' => -7, 'active' => 1, 'type' => 1, 'unique' => '0XXX', 'init' => 'cluMobileDiary', 'initV' => '1.2', 'lastDate' => '2016-08-11 10:16:11', 'ip' => $unique['ip'], 'country' => 'RY');
        $get[] = array('id' => -7, 'active' => 1, 'type' => 2, 'unique' => '0XXX', 'init' => 'cluJar', 'initV' => '10.0', 'lastDate' => '2016-08-11 10:16:11', 'ip' => $unique['ip']);
        $get[] = array('id' => -6, 'active' => 1, 'type' => 2, 'unique' => '0XXX', 'init' => 'cluJar', 'initV' => '9.0', 'lastDate' => '2016-08-11 10:16:11', 'ip' => $unique['ip']);
        return $get;
    }
    public function getUniquesArray($id = null, $start = 0, $end = 40) {
        if ($this->isGuest()) {
            return $this->getGuestUniquesArray($this->getUnique());
        }
        $cache = $this->getArray($id);
        $get = $this->umysql->prepare('SELECT * FROM `uniques` WHERE `idUser`=:idUser Limit :start, :end;');
        $get->bindParam(':idUser', $idUser = $cache['id'], PDO::PARAM_INT);
        $get->bindParam(':start', $start, PDO::PARAM_INT);
        $get->bindParam(':end', $end, PDO::PARAM_INT);
        $get->execute();
        $get = $get->fetchAll(PDO::FETCH_ASSOC);
        if ($get) {
            foreach ($get as &$v) {
                $v['lastDate'] = timeConvert($v['lastDate'], $cache['timeZone']);
                $v['ip'] = @inet_ntop($v['ip']);
                //$temp = @inet_ntop($v['ip']);
                //if(!$temp)$v['ip'] = 'FAILED';
                //else $v['ip'] = $temp;
            }
            return $get;
        }
        return null;
    }
    public function getUniquesArrayForType($type = 0, $id = null, $start = 0, $end = 40) {
        if ($this->isGuest()) {
            $array = $this->getGuestUniquesArray($this->getUnique());
            if ($array) {
                $result = array();
                foreach ($array as $v) {
                    if ($v['type'] == $type) {
                        $result[] = $v;
                    }
                }
                if (!$result)
                    return null;
                return $result;
            }
            return null;
        }
        $cache = $this->getArray($id);
        $get = $this->umysql->prepare('SELECT * FROM `uniques` WHERE `idUser`=:idUser AND `type`=:type Limit :start, :end;');
        $get->bindParam(':idUser', $idUser = $cache['id'], PDO::PARAM_INT);
        $get->bindParam(':start', $start, PDO::PARAM_INT);
        $get->bindParam(':end', $end, PDO::PARAM_INT);
        $get->bindParam(':type', $type, PDO::PARAM_INT);
        $get->execute();
        $get = $get->fetchAll(PDO::FETCH_ASSOC);
        if ($get) {
            foreach ($get as &$v) {
                $v['lastDate'] = timeConvert($v['lastDate'], $cache['timeZone']);
                $v['ip'] = @inet_ntop($v['ip']);
                //$temp = @inet_ntop($v['ip']);
                //if(!$temp)$v['ip'] = 'FAILED';
                //else $v['ip'] = $temp;
            }
            return $get;
        }
        return null;
    }
    public function getUniqueCountryArray($id = null, $countUnk = true) {
        if ($this->isGuest()) {
            $get = $this->getGuestUniquesArray($this->getUnique());
            if (!$get)
                return null;
            $uniqueArr;
            foreach ($get as $v) {
                if (!$v['country'] || $v['country'] == '?') {
                    if ($countUnk == false)
                        continue;
                    if ($uniqueArr['?']) {
                        $uniqueArr['?'] = $uniqueArr['?'] + 1;
                    } else {
                        $uniqueArr['?'] = 1;
                    }
                    continue;
                }
                if (isset($uniqueArr[$v['country']])) {
                    $uniqueArr[$v['country']] = $uniqueArr[$v['country']] + 1;
                } else {
                    $uniqueArr[$v['country']] = 1;
                }
            }
            if (!$uniqueArr)
                return null;
            arsort($uniqueArr);
            return $uniqueArr;
        }
        $cache = $this->getArray($id);
        if (!$cache)
            return null;
        $get = $this->umysql->prepare('SELECT country, COUNT(*) FROM uniques WHERE `idUser`=:idUser  GROUP BY country;');
        $get->bindParam(':idUser', $idUser = $cache['id'], PDO::PARAM_INT);
        $get->execute();
        $get = $get->fetchAll(PDO::FETCH_ASSOC);
        if (!$get)
            return null;
        $uniqueArr;
        foreach ($get as $v) {
            if (!$v['country'] || $v['country'] == '?') {
                if ($countUnk == false)
                    continue;
                if ($uniqueArr['?']) {
                    $uniqueArr['?'] = $uniqueArr['?'] + $v['COUNT(*)'];
                } else {
                    $uniqueArr['?'] = 1;
                }
                continue;
            }
            $uniqueArr[$v['country']] = $v['COUNT(*)'];
        }
        if (!$uniqueArr)
            return null;
        arsort($uniqueArr);
        return $uniqueArr;
    }
    public function getUniqueTypesArray($id = null, $countUnk = true) {
        if ($this->isGuest()) {
            $get = $this->getGuestUniquesArray($this->getUnique());
            if (!$get)
                return null;
            $uniqueArr;
            foreach ($get as $v) {
                if (isset($uniqueArr[$v['type']])) {
                    $uniqueArr[$v['type']] = $uniqueArr[$v['type']] + 1;
                } else {
                    $uniqueArr[$v['type']] = 1;
                }
            }
            if (!$uniqueArr)
                return null;
            return array('default' => $uniqueArr[0], 'mobile' => $uniqueArr[1], 'console' => $uniqueArr[2]);
        }
        $cache = $this->getArray($id);
        $get = $this->umysql->prepare('SELECT type, COUNT(*) FROM uniques WHERE `idUser`=:idUser GROUP BY type;');
        $get->bindParam(':idUser', $idUser = $cache['id'], PDO::PARAM_INT);
        $get->execute();
        $get = $get->fetchAll(PDO::FETCH_ASSOC);
        if (!$get)
            return null;
        $uniqueArr;
        foreach ($get as $v) {
            $uniqueArr[$v['type']] = $v['COUNT(*)'];
        }
        if (!$uniqueArr)
            return null;
        arsort($uniqueArr);
        return array('default' => $uniqueArr[0], 'mobile' => $uniqueArr[1], 'console' => $uniqueArr[2]);
    }
    public static function getBooleanSaveUniquesHistory() {
        return !(CLUACCOUNT_CLEAR_THIS_UNIQUE == true);
    }
    public function closeUniquesAll() {
        if ($this->isGuest() == false) {
            if ($account->getBooleanGroup('RESPASS'))
                $account->deleteGroup('RESPASS');
            if ($this->getBooleanSaveUniquesHistory() == false) {
                $temp = $this->umysql->prepare('DELETE FROM `uniques` WHERE `idUser` = :idUser AND `id` != :idunique;');
                //УДАЛЕНИЕ ЗАПИСЕЙ
            } else {
                $temp = $this->umysql->prepare('UPDATE `uniques` SET `active` = 0 WHERE `idUser` = :idUser AND `id` != :idunique;');
                //ЗАПИСИ НЕ АКТИВНЫ
            }
            $cache = $this->getArray();
            $temp->bindParam(':idunique', $idunique = $this->getUnique()['id'], PDO::PARAM_INT);
            $temp->bindParam(':idUser', $id = $cache['id'], PDO::PARAM_INT);
            $temp->execute();
            return true;
        }
        return false;
    }
    public function closeUniquesIP($type = null) {
        if ($this->isGuest() == false) {
            if ($this->getBooleanSaveUniquesHistory() == false) {
                if ($type == null) {
                    $temp = $this->umysql->prepare('DELETE FROM `uniques` WHERE `ip` != :ip AND `idUser` = :idUser AND `id` != :idunique;');
                } else {
                    $temp = $this->umysql->prepare('DELETE FROM `uniques` WHERE `type` = :type AND `ip` != :ip AND `idUser` = :idUser AND `id` != :idunique;');
                    $temp->bindParam(':type', $type, PDO::PARAM_INT);
                }
                //УДАЛЕНИЕ ЗАПИСЕЙ
            } else {
                if ($type == null) {
                    $temp = $this->umysql->prepare('UPDATE `uniques` SET `active` = 0 WHERE `idUser` = :idUser AND `id` != :idunique AND `ip` != :ip;');
                } else {
                    $temp = $this->umysql->prepare('UPDATE `uniques` SET `active` = 0 WHERE `type` = :type AND `idUser` = :idUser AND `id` != :idunique AND `ip` != :ip;');
                    $temp->bindParam(':type', $type, PDO::PARAM_INT);
                }
                //ЗАПИСИ НЕ АКТИВНЫ
            }
            $cache = $this->getArray();
            //print_r($cache);
            exit($cache['ip']);
            echo $cache['ip'];
            $ip = inet_pton($cache['ip']);
            var_dump($ip);
            exit();
            $id = $cache['id'];
            $idunique = $this->getUnique()['id'];
            //TEST
            //
			$temp->bindParam(':idunique', $idunique, PDO::PARAM_STR);
            $temp->bindParam(':idUser', $id, PDO::PARAM_INT);
            $temp->bindParam(':ip', $ip, PDO::PARAM_STR);
            $temp->execute();
            return true;
        }
        return false;
    }
    public function closeUnique($unique, $killMy = false) {
        if ($this->isGuest())
            return false;
        $boolKillMy = $this->getUniqueStr() == $unique;
        if ($boolKillMy) {
            if ($killMy == false)
                return false;
            //Если не разрешено килить себя, то не киль
            //
			if ($account->getBooleanGroup('RESPASS'))
                $account->deleteGroup('RESPASS');
            //Текущяя сессия восстанавливала пароль
        //
		}
        if ($this->getBooleanSaveUniquesHistory() == false) {
            $temp = $this->umysql->prepare('DELETE FROM `uniques` WHERE `idUser` = :idUser and `unique` = :unique Limit 1;');
        } else {
            $temp = $this->umysql->prepare('UPDATE `uniques` SET `active` = 0 WHERE `idUser` = :idUser and `unique` = :unique Limit 1;');
        }
        $cache = $this->getArray();
        $temp->bindParam(':unique', $unique, PDO::PARAM_STR);
        $temp->bindParam(':idUser', $id = $cache['id'], PDO::PARAM_INT);
        $temp->execute();
        return true;
    }
    public function isGuest() {
        return $this->guest;
    }
    public function close() {
        return cluAccount::closeMyConnect($this);
    }
    public function getGroups($id = null) {
        if ($id == null || $id == $this->array['id'])
            return $this->groups; //КЕШИРОВАНИЕ
        if ($this->isGuest() == false)
            return null;
        $cache = $this->getArray($id);
        if ($this->cacheGroups[$id]) {
            return $this->cacheGroups[$id];
        }
        if ($cache['group']) {
            $arr = explode(',', $cache['group']);
            $this->cacheGroups[$id] = $arr;
            return $arr;
        }
        return null;
    }
    public function getBooleanGroup($name, $id = null) {
        $arr = $this->getGroups($id);
        if ($arr) {
            foreach ($arr as $v) {
                if ($v == $name)
                    return true;
            }
        }
        return false;
    }
    public function deleteGroup($group) { //удаляет у тебя группу
        if ($this->groups) {
            foreach ($this->groups as $n => $v) {
                if ($group == $v) {
                    unset($this->groups[$n]);
                    if ($this->groups) {
                        $newGroup = '';
                        foreach ($this->groups as $v) {
                            if (empty($newGroup) == false) {
                                $newGroup .= ',';
                            }
                            $newGroup .= $v;
                        }
                    } else {
                        $newGroup = null;
                        $this->groups = null;
                    }
                    $cache = $this->umysql->prepare('UPDATE `users` SET `group` = :group WHERE `id` = :id Limit 1;');
                    $cache->bindParam(':group', $newGroup, PDO::PARAM_STR);
                    $cache->bindParam(':id', $this->array['id'], PDO::PARAM_INT);
                    $cache->execute();
                    $this->groups = $newGroup;
                    return true;
                }
            }
        }
        return false;
    }
    private $cacheGetArrayPrepare = null;
    public function &getArray($id = null) { //Получение информации о человеке
        if ($id == null || $id == $this->array['id'])
            return $this->array; //КЕШИРОВАНИЕ}
        if ($this->isGuest())
            return null;
        if ($this->cache) {
            $cache = $this->cache[$id];
            if ($cache)
                return $cache;
        }
        if ($this->cacheGetArrayPrepare == null) {
            $this->cacheGetArrayPrepare = $this->umysql->prepare('SELECT * FROM users WHERE id = :id Limit 1;');
        }
        $this->cacheGetArrayPrepare->bindParam(':id', $id, PDO::PARAM_INT);
        $this->cacheGetArrayPrepare->execute();
        $this->cache[$id] = $this->cacheGetArrayPrepare->fetch(PDO::FETCH_ASSOC);
        return $this->cache[$id];
    }
    public function getVisibleUser($id = null) {//ПОЛЬЗОВАТЕЛЬ ОНЛАЙН ЛИ, ВЫЧИСЛЯЕТСЯ ПОСРЕДСТВОМ
        if ($id == null || $id == $this->array['id'])
            return true; // THIS USER
        if ($this->getBooleanGroup('Hidden', $id))
            return false; // HIDDEN
        $cache = $this->getArray($id);
        $result = strtotime(DATE) - strtotime($cache['last_online']);
        return $result >= 0 && $result < CLUACCOUNT_MIN_TIME_ONLINE;
    }
    public static function closeMyConnect($account = null) {
        if ($account == null) {
            $account = cluAccount::getAccount();
        }
        if ($account != null) {
            $unique = $account->getUnique();
            if ($unique) {
                $account->closeUnique($unique['str'], true);
            }
        }
        if ($_COOKIE['user']) {
            foreach ($_COOKIE['user'] as $name => $v) {
                setcookie('user[unique]', '');
            }
            unset($_COOKIE['user']);
        }
        cluAccount::$userAccount = null;
        return true;
    }
    public static function getBooleanConnect() {
        //if(cluAccount::getConnectUnique())return true;
        return cluAccount::$userAccount != null;
    }
    public static function getConnectUnique() {
        if (isset($_COOKIE['user']['unique'])) {
            return $_COOKIE['user']['unique'];
        }
        return false;
    }
    private static $userAccount = null;
    public static function getAccount($crash = true) {
        if (cluAccount::$userAccount != null) {
            return cluAccount::$userAccount;
        }
        $connectUnique = cluAccount::getConnectUnique();
        if (!$connectUnique) {
            if ($crash) {
                createCrash()->account(RAUE);
            }
            //cluAccount::killMyConnect();
            return null;
        }
        if ($connectUnique == '1' || $connectUnique == '2' || $connectUnique == '3') {
            cluAccount::$userAccount = new cluAccount(
                    cluAccount::createGuestArray($connectUnique), null, cluAccount::createGuestUnique($connectUnique), true
            );
            return cluAccount::$userAccount;
        }
        //////////////ОСНОВА
        cluServer::loadLibrary(DBLibrary, false);
        $mysql = new cluMysql();
        //ПОЛУЧЕНИЕ UNIQUE
        $unique = $mysql->prepare('SELECT * FROM `uniques` WHERE `unique`=:unique Limit 1;');
        $unique->bindParam(':unique', $connectUnique, PDO::PARAM_STR);
        $unique->execute();
        $unique = $unique->fetch(PDO::FETCH_ASSOC);
        if (!$unique) {
            if ($crash) {
                createCrash()->account(RAUE);
            }
            return null;
        }
        if (!$unique['active']) {
            if ($crash) {
                createCrash()->account(RAUE);
            }
            return null;
        } // данная сессия была закрыта извне
        //ПОЛУЧЕНИЕ ПОЛЬЗОВАТЕЛЯ
        $auth = $mysql->prepare('SELECT * FROM users WHERE `id` = :id Limit 1;');
        $auth->bindParam(':id', $unique['idUser'], PDO::PARAM_INT);
        $auth->execute();
        $auth = $auth->fetch(PDO::FETCH_ASSOC);
        //
        if (!$auth) { //ПОЛЬЗОВАТЕЛЬ НЕ СУЩЕСТВУЕТ ИЛИ UNIQUE БИТЫЙ
            if ($crash) {
                createCrash()->account(RAUE);
            }
            return null;
        }
        $account = new cluAccount($auth, $mysql, $unique);
        if ($auth['autocloserhourconnect']) { //СЕССИЯ ОГРАНИЧЕНА НА ЧАС
            if ((strtotime(DATE) - strtotime($unique['lastDate'])) >= 3600) {
                //ВРЕМЯ ИСТЕКЛО
                $account->close();
                if ($crash) {
                    createCrash()->account(RAUE);
                }
                return null;
            }
        }
        $ipConnectStr = cluServer::$connect->ip;
        $ipConnect = inet_pton($ipConnectStr);
        $boolSetIp = $unique['ip'] != $ipConnect;
        if ($auth['sessionprivaseip']) {
            if ($boolSetIp) {
                $account->close();
                if ($crash) {
                    createCrash()->account(RAUE);
                }
                return null; // IP ИЗМЕНИЛСЯ
            }
        }
        if ($auth['blockIPV4']) {
            if (getConnectIpVersion() == 'IPV4') {
                //НЕЛЬЗЯ ТАКОЕ СОЕДИНЕНИЕ
                $account->close();
                if ($crash) {
                    createCrash()->account(RAUE);
                }
                return null;
            }
        }
        if ($auth['blockIPV6']) {
            if (getConnectIpVersion() == 'IPV6') {
                //НЕЛЬЗЯ ТАКОЕ СОЕДИНЕНИЕ
                $account->close();
                if ($crash) {
                    createCrash()->account(RAUE);
                }
                return null;
            }
        }
        $browser = getUserBrowser();
        $boolSetInit = $unique['init'] != $browser[0];
        $boolSetInitV = $unique['initV'] != $browser[1];
        if ($auth['sessionprivasebrowser']) {
            if ($boolSetInit) {
                $account->close();
                if ($crash) {
                    createCrash()->account(RAUE);
                }
                return null; // init кука отсутствует или браузер уже не тот
            }
            if ($boolSetInitV) {
                $account->close();
                if ($crash) {
                    createCrash()->account(RAUE);
                }
                return false; // initV кука отсутствует или браузер уже не тот
            }
        }
        if ($boolSetIp || $boolSetInit || $boolSetInitV) {
            //UPDATER
            $temp = $mysql->prepare('
				UPDATE `uniques` SET lastDate = :date, ip = :ip, init = :init, initV = :initV, country = :country WHERE `idUnique` =:idUnique Limit 1;
				UPDATE `users` SET last_online = :date WHERE id =:idUser Limit 1;
			');
            $temp->bindParam(':ip', $ipConnect, PDO::PARAM_STR);
            $temp->bindParam(':init', $browser[0], PDO::PARAM_STR);
            $temp->bindParam(':initV', $browser[1], PDO::PARAM_STR);
            cluServer::loadLibrary(CF_LIBRARY, false);
            $country = getGeolocation($mysql, $ipConnectStr);
            //IP НЕ РАВЕН, ТРЕБУЕТСЯ ПЕРЕБРАТЬ ГЕОЛОКАЦИЮ
            $temp->bindParam(':country', $country, PDO::PARAM_STR);
            $unique['init'] = $browser[0];
            $unique['initV'] = $browser[1];
        } else {
            $temp = $mysql->prepare('
				UPDATE `uniques` SET lastDate = :date WHERE id =:idUnique Limit 1;
				UPDATE `users` SET last_online = :date WHERE id =:idUser Limit 1;
			');
        }
        $date = DATE;
        $unique['ip'] = $ipConnectStr;
        $auth['last_online'] = $date;
        $unique['lastDate'] = $date;
        $temp->bindParam(':idUser', $auth['id'], PDO::PARAM_INT);
        $temp->bindParam(':idUnique', $unique['id'], PDO::PARAM_INT);
        $temp->bindParam(':date', $date, PDO::PARAM_STR);
        $temp->execute();
        cluAccount::$userAccount = $account;
        return cluAccount::$userAccount;
    }
    public static function createGuestArray($index) {
        $default = array(
            'id' => -10,
            'boy' => '1',
            'last_online' => DATE,
            'data' => DATE,
            'autocloserconnect' => 1,
            'sessionprivasebrowser' => 1,
            'sessionprivaseip' => 1,
            'blockIPV4' => 1,
            'blockIPV6' => 1,
            'avatar' => 9,
            'backgroundVInfo' => 9,
            'backgroundVBACKGROUND' => 4,
            'nameUser' => 'Роман',
            'surnameUser' => 'Ширяев',
            'patronymicUser' => 'Иванович',
            'pass2' => 'XX'
        );
        if ($index == 1) {
            $default['class'] = '10B';
            return $default;
        }
        if ($index == 2) {
            $default['group'] = 'AC,BLOCK';
            return $default;
        }
        if ($index == 3) {
            $default['group'] = 'ADM,BLOCK';
            return $default;
        }
        return false;
    }
    public static function createGuestUnique($index) {
        $array = array(
            'id' => '1',
            'unique' => $index,
            'country' => 'BY',
        );
        $browser = getUserBrowser();
        if ($browser[0])
            $array['init'] = $browser[0];
        if ($browser[1])
            $array['initV'] = $browser[1];
        $ip = getConnectIp();
        if ($ip)
            $array['ip'] = $ip;
        return $array;
    }
    public static function createAuthAccount($type, $login, $pass, $init, $initV, $typeConnect = 0) {
        //if(cluAccount::getBooleanConnect()){
        if (cluAccount::getConnectUnique()) {
            cluAccount::closeMyConnect();
        }
        if ($type == 3) {
            if (($login == '1' && $pass == '1') || ($login == '2' && $pass == '2') || ($login == '3' && $pass == '3')) {
                setcookie('user[unique]', $login, time() + CLUACCOUNT_MIN_TIME_LIFE_COOKIE);
                $_COOKIE['user']['unique'] = $login;
                return new cluAccount(cluAccount::createGuestArray($login), null, cluAccount::createGuestUnique($login), true);
            }
            return false;
        }
        $ipConnectStr = getConnectIp();
        $ipConnect = inet_pton($ipConnectStr);
        cluServer::loadLibrary(DBLibrary);
        $mysql = new cluMysql();
        $auth = null;
        if ($type == 1) { //addUser
            $auth = $mysql->prepare('SELECT * FROM `addUser` WHERE `email` =:name AND `keyStr` =:pass Limit 1;');
            $auth->bindParam(':name', $login, PDO::PARAM_STR);
            $auth->bindParam(':pass', $pass, PDO::PARAM_STR);
            $auth->execute();
            $auth = $auth->fetch(PDO::FETCH_ASSOC);
            if ($auth) {
                if ($auth['email'] == $login && $auth['keyStr'] == $pass) {
                    $userSap = null;
                    if (!$auth['idUser']) {
                        $create = $mysql->prepare('INSERT INTO `users` (`email`, `pass`, `group`, `class`) VALUES (:email, :pass, :group, :class);');
                        $create->bindParam(':email', $auth['email'], PDO::PARAM_STR);
                        $create->bindParam(':pass', $auth['keyStr'], PDO::PARAM_STR);
                        $create->bindParam(':class', $auth['class'], PDO::PARAM_STR);
                        if (!$auth['group']) {
                            $create->bindParam(':group', $gr = 'EDITC', PDO::PARAM_STR);
                        } else {
                            $groupConcat = explode(',', $auth['group']);
                            $groupConcat[] = 'EDITC';
                            $create->bindParam(':group', $gr = implode(',', $groupConcat), PDO::PARAM_STR);
                            unset($groupConcat, $gr);
                        }
                        $create->execute();
                        unset($create, $gr);
                        $userSap = $mysql->prepare('SELECT * FROM `users` WHERE `email` =:email AND `pass` =:pass Limit 1;');
                        $userSap->bindParam(':email', $auth['email'], PDO::PARAM_STR);
                        $userSap->bindParam(':pass', $pass, PDO::PARAM_STR);
                        $userSap->execute();
                        $userSap = $userSap->fetchAll(PDO::FETCH_ASSOC);
                        if (!$userSap)
                            return false;
                        $userSap = $userSap[0];
                        if (!$userSap)
                            return false;
                        $auth['idUser'] = $userSap['id'];
                        //UPDATE ID FOR ADDUSER
                        $set = $mysql->prepare('UPDATE `addUser` SET `idUser` = :idUser WHERE `id` = :id Limit 1; ');
                        $set->bindParam(':id', $auth['id'], PDO::PARAM_INT);
                        $set->bindParam(':idUser', $auth['idUser'], PDO::PARAM_INT);
                        $set->execute();
                        unset($set);
                    }
                    if ($userSap == null) {
                        $userSap = $mysql->prepare('SELECT * FROM `users` WHERE `id` = :id Limit 1;');
                        $userSap->bindParam(':id', $auth['idUser'], PDO::PARAM_INT);
                        $userSap->execute();
                        $userSap = $userSap->fetchAll(PDO::FETCH_ASSOC);
                        if (!$userSap)
                            return false;
                        $userSap = $userSap[0];
                        if (!$userSap)
                            return false;
                    }
                    $auth = $userSap;
                    unset($userSap);
                }
            }
        }else
        if ($type == 2) { //RESET PASS
            $reset = $mysql->prepare('SELECT * FROM `resetPass` WHERE `email` =:name AND `keygen` =:pass Limit 1;');
            $reset->bindParam(':name', $login, PDO::PARAM_STR);
            $reset->bindParam(':pass', $pass, PDO::PARAM_STR);
            $reset->execute();
            $reset = $reset->fetchAll(PDO::FETCH_ASSOC);
            if ($reset) {
                $reset = $reset['0'];
                if ($reset) {//ЗАПИСЬ ЕСТЬ, ВСЕ ОК, УДАЛИТЬ ТО ЧТО НЕ ТРЕБУЕТСЯ БОЛЕЕ И ПОЛУЧИТЬ ПЕРВОГО ПОЛЬЗОВАТЕЛЯ
                    //удаляем
                    $delete = $mysql->prepare('DELETE FROM `resetPass` WHERE `id` = :id Limit 1;'); //УДАЛИТЬ ТЕМ САМЫМ ПРЕКРАЩЯЕМ ДОСТУП
                    $delete->bindParam(':id', $reset['id'], PDO::PARAM_STR);
                    $delete->execute();
                    unset($delete);
                    //
                    //print_r($reset);
                    $auth = $mysql->prepare('SELECT * FROM users WHERE email=:email Limit 1;');
                    $auth->bindParam(':email', $reset['email'], PDO::PARAM_STR);
                    $auth->execute();
                    unset($reset); // больше он не требуется
                    $auth = $auth->fetch(PDO::FETCH_ASSOC);
                    if ($auth) { //ПОЛЬЗОВАТЕЛЬ ЗДЕСЬ, ЕМУ ТРЕБУЕТСЯ СПЕЦ ГРУППА
                        $groupConcat = explode(',', $auth['group']);
                        $boolConcat = true;
                        foreach ($groupConcat as $v) {
                            if ($v == 'RESPASS') {
                                $boolConcat = false;
                                break;
                            }
                        }
                        if ($boolConcat)
                            $groupConcat[] = 'RESPASS';
                        $ren = $mysql->prepare('UPDATE `users` SET `group` = :group WHERE `id` = :id  Limit 1;');
                        $ren->bindParam(':id', $auth['id'], PDO::PARAM_INT);
                        $ren->bindParam(':group', $gr = implode(',', $groupConcat), PDO::PARAM_STR);
                        $ren->execute();
                        //ГРУППА ДАНА, ОБНОВИМ ЕЕ ПОЛЬЗОВАТЕЛЮ!
                        $auth['group'] = $gr;
                        unset($groupConcat, $boolConcat, $ren);
                    } else
                        $auth = null;
                }
            }
        }else {
            $auth = $mysql->prepare('SELECT * FROM users WHERE (email=:name or name=:name) Limit 1;');
            $auth->bindParam(':name', $login, PDO::PARAM_STR);
            $auth->execute();
            $auth = $auth->fetch(PDO::FETCH_ASSOC);
        }
        if ($auth) { // ПОЛЬЗОВАТЕЛЬ ВЕРЕН, ПРИСТУПАЕМ К СОЗДАНИИ СЕССИИ, И МЕЛКИМ ТЕСТАМ
            if(!password_verify($pass,$auth['pass']) && !password_verify($pass,$auth['pass2'])) return false;
            $pass = null;
            $thisIPV = getConnectIpVersion();
            if ($auth['blockIPV4']) {
                if ($thisIPV == 'IPV4')
                    return false;
            }
            if ($auth['blockIPV6']) {
                if ($thisIPV == 'IPV6')
                    return false;
            }
            if ($auth['autocloserconnect']) {
                $temp = $mysql->prepare('DELETE FROM `uniques` WHERE `idUser` = :idUser;');
                $temp->bindParam(':idUser', $auth['id'], PDO::PARAM_INT);
                $temp->execute();
            }
            $temp = $mysql
                    ->prepare('INSERT INTO `uniques` (`ip`,`unique`, `lastDate`,`idUser`, `init`, `initV`, `country`, `type`) 
							VALUES (:ip, :unique, :date, :idUser, :init, :initV, :country, :type);
					UPDATE `users` SET last_online = :date WHERE id =:idUser Limit 1;
				');
            //
            $connectUnique = md5(uniqid(rand(), 1));
            if ($init == false)
                $init = '';
            if ($initV == false)
                $initV = '';
            $temp->bindParam(':unique', $connectUnique, PDO::PARAM_STR);
            $temp->bindParam(':init', $init, PDO::PARAM_STR);
            $temp->bindParam(':initV', $initV, PDO::PARAM_STR);
            $temp->bindParam(':idUser', $id = $auth['id'], PDO::PARAM_INT);
            $temp->bindParam(':type', $typeConnect, PDO::PARAM_STR);
            $temp->bindParam(':date', $date = DATE, PDO::PARAM_STR);
            $temp->bindParam(':ip', $ipConnect, PDO::PARAM_STR);
            cluServer::loadLibrary(CF_LIBRARY, false);
            $temp->bindParam(':country', $country = getGeolocation($mysql, $ipConnectStr), PDO::PARAM_STR);
            $temp->execute();
            //ПОЛУЧЕНИЕ КУКО
            $temp = $mysql->prepare('SELECT * FROM `uniques` WHERE `idUser` = :idUser AND `unique` = :unique Limit 1;');
            $temp->bindParam(':idUser', $auth['id'], PDO::PARAM_INT);
            $temp->bindParam(':unique', $connectUnique, PDO::PARAM_STR);
            $temp->execute();
            $unique = $temp->fetch(PDO::FETCH_ASSOC);
            if (!$unique)
                return false;
            $unique['ip'] = $ipConnectStr;
            setcookie('user[unique]', $unique['unique'], time() + CLUACCOUNT_MIN_TIME_LIFE_COOKIE);
            $_COOKIE['user']['unique'] = $unique['unique'];
            return new cluAccount($auth, $mysql, $unique);
        }
        return false;
    }
}
;
