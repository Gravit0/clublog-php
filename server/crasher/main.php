<?php
class crash {
    public $library = array(LibDecodeEncode);
    function vMain($test) {
        echo '<h1>Произошла критическая ошибка. Сообщите администратору подробности ошибки:</h1>';
        echo '<br>Код ошибки:';
        echo $test[0];
        echo '<br>Комментарий:';
        echo $test[1];
        echo '<br>Не забудьте описать что Вы делали до возникновения ошибки.';
    }
    function main($test) {
        echo var_dump($test);
        echo encode(array('key' => $test[0], 'comment' => $test[1]));
    }
    function vAccount($test) {
        echo encode(array('key' => $test[0]));
        echo '<h1>Произошла критическая ошибка при обработке вашего аккаунта. Попробуйте почистить cookies для устранения проблемы.</h1>';
        echo '<br>Код ошибки:';
        echo $test[0];
    }
    function account($test) {
        echo encode(array('key' => $test[0]));
    }
    function vPlugin($test) {
        echo '<h1>Произошла критическая ошибка при обработке запроса. Сообщите администратору подробности ошибки:</h1>';
        echo '<br>Код ошибки:';
        echo $test[0];
        if ($test[1]) {
            echo '<br>Плагин/Библиотека:';
            echo $test[1];
        }
        echo '<br>Комментарий:';
        echo $test[2];
    }
    function plugin($test) {
        echo encode($test);
    }
    function vSQLError($test) {
        echo '<h1>Произошла критическая ошибка при обработке SQL запроса. Сообщите администратору подробности ошибки:</h1>';
        echo '<br>Код ошибки:';
        echo $test[0];
        if ($test[1]) {
            echo '<br>Плагин/Библиотека:';
            echo $test[1];
        }
        if ($test[2]) {
            echo '<br>Комментарий:';
            echo $test[2];
        }
    }
    function SQLError($test) {
        echo encode($test);
    }
    function vPluginException($test) {
        $e = $test[0];
        echo '<h1>Было выброшено необработанное исключение. Сообщите администратору подробности ошибки:</h1>';
        echo '<br>Исключение:';
        echo get_class($e);
        echo '<br>Комментарий:';
        echo $e->getMessage();
        echo '<br>StackTrace:<pre>';
        echo $e->getTraceAsString();
        echo '</pre>';
    }
    function pluginException($test) {
        $e = $test[0];
        echo encode(array('Exception'=>$e->getMessage(),'debug'=>$e->getTrace()));
    }
}
