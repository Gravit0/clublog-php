#!/usr/bin/env php
<?php
function getmicrotime() 
{ 
    list($usec, $sec) = explode(" ", microtime()); 
    return ((float)$usec + (float)$sec); 
} 
$time_start = getmicrotime();
//$argv[] = array('spacePlugin'=>'console');
require('config/console.php'); // загрузить настройки
require('config/db.php'); // загрузить настройки
require('configLoader.php'); // загрузить настройки
require('cluServer.php'); // загрузить default void
require('serverDeveloper.php');
require('spaces/server.php');
//echo DirConsolePlugins.$argv[1].'.php';
$args = null;
unset($argv[0]);
$name = null;
if (isset($argv[1])) {
    $name = $argv[1];
} else {
    $name = 'help';
}
unset($argv[1]);
$key = null;
foreach($argv as $v)
{
    if(!strncmp($v,'-',1))
    {
        $key=substr($v,1);
    }
    else
    {
        $args[$key]=$v;
    }
}
unset($v);
ob_start();
if (cluServer::loadPlugin(PASS,$name, DirServerPlugins . $name . '.php', $args)) {
    $time_end = microtime(1);
    $time = $time_end - $time_start;
    echo $time;
    cluServer::stop();
} else {
    createCrash('cluServer')->pluginNotLoaded($name);
}
