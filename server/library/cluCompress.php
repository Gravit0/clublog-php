<?php
class compress {
    private $init = false;
    private $lvl;
    private $compress;
    function __construct($compress, $level, $test = true) {
        $level = (int) $level;
        if ($level >= 0 && $level <= 9) {
            if ($test == true) {
                if (compress::getBooleanMod($compress)) {
                    $this->init = true;
                    $this->lvl = $level;
                    $this->compress = $compress;
                }
            } else {
                $this->init = true;
                $this->lvl = $level;
                $this->compress = $compress;
            }
        }
    }
    public function getCompress() {
        return $this->compress;
    }
    public function setCompressLevel($level) {
        $level = (int) $level;
        if ($level >= 0 && $level <= 9) {
            $this->lvl = $level;
            return true;
        }
        return false;
    }
    public function setCompress($compress) {
        if (compress::getBooleanMod($compress)) {
            $this->compress = $compress;
            if ($level >= 0 && $level <= 9) {
                $this->init = true;
            }
            return true;
        }
        return false;
    }
    public function getInit() {
        return $this->init;
    }
    public function getCompressLevel() {
        return $this->lvl;
    }
    public function compress($str) {
        if ($this->init == false)
            return false;
        if ($this->compress == 'deflate') {
            $a = @gzdeflate($str, $$this->lvl);
            if (!$a)
                return false;
            return $a;
        }
        if ($this->compress == 'gzip') {
            $a = @gzencode($str, $this->lvl);
            if (!$a)
                return false;
            return $a;
        }
        if ($this->compress == 'zlib') {
            $a = @gzcompress($str, $this->lvl);
            if (!$a)
                return false;
            return $a;
        }
        return false;
    }
    public function ucompress($str) {
        if ($this->init == false)
            return false;
        if ($this->compress == 'deflate') {
            $a = @gzinflate($str);
            if (!$a)
                return false;
            return $a;
        }
        if ($this->compress == 'gzip') {
            $a = @gzdecode($str);
            if (!$a)
                return false;
            return $a;
        }
        if ($this->compress == 'zlib') {
            $a = @gzuncompress($str);
            if (!$a)
                return false;
            return $a;
        }
        return false;
    }
    public static function getMods() {
        return array('deflate', 'gzip', 'zlib');
    }
    public static function getBooleanMod($mod) {
        $array = compress::getMods();
        if (!$array)
            return false;
        foreach ($array as $v) {
            if ($v == $mod)
                return true;
        }
        return false;
    }
}
