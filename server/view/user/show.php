<?php
$accarray = array();
if (cluServer::$account) {
    $accarray = cluServer::$account->getArray();
}
$acctype = "Только чтение";
$groups = explode(',', $model['group']);
$mygroups = null;
if ($account)
    $mygroups = explode(',', $accarray['group']);
if (in_array("COMMENT", $groups))
    $acctype = "Чтение и комментирование";
if (in_array("POSTS", $groups))
    $acctype = "Полноценный аккаунт";
if (in_array("READONLY", $groups))
    $acctype = "Только чтение(выдано модератором)";
if (in_array("NOLOGIN", $groups))
    $acctype = "Служебный";
$karmacolor = "#DFDFDF";
if ($results['karma'] > 0)
    $karmacolor = "green";
else if ($results['karma'] < 0)
    $karmacolor = "red";
cluServer::$data->LoadLibrary('select/std');
?>
<img src="<?= DirImages ?>avatar.png" class="avatar">
Ник пользователя: <font style="color: #<?= $model['colord'] ?>;"><?= $model['name'] ?></font>
<?php
if ($accarray) {
    if ($accarray['id'] == $model['id']) {
        echo '<b>(это вы)</b>';
    }
}
if ($results['banid'] > 0) {
    echo '<font style="color: red;">(забанен)</font>';
} else if (( strtotime($model['last_online']) + 3600) > time()) {
    echo '<b style="color: green;">(онлайн)</b>';
}
?>
<br>Имя пользователя: <?= $model['nameUser'] ?>
<br>
<?php
if ($model['boy']) {
    echo 'Пол: Мужской<br>';
} else {
    echo 'Пол: Женский<br>';
}
if (cluServer::$account) {
    if (cluServer::$account->getBooleanGroup('ADM')) {
        echo 'Email: ' . $results['email'] . '<br>';
        echo 'Последний раз коментировал: ' . $results['last_comment'] . '<br>';
    }
}
?>
Комментариев: <?= $model['comments'] ?> . <br>
Последний раз был: <?= $model['last_online'] ?> <br>
Часовой пояс: <?= $model['timeZone'] ?> <br>
Тип аккаунта: <?= $acctype ?> <br>
Карма: 
<?php
if (cluServer::$account) {
    if (cluServer::$account->getBooleanGroup('COMMENT') && $accarray['id'] != $model['id']) {
        echo ' <img src="' . DirImages . 'up.png" class="profileaction" onclick="apirequest(\'?type=karma&action=up&id=' . $model['id'] . '\');"> ';
    }
}
?>
<font style="color: <?= $karmacolor ?>"><?= $model['karma'] ?></font>
<?php
if (cluServer::$account) {
    if (cluServer::$account->getBooleanGroup('COMMENT') && $accarray['id'] != $model['id']) {
        echo ' <img src="' . DirImages . 'down.png" class="profileaction" onclick="apirequest(\'?type=karma&action=down&id=' . $model['id'] . '\');"> ';
    }
}
?><br>
Приглашений: <?= $model['invites'] ?><br>
<?php
if (cluServer::$account)
    if (($accarray['id'] == $model['id']) || in_array("ADM", $mygroups)) {
        echo 'Голосов за карму: ';
        echo $model['karmavotes'];
        echo '<br>';
    }
if (in_array("ADM", $groups))
    echo 'Должность: <font style="color: red;">Администратор</font><br>';
else if (in_array("MODER", $groups))
    echo 'Должность: <font style="color: blue;">Модератор</font><br>';
if (cluServer::$account) {
    echo '<select id="adm_select" class="std-select">';
    if (cluServer::$account->getBooleanGroup('MODER')) {
        if (!in_array("READONLY", $groups))
            echo '<option value="givereadonly">Выдать ReadOnly</option>';
        else
            echo '<option  value="unreadonly">Снять ReadOnly</option>';
        if ($results['banid'] == 0)
            echo '<option  value="giveban">Заблокировать</option>';
        else
            echo'  <option  value="unban">Разблокировать</option>';
    }
    if (cluServer::$account->getBooleanGroup('ADM')) {
        if (!in_array("MODER", $groups))
            echo '<option value="givemoder">Выдать права модератора</option>';
        else
            echo '<option  value="unmoder">Снять модератора</option>';
        if (!in_array("COMMENT", $groups))
            echo '<option value="givecomment">Выдать комментатора</option>';
        else {
            echo '<option  value="uncomment">Снять комментатора</option>';
            if (!in_array("POSTS", $groups)) {
                echo '<option  value="ainvite">Пригласить (права админа)</option>';
            }
        }
    }
    if (cluServer::$account->getBooleanGroup('SUPERUSER')) {
        if (!in_array("MODER", $groups))
            echo '<option value="giveadmin">Выдать права администратора</option>';
        else
            echo '<option  value="unadmin">Снять администратора</option>';
    }
    ?>
    <script>
        function admin_panel()
        {
            var elm = $('#adm_select')[0];
            apirequest("?type=adminapi&action=" + elm.value + "&id=" +<?= $model['id'] ?>);
        }
    </script>
    </select><input type="submit" onclick="admin_panel();" value="Выполнить"><br>
    <?php
}
echo 'Все группы:<br><div id=groups>';
if (in_array("ADM", $groups)) {
    echo '<div class="priveleg">Администратор</div>';
}
if (in_array("MODER", $groups)) {
    echo '<div class="priveleg">Модератор</div>';
}
if (in_array("ACC", $groups)) {
    echo '<div class="priveleg">Пользователь</div>';
}
if (in_array("COMMENT", $groups)) {
    echo '<div class="priveleg">Комментатор</div>';
}
if (in_array("POSTS", $groups)) {
    echo '<div class="priveleg">Автор</div>';
}
if (in_array("READONLY", $groups)) {
    echo '<div class="priveleg">Только чтение</div>';
}
if (in_array("SUPERUSER", $groups)) {
    echo '<div class="priveleg">Суперпользователь</div>';
}
if (in_array("NOLOGIN", $groups)) {
    echo '<div class="priveleg">Запрет входа</div>';
}
echo '</div><br>';
?>
<br>
