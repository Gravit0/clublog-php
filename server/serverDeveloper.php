<?php

// #Ulin Studio
// #ULIN TECH
// #Cluster Group 2015-2016
// #редактировать только разработчикам
//
//
//
//
////BLOCK AUTH
//
//
cluServer::$connect = cluConnect::createThisConnect();
if (BLOCK_DONT_VALID_IP == true) {
    if (cluServer::$connect->isInvalid()) {
        exit();
    }
}
if (BLOCK_IPV6 == true) {
    if (cluServer::$connect->getIpVersion() == cluConnect::IPV6) {
        exit();
    }
}
if (BLOCK_IPV4 == true) {
    if (cluServer::$connect->getIpVersion() == cluConnect::IPV4) {
        exit();
    }
}
//DATA
if (SET_PHP_TIME_ZONE == true) {
    date_default_timezone_set(PHP_TIME_ZONE);
}
if (AUTO_CREATE_CLUDATE == true) {
    cluServer::getDate();
}
//
//LOCALE
if (SET_PHP_LOCALE == true)
    setlocale(LC_TIME, PHP_LOCALE);
//
//STANDART HEADER
if (GEN_HEADERS_CLUSERVER == true) {
    header('cluServer: ' . NameServer);
    header('cluSeverData: ' . DATE);
    if (SET_PHP_ENCODING == true) {
        header('cluServerEncode: ' . PHP_ENCODE);
    }
    header('cluVersion: ' . Version);
    header('cluVersionData: ' . VersionData);
}
//
//DONT CACHE, DOCUMENT EXPIRED
if (autodontcache == true) {
    if (DONT_DOCUMENT_EXPIRED == true) {
        header('Cache-Control: max-age=900, no-store, no-cache, must-revalidate');
        ini_set('session.cache_limiter', 'public');
        session_cache_limiter(false);
    } else {
        header('Cache-Control: no-store, no-cache, must-revalidate');
    }
    header('Expires: ' . date('r'));
} else {
    if (DONT_DOCUMENT_EXPIRED == true) {
        header('Cache-Control: max-age=900');
        ini_set('session.cache_limiter', 'public');
        session_cache_limiter(false);
    }
}
//
//ENCODING
if (SET_PHP_ENCODING == true) {
    header('Content-type: text/html; charset=' . PHP_ENCODE);
}
//
if (PHP_VISIBLE_ERRORS == true) {
    if (PHP_VISIBLE_ALL_ERRORS == true) {
        cluServer::setVisibleErrors(true, true);
    } else {
        cluServer::setVisibleErrors(true, false);
    }
} else {
    cluServer::setVisibleErrors(false, false);
}

function getUserBrowser($user_agent = null) {
    //return array('TEST', '0.2');
    $boolNull = $user_agent == null;
    if ($boolNull) {
        if (isset($GLOBALS['USER_BROWSER']['N'])) {
            return $GLOBALS['USER_BROWSER']['N'];
        }
        $user_agent = $_SERVER['HTTP_USER_AGENT'];
    } else {
        if (isset($GLOBALS['USER_BROWSER'][$user_agent]))
            return $GLOBALS['USER_BROWSER'][$user_agent];
    }
    if (!$user_agent)
        return false;
    //$GLOBALS['USER_BROWSER'][$user_agent] = array('BY', '0,2');
    //return $GLOBALS['USER_BROWSER'][$user_agent];
    $browser = array();
    $version = array();
    $bytes = unpack('C*', $user_agent);
    $i = count($bytes);
    $cScoba = 0; //()
    $a; //ACTIV
    // TEST )55(
    //while($i>0){
    if ($bytes[$i] == 32) { //САМЫЙ ПЕРВЫЙ SPACE
        $i = $i - 1;
        while ($i > 0) { //DEL SPACE TEST() SPACE SPACE
            $a = $bytes[$i];
            if ($a == 32) {
                $i = $i - 1;
                continue;
            }
            break;
        }
    }
    while ($i > 0) {
        $a = $bytes[$i];
        $i = $i - 1;
        //
        if ($cScoba == 0) {
            if ($a == 47)
                break;  // TEST/55
            if ($a == 92)
                break; // TEST\55
        }
        if ($a == 40) { // (
            $cScoba = $cScoba - 1;
            if ($cScoba == 0)
                break; // END )
        }else
        if ($a == 41) { // )
            $cScoba = $cScoba + 1;
            if ($cScoba == 1)
                continue; //START TEST (
        }
        $version[] = $a;
    }
    if ($bytes[$i] == 32) { //есть space разделитель
        $i = $i - 1;
        while ($i > 0) { //DEL SPACE TEST() SPACE SPACE
            $a = $bytes[$i];
            if ($a == 32) {
                $i = $i - 1;
                continue;
            }
            break;
        }
    }
    while ($i > 0) {
        $a = $bytes[$i];
        $i = $i - 1;
        if ($a == 32)
            break; // SPACE
        if ($a == 41)
            break; // )
        $browser[] = $a;
    }
    //}
    //STR
    $versionStr = '';
    $browserStr = '';
    //переворот массива и создание строки
    $i = count($version);
    if ($i > 0) {
        array_reverse($version); //ПЕРЕВОРОТ
        while ($i > 0) {
            $i = $i - 1;
            $versionStr .= chr($version[$i]);
        }
    }
    $i = count($browser);
    if ($i > 0) {
        array_reverse($browser); //ПЕРЕВОРОТ
        while ($i > 0) { //COUNT 0 1 2 3 = 4
            $i = $i - 1; //ИЗА PHP
            $browserStr .= chr($browser[$i]);
        }
        if ($browserStr == 'OPR')
            $browserStr = 'Opera';
    }
    //echo 'BROWSER:'.$browserStr.' VERSION:'.$versionStr;
    //exit();
    if ($boolNull) {
        $GLOBALS['USER_BROWSER']['N'] = array($browserStr, $versionStr);
        return $GLOBALS['USER_BROWSER']['N'];
    }
    $GLOBALS['USER_BROWSER'][$user_agent] = array($browserStr, $versionStr);
    return $GLOBALS['USER_BROWSER'][$user_agent];
}

function createUrlLocation($url, $array) {
    $str = $url;
    if (is_array($array) && $array) {
        $start = false;
        foreach ($array as $n => $v) {
            if ($start == false) {
                $str .= '?' . htmlentities(urlencode($n)) . '=' . htmlentities(urlencode($v));
                $start = true;
                continue;
            }
            $str .= '&' . htmlentities(urlencode($n)) . '=' . htmlentities(urlencode($v));
        }
    }
    return $str;
}

function createStrLocation($type, $array) {
    if (!$type)
        return false;
    $str = '?type=' . htmlentities(urlencode($type));
    if (is_array($array) && $array) {
        foreach ($array as $n => $v) {
            if (!$n)
                continue;
            $str .= '&' . htmlentities(urlencode($n)) . '=' . htmlentities(urlencode($v));
        }
    }
    return $str;
}

function createHeaderLocation($type, $array = null) {
    $str = createStrLocation($type, $array);
    if (!$str)
        return false;
    header('Location: ' . $str);
    exit();
    return true;
}

//
// #Ulin Studio
// #ULIN TECH
// #Cluster Group 2015-2017
?>
