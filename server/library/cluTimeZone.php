<?php
function timeConvert($time, $belt1) {
    if (!$belt1 || !$time || $time == '0000-00-00 00:00:00')
        return $time;
    //if(getBooleanZoneBelt($bel1) == false)return $time;
    if (isset($GLOBALS['TIMEZONEOBJDEF']) == false) {
        try {
            $GLOBALS['TIMEZONEOBJDEF'] = new DateTimeZone(date_default_timezone_get());
        } catch (Exception $e) {
            createCrash()->default1(RBad,'cluTimeZone: bad server zone \'' . $belt1 . '\'');
        }
    }
    if (isset($GLOBALS['CLUTIMEDONTCACHER']) == false) {
        if (isset($GLOBALS['TIMEZONEOBJS'][$belt1]) == false) {
            try {
                $GLOBALS['TIMEZONEOBJS'][$belt1] = new DateTimeZone($belt1);
            } catch (Exception $e) {
                //startCrash(null, RBad, 'cluTimeZone: bad zone \''.$belt1.'\'', false);
                return $time;
            }
        }
        $time = new DateTime($time, $GLOBALS['TIMEZONEOBJDEF']);
        $time->setTimezone($GLOBALS['TIMEZONEOBJS'][$belt1]);
    } else {
        $time = new DateTime($time, $GLOBALS['TIMEZONEOBJDEF']);
        try {
            $time->setTimezone(new DateTimeZone($belt1));
        } catch (Exception $e) {
            //startCrash(null, RBad, 'cluTimeZone: bad zone \''.$belt1.'\'', false);
            return $time;
        }
    }
    return $time->format('Y-m-d H:i:s');
}
function timeServerConvert($belt1) {
    return timeConvert(DATE, $belt1);
}
function getTimeZonesCount() {
    return count(getTimeZones());
}
function getTimeZones() {
    if (isset($GLOBALS['TIMEZONES']) == false) {
        $GLOBALS['TIMEZONES'] = array(
                    'AFRICA' => array('index' => DateTimeZone::AFRICA, 'str' => 'Временные зоны Африки'),
                    'AMERICA' => array('index' => DateTimeZone::AMERICA, 'str' => 'Временные зоны Америки'),
                    'ANTARCTICA' => array('index' => DateTimeZone::ANTARCTICA, 'str' => 'Временные зоны Антарктики'),
                    'ARCTIC' => array('index' => DateTimeZone::ARCTIC, 'str' => 'Временные зоны Арктики'),
                    'ASIA' => array('index' => DateTimeZone::ASIA, 'str' => 'Временные зоны Азии'),
                    'ATLANTIC' => array('index' => DateTimeZone::ATLANTIC, 'str' => 'Временные зоны Атлантики'),
                    'AUSTRALIA' => array('index' => DateTimeZone::AUSTRALIA, 'str' => 'Временные зоны Австралии'),
                    'EUROPE' => array('index' => DateTimeZone::EUROPE, 'str' => 'Временные зоны Европы'),
                    'INDIAN' => array('index' => DateTimeZone::INDIAN, 'str' => 'Временные зоны Индии'),
                    'PACIFIC' => array('index' => DateTimeZone::PACIFIC, 'str' => 'Временные зоны Тихого океана'),
                    'UTC' => array('index' => DateTimeZone::UTC, 'str' => 'Временная зона UTC')//,
                //'ALL'=>array('index' => DateTimeZone::ALL, 'str' => 'Все временные зоны')
        );
    }
    return $GLOBALS['TIMEZONES'];
}
function getIndexTimeZone($name) {
    $arr = getTimeZones();
    if (!$arr)
        return false;
    if (isset($arr[$name]) == false)
        return false;
    return $arr[$name];
}
//function getBooleanZoneBelt($zone){
//	if($zone)return true;
//	if(isset($GLOBALS['BELTCACHE'][$zone])){
//		echo 'cacheBOOLBELT ';
//		return $GLOBALS['BELTCACHE'][$zone];
//	}
//	$belts = getAllZoneBelts();
//	if(!$belts)return false;
//	foreach($belts as $v){
//		if($v == $zone){
//			$GLOBALS['BELTCACHE'][$zone] = true;
//			
//			return true;
//		}
//	}
//	$GLOBALS['BELTCACHE'][$zone] = false;
//	return false;
//}
function addFlagDontCluTimeCreateCache() {
    $GLOBALS['CLUTIMEDONTCACHER'] = true;
}
function removeFlagDontCluTimeCreateCache() {
    unset($GLOBALS['CLUTIMEDONTCACHER']);
}
function getZoneBelts($zone = null) {
    if (!$zone)
        return false;
    $v = DateTimeZone::listIdentifiers($zone);
    if (!$v)
        return false;
    //sort($v);
    return $v;
}
function getAllZoneBelts() {
    return DateTimeZone::listIdentifiers();
}
function getTimeBeltCount() {
    return count(DateTimeZone::listIdentifiers());
}
function getServerTimeZoneStr() {
    return date_default_timezone_get();
}
function setUserTimeZone($account, $timeZone) {
    if ($account->isGuest())
        return false;
    $update = $account->umysql->prepare('UPDATE users SET timeZone = :timeZone WHERE id =:id Limit 1;');
    $update->bindParam(':timeZone', $timeZone, PDO::PARAM_STR);
    $update->bindParam(':id', $account->getArray()['id'], PDO::PARAM_INT);
    $update->execute();
    return $account->setArray('timeZone', $timeZone);
}
function getDataView($sec_birthday) {
    // Сегодняшняя дата
    $sec_now = time();
    // Подсчитываем количество месяцев, лет
    for ($time = $sec_birthday, $month = 0; $time < $sec_now; $time = $time + date('t', $time) * 86400, $month++) {
        $rtime = $time;
    }
    $month = $month - 1;
    // Количество лет
    $year = intval($month / 12);
    // Количество месяцев
    $month = $month % 12;
    // Количество дней
    $day = intval(($sec_now - $rtime) / 86400);
    $result = declination($year, 'год', 'года', 'лет') . ' ' . declination($month, 'месяц', 'месяца', 'месяцев') . ' ' . declination($day, 'день', 'дня', 'дней');
    return $result;
}
function declination($num, $one, $ed, $mn, $notnumber = false) {
    if (($num == '0') or ( ($num >= '5') and ( $num <= '20')) or preg_match('|[056789]$|', $num))
        if (!$notnumber)
            return $num . ' ' . $mn;
        else
            return $mn;
    if (preg_match('|[1]$|', $num))
        if (!$notnumber)
            return $num . ' ' . $one;
        else
            return $one;
    if (preg_match('|[234]$|', $num))
        if (!$notnumber)
            return $num . ' ' . $ed;
        else
            return $ed;
}
