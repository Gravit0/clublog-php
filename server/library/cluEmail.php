<?php
function initMail() {
    if (isset($GLOBALS['MAILINIT']))
        return false;
    $GLOBALS['MAILINIT'] = true;
    if (SENDGRID == true) {
        $GLOBALS['MAILARRAY'] = array('api_user' => SENDGRID_USER_NAME, 'api_key' => SENDGRID_USER_PASS, 'from' => DEFAULT_EMAIL_STR);
        $GLOBALS['MAILCURL'] = curl_init('https://api.sendgrid.com/api/mail.send.json');
        curl_setopt($GLOBALS['MAILCURL'], CURLOPT_POST, true);
        curl_setopt($GLOBALS['MAILCURL'], CURLOPT_HEADER, false);
        curl_setopt($GLOBALS['MAILCURL'], CURLOPT_RETURNTRANSFER, true);
        if (SENDGRID_TEST_VALID == true) {
            $GLOBALS['MAILTESTVALID'] = '{"message":"success"}';
        }
        return true;
    }
    $GLOBALS['HEADERSEMAIL'] = 'MIME-Version: 1.0' . "\r\n";
    $GLOBALS['HEADERSEMAIL'] .= 'Content-type: text/html; charset=' . EncodingServer . "\r\n";
    /* дополнительные шапки */
    $GLOBALS['HEADERSEMAIL'] .= 'From: ' . DEFAULT_EMAIL_NAME_STR . ' <' . DEFAULT_EMAIL_STR . '>' . "\r\n";
    //$str .= 'Cc: '.DEFAULT_EMAIL_STR."\r\n";
    //$str .= 'Bcc:'.DEFAULT_EMAIL_STR."\r\n";
    $GLOBALS['HEADERSEMAIL'] .= 'Reply-To: ' . DEFAULT_EMAIL_STR . "\r\n";
    return true;
}
function closeMail() {
    if (isset($GLOBALS['MAILINIT']) == false)
        return false;
    unset($GLOBALS['MAILINIT']);
    if (SENDGRID == true) {
        curl_close($GLOBALS['MAILCURL']);
        unset($GLOBALS['MAILARRAY']);
        unset($GLOBALS['MAILCURL']);
        unset($GLOBALS['MAILTESTVALID']);
        return true;
    }
    unset($GLOBALS['HEADERSEMAIL']);
    return true;
}
function mailToEmail($account, $email, $name, $str) {
    if (isset($GLOBALS['MAILINIT']) == false)
        initMail();
    if (!$email || !$name || !$str)
        return false;
    if ($account->isGuest())
        return false;
    return mailToEmailDontCluAccount($email, $name, $str);
}
function mailToEmailDontCluAccount($email, $name, $str) {
    if (isset($GLOBALS['MAILINIT']) == false)
        initMail();
    if (!$email || !$name || !$str)
        return false;
    if (SENDGRID == true) {
        $GLOBALS['MAILARRAY']['subject'] = $name;
        $GLOBALS['MAILARRAY']['html'] = $str;
        $GLOBALS['MAILARRAY']['to'] = $email;
        //$params = array(
        //     'api_user' => SENDGRID_USER_NAME,
        //     'api_key' => SENDGRID_USER_PASS,
        //      'to' => $email,
        //     'subject' => $name,
        //     'html' => $str,
        //     'from' => DEFAULT_EMAIL_STR,
        //);
        curl_setopt($GLOBALS['MAILCURL'], CURLOPT_POSTFIELDS, $GLOBALS['MAILARRAY']);
        if (SENDGRID_TEST_VALID == true) {
            return $GLOBALS['MAILTESTVALID'] == curl_exec($GLOBALS['MAILCURL']);
        }
        $t = curl_exec($GLOBALS['MAILCURL']);
        return true;
    }
    return mail($email, $name, $str, $GLOBALS['HEADERSEMAIL']);
}
