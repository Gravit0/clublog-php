<?php
function initCache() {
    if (cacheEnable == false)
        return null;
    $memcache = new Memcache;
    $memcache->connect(cacheMemcacheIP, cacheMemcachePort);
    if ($memcache) {
        $newcache = new cacheEngine;
        $newcache->memcache = $memcache;
        return $newcache;
    } else
        return null;
}
class cacheEngine {
    public $memcache;
    function getVersion() {
        return $this->memcache->getVersion();
    }
    function getStats() {
        return $this->memcache->getStats();
    }
    function set($key, $value, $lifetime = cacheStdTime) {
        $this->memcache->set(cacheMainPrefix . $key, $value, false, $lifetime);
    }
    function update($key, $newlifetime = cacheStdTime) {
        //$this->memcache->touch(cacheMainPrefix.$key, $lifetime);
    }
    function clearall() {
        $this->memcache->flush();
    }
    function add($key, $value, $lifetime = cacheStdTime) {
        $this->memcache->add(cacheMainPrefix . $key, $value, false, $lifetime);
    }
    function get($key) {
        return $this->memcache->get(cacheMainPrefix . $key);
    }
    function delete($key) {
        return $this->memcache->delete(cacheMainPrefix . $key);
    }
}
;
