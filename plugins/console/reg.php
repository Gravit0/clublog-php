<?php
function generateStr($length = 8){
  $chars = 'abdefhiknrstyzABDEFGHKNQRSTYZ23456789';
  $numChars = strlen($chars);
  $string = '';
  for ($i = 0; $i < $length; $i++) {
    $string .= substr($chars, rand(1, $numChars) - 1, 1);
  }
  return $string;
}
class reg extends Plugin {
    public $library = array(LibDecodeEncode,'cluEmail');
	function run($arguments){
        //echo 1;
        $login = $arguments['login'];
        $name = $arguments['name'];
        $pass = $arguments['pass'];
        $pass2 = $arguments['pass2'];
        $email = $arguments['email'];
        if(!$login || !$pass || !$pass2 || !$email || !$name)
        {
            returnNotice('0','Не все поля заполнены','blue');
            cluServer::stop();
        }
        if($pass !== $pass2)
        {
            returnNotice('0','Пароли не совпадают','blue');
            cluServer::stop();
        }
        if(!$arguments['rec'])
        {
            returnNotice('0','Проверка на бота не пройдена','blue');
            cluServer::stop();
        }
        if(strpos($email,'@') === false)
        {
            returnNotice('0','Email некорректен','blue');
            cluServer::stop();
        }
        $keystr = generateStr(64);
        $result = 'Кто-то с IP адресом '.getConnectIp();
        $result .= ' указал ваш EMail адрес в форме регистрации аккаунта.<br>';
        $result .= 'Логин: ';
        $result .= $login;
        $result .= '<br>Если Вы этого не делали, просим Вас проигнорировать это письмо.';
        $result .= '<br>Если же Вы хотите подтвердить этот Email перейдите по ссылке: <a href="http:\\\\site.gravithome.ru\\?type=confirmemail&key='.$keystr.'">Подтвердить</a>';
        if(mailToEmailDontCluAccount($email,'Подтверждение аккаунта на '.NameServer,$result))
        {
            $mysql = new cluMysql();
            $cache = $mysql->prepare('INSERT INTO `addUser` (`email`, `keyStr`, `pass`, `name`, `login`) 
                VALUES (:email, :keyStr, :pass, :name, :login); ');
            $cache->bindParam(':email',$email, PDO::PARAM_STR);
            $cache->bindParam(':keyStr', $keystr, PDO::PARAM_STR);
            $cache->bindParam(':name', $name, PDO::PARAM_STR);
            $cache->bindParam(':login', $login, PDO::PARAM_STR);
            $cache->bindParam(':pass', password_hash($pass,PASSWORD_DEFAULT), PDO::PARAM_STR);
            $cache->execute();
            returnOK();
        }
        else
        {
            returnNotice('0','Не удалось отправить Email','blue');
            cluServer::stop();
        }
	}
}
