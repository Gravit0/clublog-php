var nofastcomment = false;
function api_updaterating(postid,boolea)
{
    var url;
    if(boolea == true)
    {
        url="?type=blogapi&command=rating_up&id="+postid;
    }
    else
    {
        url="?type=blogapi&command=rating_down&id="+postid;
    }
    $.ajax({
        type: 'POST',
        url: "index.php"+url+"&spacePlugins=console",
        success: function(data){
            if(jsonparser(data)){
                var $s = $("#rating");
                var s = $s[0];
                if(boolea == true)
                {
                    s.innerText = +s.innerText + 1;
                }
                else
                {
                    s.innerText = +s.innerText - 1;
                }
                if(+s.innerText > 0) $s.css('color','green');
                else if(+s.innerText < 0) $s.css('color','red');
                else $s.css('color','black');
            }
    }
    });
}
function api_sendcomment(pageid)
{
    if(nofastcomment)
    {
        noticeSetup('Попробуйте отправить комментарий позже.','blue');
        return;
    }
    var _login = document.getElementById('newcomment');
    var url = '?type=blogapi&command=newcomment&page='+pageid+'&comment='+_login.value;
    nofastcomment=true;
    setInterval(function() {
        nofastcomment=false;
    },5000);
    apirequest(url);
}

function api_deletecomment(commentid)
{
    var url = "?type=blogapi&command=comment_rm&id="+commentid;
    $.ajax({
        type: 'POST',
        url: "index.php"+url+"&spacePlugins=console",
        success: function(data){
            if(jsonparser(data)){
                var s = $("font.commentid").each(function() {
                    var th = $( this )[0];
                    if(+th.innerText == commentid)
                        th.parentElement.parentElement.remove();
                });
                var d = $("#comment_num")[0];
                d.innerText = +d.innerText - 1;
            }
    }
    });
}
