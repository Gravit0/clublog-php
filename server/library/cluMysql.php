<?php
class cluMysql extends PDO {
    function __construct($MysqlHost = MYSQL_HOST, $DbName = MYSQL_DB, $DbLogin = MYSQL_LOGIN, $DbPass = MYSQL_PASS) {
        if (MYSQL_VISIBLE_EW == true) {
            ini_set('mysql.trace_mode', 1);
        }
        if (SET_MYSQL_ENCODING == true && MYSQL_CACHE_CONNECT == true) {
            $options = array(PDO:: MYSQL_ATTR_INIT_COMMAND => 'SET NAMES ' . MYSQL_ENCODE,
                PDO::ATTR_PERSISTENT => true
            );
        } else
        if (SET_MYSQL_ENCODING == true) {
            $options = array(PDO:: MYSQL_ATTR_INIT_COMMAND => 'SET NAMES ' . MYSQL_ENCODE);
        } else
        if (MYSQL_CACHE_CONNECT == true) {
            $options = array(PDO::ATTR_PERSISTENT => true);
        }
        try {
            parent::__construct('mysql:host=' . $MysqlHost . ';dbname=' . $DbName, $DbLogin, $DbPass, $options);
        } catch (Exception $e) {
            cluServer::loadLibrary('cluAccount', false);
            cluAccount::killMyConnect();
            createCrash()->SQLError(RNCM,null,null);
        }
        if (SET_MYSQL_TIME_ZONE == true) {
            $this->setTimeZone(MYSQL_TIME_ZONE);
        }
    }
    function setTimeZone($zone) {
        $this->exec('SET time_zone = "' . $zone . '";');
        return true;
    }
    function optimisationTables() {
        $tables = $this->query('SHOW TABLES;');
        //ПРИВОЖУ В НОРМАЛЬНЫЙ ВИД
        $tables = $tables->fetchAll();
        if (!$tables)
            return false;
        $n = '';
        foreach ($tables as $v) {
            $n = $n . 'OPTIMIZE TABLE `' . $v[0] . '`; ';
        }
        if ($n) {
            $this->query($n);
            return true;
        }
        return false;
    }
    function getStatus($account) {
        if ($this->getBooleanConnection() == false)
            return false;
        if ($account->isGuest())
            return false;
        $temp = $this->query('SHOW TABLE STATUS;');
        $temp = $temp->fetchAll();
        if (!$temp)
            return false;
        return $temp;
    }
}
