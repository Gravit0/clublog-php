<?php
class Logger
{
    public $file;
    function __construct()
    {
        $this->file = fopen(HomeDirectory. 'runtime/' . 'server.log','a');
    }
    function __destruct()
    {
        fclose($this->file);
    }
    function log($text,$rate=null,$category=null)
    {
        $result = date('m.d.y H:i:s') . ' ';
        if($rate) $result .= '{'.$rate.'}';
        if($category) $result .= "[$category]";
        $result .= $text;
        $result .= "\n";
        fwrite($this->file,$result);
    }
    function err($text,$category=null)
    {
        $result = date('m.d.y H:i:s') . '{E}';
        if($category) $result .= "[$category]";
        $result .= $text;
        $result .= "\n";
        fwrite($this->file,$result);
    }
}
