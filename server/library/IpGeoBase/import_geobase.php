<?php
set_time_limit(0); // указываем, чтобы скрипт не ограничивался временем по умолчанию
ignore_user_abort(1); // указываем, чтобы скрипт продолжал работать даже при разрыве
cluServer::loadLibrary('cluAccount');
$account = cluAccount::getAccount();
if(file_exists(HomeDirectory.DirLibrary.'/IpGeoBase/cities.txt') == false && file_exists(HomeDirectory.DirLibrary.'/IpGeoBase/cidr_optim.txt') == false){
	exit('Ошибка! Файлов нет.');
}
$temp = $account->umysql->getConnection()->prepare('
	TRUNCATE TABLE `geo__cities`,
	TRUNCATE TABLE `geo__base`
');
$temp->execute();
// проверяем наличие файла cities.txt в папке рядом с этим скриптом
$file = file(HomeDirectory.DirLibrary.'/IpGeoBase/cities.txt');
$pattern = '#(\d+)\s+(.*?)\t+(.*?)\t+(.*?)\t+(.*?)\s+(.*)#';
foreach ($file as $row){
	$row = iconv('windows-1251', 'utf-8', $row);
	if(preg_match($pattern, $row, $out)){
		$temp = $account->umysql->getConnection()->prepare("INSERT INTO `geo__cities` (`city_id`, `city`, `region`, `district`, `lat`, `lng`) VALUES('$out[1]', '$out[2]', '$out[3]', '$out[4]', '$out[5]', '$out[6]')");
		$temp->execute();
	}        
}   
$file = file(HomeDirectory.DirLibrary.'/IpGeoBase/cidr_optim.txt');
$pattern = '#(\d+)\s+(\d+)\s+(\d+\.\d+\.\d+\.\d+)\s+-\s+(\d+\.\d+\.\d+\.\d+)\s+(\w+)\s+(\d+|-)#';
foreach ($file as $row){
	if(preg_match($pattern, $row, $out)){
		$temp = $account->umysql->getConnection()->prepare("INSERT INTO `geo__base` (`long_ip1`, `long_ip2`, `ip1`, `ip2`, `country`, `city_id`) VALUES('$out[1]', '$out[2]', '$out[3]', '$out[4]', '$out[5]', '$out[6]')");
		$temp->execute();
	}        
}
exit('OK');
?>
