var global_initNotices = false;
var global_initPopup = false;
var global_showPopup = false;
var DirImages = 'data/img/';
function initNotices()
{
    var elm = document.body;
    var child = document.createElement('div');
    child.setAttribute('id','notices');
    elm.appendChild(child);
    global_initNotices = true;
}
function initPopupWindow()
{
    var elm = document.body;
    var child = document.createElement('div');
    child.setAttribute('id','popup');
    child.setAttribute('style','display: none;');
    elm.appendChild(child);
    global_initPopup = true;
}
function showPopupWindow(text)
{
    if(global_initPopup == false) initPopupWindow();
    var $elm = $('#popup');
    var elm = $elm[0];
    var result = '<img id="popupclose" onclick="hidePopupWindow()" src="'+DirImages+'del.png">';
    elm.innerHTML = result+text;
    $elm.css('display','');
    global_showPopup = true;
}
function hidePopupWindow()
{
    if(global_initPopup == false) return;
    var $elm = $('#popup');
    var elm = $elm[0];
    elm.innerHTML = '';
    $elm.css('display','none');
    global_showPopup = false;
}
function noticeSetup(text,color=null)
{
    if(global_initNotices == false) initNotices();
    var result = '<div class="noticesetup" ';
    if(color!=null)
    {
        result+='style="background-color: '+color+'"';
    }
    result+='>'+text+'</div>';
    var $r = $(result);
    $r.on('click', function(){
        $r.remove();
    });
    $r.appendTo('#notices');
}
function actionuniversal(method,url)
{
    $.ajax({
        method: method,
        url: url,
        success: function(data){
        if(data!="OK")
                noticeSetup(data);
            else
                location.reload();
    }
    });
}
function jsonparser(dojson)
{
            var json = null;
            try {
                 json =  JSON.parse(dojson);
            }
            catch(e)
            {
                noticeSetup(dojson);
                console.log(e);
                return false;
            }
            if(json.key!='1')
            {
                if(json.err!=null)
                    noticeSetup(json.err.text,json.err.color);
                else
                    noticeSetup('Ошибка '+json.key,'blue');
                //noticeSetup(xhr.responseText);
                return false;
            }
            else
                return true;
}
function apirequest(url)
{
    $.ajax({
        type: 'POST',
        url: "index.php"+url+"&spacePlugins=console",
        success: function(data){
        if(jsonparser(data))
             location.reload();
    }
    });
}
function apirequest_func(url,func)
{
    $.ajax({
        type: 'POST',
        url: "index.php"+url+"&spacePlugins=console",
        success: function(data){
            if(jsonparser(data))
                func();
    }
    });
}
