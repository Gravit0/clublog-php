<?php
class confirmemail extends Plugin {
    public $library = array(LibDecodeEncode,'cluEmail');
	function run($arguments){
        $visible = initVisible();
        $visible->setName('Подтверждение аккаунта');
        $keyStr = $arguments['key'];
        if(!$keyStr)
        {
            echo 'Что-то пошло не так :(';
            cluServer::stop();
        }
        $mysqld = new cluMysql();
        $results = $mysqld->prepare('SELECT * FROM addUser WHERE `keyStr` = :name LIMIT 1;');
        $results->bindParam(':name', $keyStr, PDO::PARAM_STR);
        $results->execute();
        $results = $results->fetchAll(PDO::FETCH_ASSOC);
        if($results)
        {
            $results = $results[0];
            $mysql = $mysqld->prepare("INSERT INTO `users` (
					`group` ,
					`name` ,
					`nameUser` ,
					`pass` ,
					`timeZone` ,
					`karma`,
					`email`
					)
					VALUES (
					'', :login,:name, :pass, 'Asia/Barnaul', '0', :email
					);");
            $mysql->bindParam(':name', $results['name'], PDO::PARAM_STR);
            $mysql->bindParam(':login', $results['login'], PDO::PARAM_STR);
            $mysql->bindParam(':pass', password_hash($results['pass'],PASSWORD_DEFAULT), PDO::PARAM_STR);
            $mysql->bindParam(':email', $results['email'], PDO::PARAM_STR);
            $mysql->execute();
            $mysql = $mysqld->prepare('DELETE FROM `addUser` WHERE `id` = :id');
            $mysql->bindParam(':id',$results['id'], PDO::PARAM_STR);
            $mysql->execute();
            echo 'Почта успешно подтвержена. Теперь Вы можете авторизироватся';
        }
	}
}
