<?php
function decode($ss) {
    if ($ss == null || !$ss)
        return null;
    $s = unpack('C*', $ss);
    $i = 1;
    $max = count($s) + 1;
    $result = null;
    $name = '';
    $buffer = '';
    while ($i != $max) {
        $c = 0; //[
        $typeB = false;
        $type = 0;
        while ($i != $max) { // сборка буфера имени, "text["
            $a = $s[$i];
            $i = $i + 1;
            if ($a == 64) {
                $ta = $s[$i];
                if ($ta != 91) {
                    $type = $ta;
                    $typeB = true;
                    $i = $i + 1;
                    continue;
                }
            } else
            if ($a == 92 && $i != $max) { //поддержка экрана \
                $ta = $s[$i];
                //                    if(ta==91){
                //                        c = c + 1;
                //                    }
                //                    if(ta==93){
                //                        c = c - 1;
                //                    }
                if ($ta == 91 || $ta == 93 || $ta == 92 || $ta == 64) {
                    $name .= pack('C*', $ta);
                    $i = $i + 1;
                    continue;
                }
            } else
            if ($a == 91) { // [
                $c = $c + 1;
                if ($c == 1)
                    break;
            }
            $name .= pack('C*', $a);
        }
        if ($c == 0) { //ошибка символ [ не был найден
            //echo 'ERR';
            return $result;
        }
        //BUFFER
        $dopRascoding = false;
        $replacer = false; // INT \[
        while ($i != $max) {
            $a = $s[$i];
            $i = $i + 1;
            if ($a == 92) { //поддержка экрана \, есть символы значит раскодировку подедрживать и поддерживать []
                $ta = $s[$i];
                //                    if(ta == 91){
                //                        c = c + 1;
                //                    }
                //                    if(ta == 93){
                //                        c = c - 1;
                //                    }
                if ($ta == 91 || $ta == 92 || $ta == 93) {
                    $buffer .= pack('C*', 92);
                    $buffer .= pack('C*', $ta);
                    $replacer = true;
                    $i = $i + 1;
                    continue;
                }
            } else
            if ($a == 91) { //[
                $c = $c + 1;
                $dopRascoding = true;
            } else
            if ($a == 93) { //]
                $c = $c - 1;
                if ($c == 0) { // закрыто
                    if ($result == null)
                        $result = array();
                    if ($dopRascoding) {
                        $result[$name] = decode($buffer);
                        //result.setCluObjects(name.toString(), decode(buffer.toByteArray()));
                    } else {
                        if ($replacer) {
                            $buffByte = unpack('C*', $buffer);
                            $buffer = '';
                            $maxB = count($buffByte) + 1;
                            $replacerBuff = '';
                            $y = 1;
                            while ($y != $maxB) {
                                $aB = $buffByte[$y];
                                $y = $y + 1;
                                if ($aB == 92 && $y != $maxB) { //поддержка экрана \
                                    $ta = $buffByte[$y];
                                    if ($ta == 91 || $ta == 93 || $ta == 92) {
                                        $replacerBuff .= pack('C*', $ta);
                                        $y = $y + 1;
                                        continue;
                                    }
                                }
                                $replacerBuff .= pack('C*', $aB);
                            }
                            $buffer = '';
                            $buffer = $replacerBuff;
                            $replacerBuff = '';
                        }
                        if ($typeB == false) {
                            $result[$name] = $buffer;
                            //result.setString(name.toString(), buffer.toString());
                        } else {
                            if ($type == 49) { //BOOLEAN
                                if (count($buffer) == 1 && $buffer && unpack('C*', $buffer)[1] == 49) {
                                    $result[$name] = true;
                                } else
                                    $result[$name] = false;
                            }else if ($type == 76) { // LONG
                                $result[$name] = (float) $buffer;
                            } else if ($type == 68) { //DOUBLE
                                $result[$name] = (double) $buffer;
                            } else if ($type == 73) { //INT
                                $result[$name] = (int) $buffer;
                            } else
                                $result[$name] = $buffer;
                        }
                    }
                    break;
                }
            }
            $buffer .= pack('C*', $a);
        }
        $name = '';
        $buffer = '';
        //name.reset();
        //buffer.reset();
    //
		}
    //print_r($result);
    return $result;
}
function encode($arr, $tipizatia = true) {
    if ($arr == null || is_array($arr) == false)
        return false;
    $buffer = '';
    foreach ($arr as $n => $v) {
        $buffer .= str_replace(array('\\', '@', '[', ']'), array('\\\\', '\@', '\[', '\]'), $n);
        if ($tipizatia == true) {
            if (is_bool($v)) {
                $buffer .= '@1';
            } else if (is_int($v)) {
                $buffer .= '@I';
            } else if (is_float($v)) {
                $buffer .= '@L';
            } else if (is_double($v)) {
                $buffer .= '@D';
            }
        }
        if (is_array($v)) {
            $arr = encode($v, $tipizatia);
            if ($arr != false) {
                $buffer .= '[' . $arr . ']';
            }
        } else {
            if ($tipizatia == true) {
                if (is_bool($v)) {
                    if ($v == true) {
                        $buffer .= '[1]';
                    } else
                        $buffer .= '[]';
                } else
                    $buffer .= '[' . str_replace(array('\\', '[', ']'), array('\\\\', '\[', '\]'), $v) . ']';
            }else {
                if (is_bool($v)) {
                    if ($v == true) {
                        $buffer .= '[true]';
                    } else
                        $buffer .= '[false]';
                } else
                    $buffer .= '[' . str_replace(array('\\', '[', ']'), array('\\\\', '\[', '\]'), $v) . ']';
            }
        }
    }
    if (!$buffer)
        return false;
    return $buffer;
}
function code($arr, $tipizatia = true) {
    return encode($arr, $tipizatia);
}
