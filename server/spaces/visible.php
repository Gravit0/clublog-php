<?php

// #ULIN PROJECT
// #ULIN TECH
class visibleData {

    //ADD
    public $iconMiniBlock;
    public $visibleUserMiniBlock;
    //
    //DEFAULT
    public $name;
    public $icon;
    public $heads;
    public $style;
    public $bodyImg;
    public $loadPHPLibrary;
    public $loadCSSLibrary;
    public $loadJSLibrary;
    private $_name;
    private $_overdata;

    function render($name, $overdata = null) {
        $this->_name = $name;
        $this->_overdata = $overdata;
    }

    function renderHead() {
        if (VAddEncodingServer == true) {
            echo '<meta http-equiv="content-type" content="text/html; charset=' . PHP_ENCODE . '" />';
        }
        $name = $this->getName();
        if ($name) {
            $name .= ' - ' . NameServer;
        } else {
            $name = NameServer;
        }
        echo '<title>' . $name . '</title>';
        if (VUseOldJQuery) {
            if (VUseDebugJQuery) {
                echo '<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.js"></script>>';
            } else {
                echo '<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>';
            }
        } else {
            if (VUseDebugJQuery) {
                echo '<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.js"></script>';
            } else {
                echo '<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>';
            }
        }
        if ($this->heads) {
            foreach ($this->heads as $v) {
                echo $v;
            }
            $this->heads = null;
        }
        if ($this->loadCSSLibrary) {
            foreach ($this->loadCSSLibrary as $v) {
                $css = DirCSSLibrary . $v . '.css';
                echo '<link type="text/css" rel="stylesheet" href="./' . $css . '">';
            }
            $this->loadCSSLibrary = null;
        }
        if ($this->loadJSLibrary) {
            foreach ($this->loadJSLibrary as $v) {
                $js = DirJavaScriptJQuery . $v . '.js';
                echo '<script src="./' . $js . '"></script>';
            }
            $this->loadJSLibrary = null;
        }
    }

    function renderBody() {
        $resultHtml = ob_get_contents();
        ob_end_clean();
        echo $resultHtml;
        $this->renderView($this->_name, $this->_overdata);
    }

    function renderView($name, $overdata = null, $runcrashwarning = RUN_CRASH_TEST_VPLUGIN == true) {
        $loadPHP = true;
        if ($this->loadPHPLibrary) {
            foreach ($this->loadPHPLibrary as $v) {
                if ($v == $name) {
                    $loadPHP = false;
                    break;
                }
            }
        }
        $boolResultPHP = false;
        //$file = HomeDirectory.DirVLibrary.$name;
        if ($loadPHP) {
            $file = HomeDirectory . DirView . $name;
            if ($overdata) {
                extract($overdata, EXTR_OVERWRITE);
            }
            if (@include($file . '.php')) {
                $this->loadPHPLibrary[] = $name;
                $boolResultPHP = true;
            }
        }
        if ($boolResultPHP == false && $boolResultCSS == false && $boolResultJS) {
            if ($runcrashwarning) {
                createCrash()->plugin(RBad, $name, 'The vlibrary not exists');
                return $this;
            }
        }
        return $this;
    }

    //
    public function initData($loadIndex = true, $loadVLibrary = true) {
        if ($loadIndex) {
            $this->loadLibrary('index');
        }
        if ($loadVLibrary) {
            if (cluServer::$plugin) {
                if (cluServer::$plugin->vlibrary) {
                    if (is_array(cluServer::$plugin->vlibrary)) {
                        foreach (cluServer::$plugin->vlibrary as $v) {
                            $this->loadLibrary($v);
                        }
                    } else {
                        $this->loadLibrary(cluServer::$plugin->vlibrary);
                    }
                    unset(cluServer::$plugin->vlibrary);
                }
            }
        }
    }

    public function createData() {
        if (class_exists('cluAccount')) {
            if (cluAccount::getBooleanConnect()) {
                $account = cluAccount::getAccount();
                if ($account) {
                    $cache = &$account->getArray();
                } else {
                    $cache = null;
                }
            } else {
                $account = null;
                $cache = null;
            }
        } else {
            $account = null;
            $cache = null;
        }
        //$title = createTitleStr($this, $cache, $account);
        $this->renderView('main');
        if (ADD_VISIBLE_COMMENT == true) {
            echo '<!--' . ADD_VISIBLE_COMMENT_STR . '-->';
        }
        return '';
    }

    //UPGRADE
    function clear($obClean = true) {
        //ADD
        $this->iconMiniBlock = null;
        $this->visibleUserMiniBlock = null;
        //
        $this->name = null;
        $this->icon = null;
        $this->heads = null;
        $this->style = null;
        $this->bodyImg = null;
        if ($this->loadCSSLibrary) {
            foreach ($this->loadCSSLibrary as $n => $v) {
                if ($v == 'index')
                    continue;
                unset($this->loadCSSLibrary[$n]);
            }
            if (!$this->loadCSSLibrary)
                $this->loadCSSLibrary = null;
        }
        if ($this->loadJSLibrary) {
            foreach ($this->loadJSLibrary as $n => $v) {
                if ($v == 'index')
                    continue;
                unset($this->loadJSLibrary[$n]);
            }
            if (!$this->loadJSLibrary)
                $this->loadJSLibrary = null;
        }
        //$this->loadPHPLibrary = null;
        if ($obClean) {
            ob_clean();
        }
        return $this;
    }

    //
    function setName($name) {
        $this->name = $name;
        return $this;
    }

    function getName() {
        return $this->name;
    }

    function setIconUserMiniBlock($icon) {
        $this->iconMiniBlock = $icon;
        return $this;
    }

    function getIconUserMiniBlock() {
        return $this->iconMiniBlock;
    }

    //
    function setVisibleUserMiniBlock($visible) {
        $this->visibleUserMiniBlock = $visible;
        return $this;
    }

    function getVisibleUserMiniBlock() {
        return $this->visibleUserMiniBlock;
    }

    //
    //DEFAULT
    function addHead($head) {
        if ($head) {
            $this->heads[] = $head;
        }
        return $this;
    }

    function addStyle($style) {
        if ($style) {
            $this->style[] = $style;
        }
        return $this;
    }

    function setBodyImg($img) {
        $this->bodyImg = $img;
        return $this;
    }

    function setIcon($icon) {
        $this->icon = $icon;
    }

    function getIcon() {
        return $this->icon;
    }

    function loadPluginCss($name = null, $crasher = RUN_CRASH_TEST_VPLUGIN == true) {
        if ($name) {
            return $this->loadLibrary('plugins/plugin_' . $GLOBALS['PLUGINACTIV'] . '_' . $name, $crasher);
        } else {
            return $this->loadLibrary('plugins/plugin_' . $GLOBALS['PLUGINACTIV'], $crasher);
        }
    }

    function loadJSLibrary($name, $runcrashwarning = RUN_CRASH_TEST_VPLUGIN == true) {
        if ($this->loadJSLibrary) {
            foreach ($this->loadJSLibrary as $v) {
                if ($name == $v) {
                    if ($runcrashwarning == true) {
                        startCrash(null, RBad, $name . ', The vlibrary was already connected');
                    }
                    return $this;
                }
            }
        }
        $file = HomeDirectory . DirVLibrary . $name;
        if (file_exists($file . '.js')) {
            $this->loadJSLibrary[] = $name;
        }
        return $this;
    }

    function loadCSSLibrary($name, $runcrashwarning = RUN_CRASH_TEST_VPLUGIN == true) {
        if ($this->loadCSSLibrary) {
            foreach ($this->loadCSSLibrary as $v) {
                if ($name == $v) {
                    if ($runcrashwarning == true) {
                        startCrash(null, RBad, $name . ', The vlibrary was already connected');
                    }
                    return $this;
                }
            }
        }
        $file = HomeDirectory . DirVLibrary . $name;
        if (file_exists($file . '.css')) {
            $this->loadCSSLibrary[] = $name;
        }
        return $this;
    }

    function loadLibrary($name, $runcrashwarning = RUN_CRASH_TEST_VPLUGIN == true) {
        $loadPHP = true;
        $loadCSS = true;
        $loadJS = true;
        if ($this->loadPHPLibrary) {
            foreach ($this->loadPHPLibrary as $v) {
                if ($v == $name) {
                    $loadPHP = false;
                    break;
                }
            }
        }
        if ($this->loadCSSLibrary) {
            foreach ($this->loadCSSLibrary as $v) {
                if ($v == $name) {
                    $loadCSS = false;
                    break;
                }
            }
        }
        if ($this->loadJSLibrary) {
            foreach ($this->loadJSLibrary as $v) {
                if ($v == $name) {
                    $loadJS = false;
                    break;
                }
            }
        }
        $boolResultPHP = false;
        $boolResultCSS = false;
        $boolResultJS = false;
        if ($loadPHP) {
            try {
                $file = HomeDirectory . DirVLibrary . $name;
                if (@include($file . '.php')) {
                    $this->loadPHPLibrary[] = $name;
                    $boolResultPHP = true;
                }
            } catch (Exception $e) {
                
            }
        }
        if ($loadCSS) {
            $file = HomeDirectory . DirCSSLibrary . $name;
            if (file_exists($file . '.css')) {
                $this->loadCSSLibrary[] = $name;
                $boolResultCSS = true;
            }
        }
        if ($loadJS) {
            $file = HomeDirectory . DirJavaScriptJQuery . $name;
            if (file_exists($file . '.js')) {
                $this->loadJSLibrary[] = $name;
                $boolResultJS = true;
            }
        }
        if ($boolResultPHP == false && $boolResultCSS == false && $boolResultJS) {
            if ($runcrashwarning) {
                startCrash(null, RBad, $name . ', The vlibrary not exists');
                return $this;
            }
        }
        return $this;
    }

    function getBooleanLoadLibrary($lib) {
        if ($this->loadPHPLibrary) {
            if (in_array($lib, $this->loadPHPLibrary) == true)
                return true;
        }
        if ($this->loadCSSLibrary) {
            if (in_array($lib, $this->loadCSSLibrary) == true)
                return true;
        }
        if ($this->loadJSLibrary) {
            if (in_array($lib, $this->loadJSLibrary) == true)
                return true;
        }
        return false;
    }

    function getPHPArrayLibrary() {
        return $this->loadPHPLibrary;
    }

    function getCSSArrayLibrary() {
        return $this->loadCSSLibrary;
    }

    function getJSArrayLibrary() {
        return $this->loadJSLibrary;
    }

    function getArrayLibrary() {
        $array = array();
        if ($this->loadPHPLibrary) {
            foreach ($this->loadPHPLibrary as $v) {
                $array[] = $v;
            }
        }
        if ($this->loadCSSLibrary) {
            foreach ($this->loadCSSLibrary as $v) {
                $array[] = $v;
            }
        }
        if ($this->loadJSLibrary) {
            foreach ($this->loadJSLibrary as $v) {
                $array[] = $v;
            }
        }
        array_unique($array);
        return $array;
    }

    //
}

// #ULIN PROJECT 2017
	//