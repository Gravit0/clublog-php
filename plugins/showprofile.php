<?php
class showprofile extends Plugin {
    public $library = array();
    function run($arguments) {
        cluServer::$data->setName('Профиль');
        cluServer::$data->LoadLibrary('showprofile');
        $account = null;
        if (cluAccount::getConnectUnique()) {
            $account = cluAccount::getAccount();
        }
        if (!isset($arguments['userid'])) {
            if ($account)
                $arguments['userid'] = $account->getArray()['id'];
            else
                $arguments['userid'] = AnonymousID;
        }
        $mysql = new cluMysql();
        $results = $mysql->prepare("SELECT * FROM users WHERE `id` = :name LIMIT 1;");
        $results->bindParam(':name', $arguments['userid'], PDO::PARAM_STR);
        $results->execute();
        $results = $results->fetchAll(PDO::FETCH_ASSOC);
        //echo '<pre>'.print_r($results).'</pre>';
        if ($results) {
            //echo '<link href="css/showprofile.css" rel="stylesheet" type="text/css">';
            $results = $results['0'];
            cluServer::$data->render('user/show', ['model' => $results]);
        } else {
        }
    }
}
