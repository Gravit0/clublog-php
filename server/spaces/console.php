<?php

// #ULIN PROJECT
// #ULIN TECH
function returnNotice($code, $text, $color) {
    cluServer::$data->setAdd(array('key' => $code, 'err' => array('text' => $text, 'color' => $color)));
    return true;
}

function returnOK() {
    cluServer::$data->setKey(ROK);
    return true;
}

class consoleData implements ArrayAccess {

    private $key;
    private $arg;
    private $add;

    public function setKey($v) {
        $this->key = $v;
        return $this;
    }

    public function setArg($v) {
        $this->arg = $v;
        return $this;
    }

    public function setAdd($v) {
        $this->add = $v;
        return $this;
    }

    public function getKey() {
        return $this->key;
    }

    public function getArg() {
        return $this->arg;
    }

    public function getAdd() {
        return $this->add;
    }

    public function offsetExists($name) {
        //СУЩ
        //
		if ($name == 'key') {
            if ($this->key)
                return true;
            return false;
        }
        if ($name == 'arg') {
            if ($this->arg)
                return true;
            return false;
        }
        if ($name == 'add') {
            if ($this->add)
                return true;
            return false;
        }
        return isset($this->add[$name]);
    }

    public function offsetSet($name, $v) {
        //СЕТИНГ
        //
		if ($name == 'key') {
            $this->key = $v;
            return true;
        }
        if ($name == 'arg') {
            $this->arg = $v;
            return true;
        }
        if ($name == 'add') {
            $this->add = $v;
            return true;
        }
        if (!$name) {
            $this->add[] = $v;
            return true;
        }
        $this->add[$name] = $v;
        return true;
    }

    public function offsetUnset($name) {
        //УДАЛЕНИЕ
        //
		if ($name == 'key') {
            $this->key = null;
            return true;
        }
        if ($name == 'arg') {
            $this->arg = null;
            return true;
        }
        if ($name == 'add') {
            $this->add = null;
            return true;
        }
        unset($this->add[$name]);
        return false;
    }

    public function offsetGet($name) {
        //ПОЛУЧЕНИЕ
        //
		if ($name == 'key') {
            return $this->key;
        }
        if ($name == 'arg') {
            return $this->arg;
        }
        if ($name == 'add') {
            return $this->add;
        }
        return $this->add[$name];
    }

    public function clear() {
        $this->key = null;
        $this->arg = null;
        $this->add = null;
    }

    public function initData() {
        
    }

    public function createData($createCodeStr = true) {
        $result = array();
        if ($this->key) {
            $result['key'] = $this->key;
        }
        if ($this->arg) {
            $result['arg'] = $this->arg;
        }
        if ($this->add) {
            if (isset($this->add['key']))
                unset($this->add['key']);
            if (isset($this->add['arg']))
                unset($this->add['arg']);
        }
        if ($this->add) {
            if ($result) {
                foreach ($this->add as $name => $v) {
                    $result[$name] = $v;
                }
            } else {
                $result = $this->add;
            }
        }
        if (!$result)
            $result = array('key' => RNull);
        if ($createCodeStr) {
            cluServer::loadLibrary(LibDecodeEncode, false);
            return code($result);
        }
        return $result;
    }

}

?>
