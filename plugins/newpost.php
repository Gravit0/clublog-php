<?php
class newpost extends Plugin {
    public $library = array();
    function run($arguments) {
        $account = null;
        if (cluAccount::getBooleanConnect())
            $account = cluAccount::getAccount();
        else {
            $visible = initVisible();
            $visible->setName('Блог');
            $visible->loadLibrary('blog');
            print403Error();
            cluServer::stop();
        }
        if (!$account->getBooleanGroup('POSTS')) {
            $visible = initVisible();
            $visible->setName('Блог');
            $visible->loadLibrary('blog');
            print403Error();
            cluServer::stop();
        }
        if (!isset($arguments['text'])) {
            $visible = initVisible();
            $visible->setName('Новый пост');
            $visible->LoadLibrary('blog');
            cluServer::$data->render('blog/newpost');
            cluServer::stop();
        }
        $accarray = $account->getArray();
        //$browser = getUserBrowser();
        //$account = cluAccount::createAuthAccount($arguments['login'], $arguments['pass'], $browser[0], $browser[1]);
        if ($account->GetBooleanGroup("READONLY")) {
            echo 'Ваш аккаунт имеет статус ReadOnly. Вам комментировать запрещено.';
        } else {
            $mysqld = $account->umysql;
            $mysql = null;
            $mysql = $mysqld->prepare('INSERT INTO `blog` (
					`id_user` ,
					`text` ,
					`brieftext` ,
					`header`
					)
					VALUES (
					:id, :text, :minitext, :header
					);');
            print_r($arguments);
            $text = str_replace("\n", '<br>', $arguments['text']);
            $mysql->bindParam(':text', $text, PDO::PARAM_STR);
            $mysql->bindParam(':id', $accarray['id'], PDO::PARAM_INT);
            $mysql->bindParam(':minitext', $arguments['minitext'], PDO::PARAM_STR);
            $mysql->bindParam(':header', $arguments['header'], PDO::PARAM_STR);
            $mysql->execute();
            header('Location: ?type=blog');
        }
    }
}
