<?php
require('server/config/web.php'); // загрузить настройки
require('server/config/db.php'); // загрузить настройки
require('server/configLoader.php'); // загрузить настройки
require('server/cluServer.php'); // загрузить default void
require('server/serverDeveloper.php');
require('server/autoload.php');
require('vendor/autoload.php');
//cluServer::LoadLibrary('cluAccount');
if(cluAccount::getBooleanConnect())
{
    cluServer::$account = cluAccount::getAccount();
}
//cluServer::LoadLibrary('cluMysql');
//cluServer::LoadLibrary('autoload');
$arguments = null;
if(empty($_GET) == false){
foreach($_GET as $name=>$v){
 $arguments[$name] = $v;
}
unset($_GET);
}
if(empty($_POST) == false){
foreach($_POST as $name=>$v){
 $arguments[$name] = $v;
}
unset($_POST);
}
if (isset($arguments['type']) == false) {
    $loadPlugin = defaultPlugin;
} else {
    $loadPlugin = $arguments['type'];
    if (TEST_VALID_PLUGIN == true) {
        if (empty($loadPlugin) == false) {
            if (strrpos($loadPlugin, DIRECTORY_SEPARATOR) !== false) {
                if (RUN_CRASH_TEST_VALID_PLUGIN == true) {
                    createCrash()->plugin(RBadLP,$loadPlugin,'invalid type');
                } else {
                    $loadPlugin = defaultPlugin;
                }
            }
        }
    }
    unset($arguments['type']);
}
if (isset($arguments['spacePlugins']) == false) {
    if (E_CONSOLE_PLUGINS == true) {
        ob_start();
        require(HomeDirectory . DirSpaces . 'console' . '.php');
        if (cluServer::loadPlugin(PASC,$loadPlugin, HomeDirectory . DirConsolePlugins . $loadPlugin . '.php', $arguments) != null) {
            cluServer::stop();
        }
    }
} else {
    $_COOKIE['spacePlugins'] = $arguments['spacePlugins'];
    unset($arguments['spacePlugins']);
    if ($_COOKIE['spacePlugins'] == PASC || $_COOKIE['spacePlugins'] == 'console') { // PLUGIN ACTIV SPACE CONSOLE
        if (E_CONSOLE_PLUGINS == true) {
            ob_start();
            require(HomeDirectory . DirSpaces . 'console' . '.php');
            if (cluServer::loadPlugin(PASC,$loadPlugin, HomeDirectory . DirConsolePlugins . $loadPlugin . '.php', $arguments) != null) {
                cluServer::stop();
            }
        }
    } else
    if ($_COOKIE['spacePlugins'] == PASC_MULTY || $_COOKIE['spacePlugins'] == 'multiConsole') {
        if (E_MULTI_CONSOLE_PLUGINS == true) {
            require(HomeDirectory . DirSpaces . 'console' . '.php');
            cluServer::loadLibrary(LibDecodeEncode);
            $loadPlugin = explode(',', $loadPlugin);
            $result = initResult();
            $resultArr = array();
            $i = -1;
            foreach ($loadPlugin as $v) {
                $i = $i + 1;
                if ($arguments[$i]) {
                    $args = encode($arguments[$i]);
                } else {
                    $args = null;
                }
                if (class_exists($v) == false) {
                    $file = HomeDirectory . DirConsolePlugins . $v . '.php';
                } else {
                    $file = null;
                }
                ob_start();
                if (cluServer::loadPlugin(PASC,$v, $file, $args) != null) {
                    $args = $result->createResult();
                    $result->clear();
                    if (!$args) {
                        $args = array('key' => RNull);
                    }
                    $resultArr[$i] = $args;
                    $args = null;
                }
                ob_end_clean();
            }
            if (!$resultArr) {
                $result->setKey(RNull);
            } else {
                $result->setKey(ROK);
                $result->setAdd($resultArr);
            }
            cluServer::stop();
            return;
        }
    }
}
createCrash()->plugin(RBadLP,$loadPlugin,'The plugin is not loaded');
