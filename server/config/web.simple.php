<?php
// #Cluster Group 2015-2016
// #ULIN PROJECT
//ИНФОРМАЦИЯ О СЕРВЕРЕ
const Version = '0.6.5/0.8.1.2 DEV-OFFICIAL'; //
const VersionData = '2017-03-22 14:14:00';
const NameServer = 'cluBlog Test Server';
const LibDecodeEncode = 'jsonED'; //По умолчанию библиотека парсера, распарсера
const defaultPlugin = 'index'; //ПЛАГИН ЗАГРУЖАЕМЫЙ ПРИ ОТСУТСТВИИ АРГУМЕНТА TYPE
#ini_set('memory_limit', '256M'); // НАСТРОЙТЕ ОГРАНИЧЕНИЕ ИЛИ ЗАКОМЕНТИРУТЕ
const autodontcache = true; //запрещяет кэшировать данные html css, заголовки http, включать лишь тем кто испытывает проблемы
const CONNECT_FORCED_HTTPS = false; //перенаправление http => https
define('URL','http://'.$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI']); //для работы многих cluPlugins требуется эта константа
//const URL = 'https://online-diary.tk'; //для работы многих cluPlugins требуется эта константа
//const URL = 'http://127.0.0.1/diary2';
//const URL = 'http://localhost/diary2';
const DATE = '';
//ВАЖНО, ПРАВИЛЬНЫЙ URL, ОТПРАВЛЯЕТСЯ В EMAIL И ИНЫЕ ДАННЫЕ
const BLOCK_IPV4 = false; //БЛОКИРОВКА ВХОДЯЩИХ ИЗ IPV4
const BLOCK_IPV6 = false; //БЛОКИРОВКА ВХОДЯЩИХ ИЗ IPV6
const BLOCK_DONT_VALID_IP = false; //БЛОКИРОВКА IP НЕ ПОДХОДЯЩИЕ ПОД IPV4-6 (ПРОИЗВОДИТСЯ ТЕСТ НА ВАЛИД)
//ТРЕБУЕТСЯ ОПРЕДЕЛЯТЬ ВЕРСИЮ IPV
//const BLOCK_DONT_VALID_IP = true; //БЛОКИРОВКА IP НЕ ПОДХОДЯЩИЕ ПОД IPV4-6 (ПРОИЗВОДИТСЯ ТЕСТ НА ВАЛИД)
//const TEST_THIS_IPV = true; //СОЗДАВАТЬ КОНСТАНТУ thisIPV в которой будет тип соединения
//ПРИХОД ФУНКЦИИ getUserIpVersion
const ADD_VISIBLE_COMMENT = true; //КОММЕНТАРИЙ В HTML КОНЦЕ СТРАНИЦЫ
const ADD_VISIBLE_COMMENT_STR = 'Gravit project 2017. cluServer Core by #Ulin.';
//ДОПОЛНЕНИЯ
	//require('./server/serverDiary.php');
//
//TIME
const LIB_SPACE_CONSOLE = 'console';
const LIB_SPACE_SERVER = 'console';
const LIB_SPACE_VISIBLE = 'visible';
const PHP_TIME_ZONE = 'Europe/Minsk'; //ВРЕМЯ СЕРВЕРА
const MYSQL_TIME_ZONE = 'Europe/Minsk'; //ВРЕМЯ MYSQL
//Europe/Moscow
//Europe/Minsk
//Europe/London
const SET_MYSQL_TIME_ZONE = true; //ПРИНУДИТЕЛЬНО УСТАНОВИТЬ ВРЕМЯ MYSQL СЕРВЕРА
const SET_PHP_TIME_ZONE = true; //ПРИНУДИТЕЛЬНО УСТАНОВИТЬ ВРЕМЯ PHP СЕРВЕРА
const PHP_LOCALE_HTML = 'ru';
const PHP_LOCALE = 'ru_RU.utf8'; //ЛОКАЛЬ СЕРВЕРА
const SET_PHP_LOCALE = false;
//ru_RU.UTF-8
//RU
//russian
//locale -a | grep ru
//MYSQL
const DirSpaces = 'server/spaces/';
const MYSQL_VISIBLE_EW = true; //ВИДИМОСТЬ MYSQL ОШИБОК
const MYSQL_CACHE_CONNECT = true; //СОЗДАВАТЬ ПОСТОЯННОЕ СОЕДИНЕНИЕ, ПРИ ПОСЛЕДУЮЩЕМ ЗАПУСКЕ СКРИПТА MYSQL БУДЕТ ПРОСТО КЕШИРОВАТСЯ
//
//КОДИРОВКИ (cp1251) (utf8)
const MYSQL_ENCODE = 'UTF8'; //КОДИРОВКА MYSQL
const PHP_ENCODE = 'UTF-8'; //КОДИРОВКА СЕРВЕРА
const VAddEncodingServer = true; //Добавлять дополнительный meta Заголовок с нужной кодировкой сервера
//ЕСЛИ ИСПОЛЬЗУЕТСЯ EncodingHeaderServer МОЖНО ВЫКЛЮЧИТЬ, НО ЛУЧШЕ ОСТАВИТЬ)
const SET_MYSQL_ENCODING = true; //МЕНЯТЬ КОДИРОВКУ MYSQL
const SET_PHP_ENCODING = true; //ОТПРАВЛЯТЬ HTTP ЗАГОЛОВОК О КОДИРОВКЕ СЕРВЕРА PHP СЕРВЕРА
//БОЛЬШЕ НЕ ТРЕБУЕТСЯ ФАЙЛ КОНФИГУРАЦИИ АПАЧА .htaccess AddDefaultCharset UTF-8
//
//АКТИВАЦИЯ ПЛАГИНОВ
const E_CONSOLE_PLUGINS = true;
const E_VISIBLE_PLUGINS = true;
const E_SERVER_PLUGINS = true;
const TEST_VALID_PLUGIN = true; //ТЕСТ НА УЯЗВИМОСТИ TYPE ?type=../server/cluServer
//
//ПУТИ СЕРВВЕРА, константы Dir* должны оканчиватся на /
//define('HomeDirectory', $_SERVER['DOCUMENT_ROOT'].'/'); //ПАПКА ГДЕ НАХОДИТСЯ СЕРВЕР
const VisibleLibrary = 'mvc';
const DirCrasher = 'server/crasher/';
const DirAction = 'server/action/';
const HomeDirectory = '/srv/http/site/'; //ПАПКА ГДЕ НАХОДИТСЯ СЕРВЕР
const DirLibrary = 'server/library/'; //ПАПКА БИБЛИОТЕК СЕРВЕРА
const DirVLibrary = 'server/vlibrary/'; //папка визуальных плагинов
const DirView = 'server/view/'; //папка визуальных плагинов
const DirCSSLibrary = 'data/css/'; //папка визуальных плагинов
const DirImages = 'data/img/'; //папка изображений
const DirJavaScriptJQuery = 'data/js/';
const DirCLibrary = 'server/constants/'; //папка констант приложений
const DirServerPlugins = 'server/plugins/'; //папка сервер плагинов
const DirConsolePlugins = 'plugins/console/'; //папка консольных плагинов
const DirVisiblePlugins = 'plugins/'; //папка визуальных плагинов
// PLUGIN
const TEST_PLUGIN = true; //ПРОВЕРЯТЬ ПЛАГИНЫ НА ПРЕДМЕТ ПОВТОРНОГО ПОДКЛЮЧЕНИЯ
const RUN_CRASH_TEST_PLUGIN = false; //В СЛУЧАЕ ПОВТОРНОГО ПОДКЛЮЧЕНИЯ БИБЛИОТЕК КРАШИТЬ СЕРВЕР
//
// VISUAL_PLUGIN
const TEST_VPLUGIN = true; //ПРОВЕРЯТЬ VПЛАГИНЫ НА ПРЕДМЕТ ПОВТОРНОГО ПОДКЛЮЧЕНИЯ
//В ПРОТИВНОМ СЛУЧАЕ ПРИ КАЖДОМ LOADVLIB БУДУТ ЗАПРАШИВАТСЯ ПОВТОРНО CSS, А В СЛУЧАЕ PHP БУДЕТ КРАШ
const RUN_CRASH_TEST_VPLUGIN = false; //В СЛУЧАЕ ПОВТОРНОГО ПОДКЛЮЧЕНИЯ VБИБЛИОТЕК КРАШИТЬ СЕРВЕР
//
const AnonymousID = 106;
// CONST_PLUGIN
const TEST_CPLUGIN = true; //ПРОВЕРЯТЬ CПЛАГИНЫ НА ПРЕДМЕТ ПОВТОРНОГО ПОДКЛЮЧЕНИЯ
//В ПРОТИВНОМ СЛУЧАЕ ПРИ КАЖДОМ LOADCONSTANT БУДЕТ КРАШ
const RUN_CRASH_TEST_CPLUGIN = true; //В СЛУЧАЕ ПОВТОРНОГО ПОДКЛЮЧЕНИЯ CБИБЛИОТЕК КРАШИТЬ СЕРВЕР
//
const VLibLoadCssReset = false; //ПОДКЛЮЧИТЬ ВСТРОЕНЫЙ RESET CSS, СБРАСЫВАЕТ СТИЛИ БРАУЗЕРОВ
const VUseOldJQuery = false; //Использовать  JQuery 1.хх вместо JQuery 3.xx для поддержки IE 6,7,8
const VUseDebugJQuery = true; //Использовать отладочную версию JQuery
//
const PhpVisibleError = true;
const PhpVisibleNoticeError = true;
const CAPTCHASiteKey = '6LeIxAcTAAAAAJcZVRqyHh71UMIEGNQ_MXjiZKhI';
const CAPTCHASecretKey = '6LeIxAcTAAAAAGG-vFI1TnRWxMZNFuojJ4WifJWe';
//
//VISIBLE ERRORS
//setVisibleServerError(true, true); // ОТОБРАЖАТЬ ОШИБКИ PHP, МОЖНО ОТКЛЮЧИТЬ ИЛИ ВКЛЮЧИТЬ ВО ВРЕМЯ РАБОТЫ КОДА
//второй параметр означает если ошибки включены то отображать ли незначительные ошибки?
//SETTINGS LIBRARY AND PLUGINS, ОБШИРНЫЕ НАСТРОЙКИ БИБЛИОТЕК И ПЛАГИНОВ
const CF_LIBRARY = 'cluGeolocation'; //библиотека геолокации
const CF_CACHE = true; //КЭШИРОВАТЬ ВСЕ ПОЛУЧЕННЫЕ CF
const CF_ERROR_CACHE = true; //КЭШИРОВАТЬ ДАЖЕ ОШИБОЧНЫЕ GEOLOCATION
const CF_HTTP_IP_COUNTRY = false; //ОПРЕДЕЛЯТЬ ГЕОЛОКАЦИЮ НА ОСНОВЕ ЗАГОЛОВКА HTTP_CF_IPCOUNTRY, работает через cdn
//ОПАСТНО, ВОЗМОЖНА ПОДМЕНА ЗАГОЛОВКА
const CF_PRIN_INTERNET = true; //ПОДДЕРЖИВАТЬ ГЕОЛОКАЦИЮ ЧЕРЕЗ ИНТЕРНЕТ
const CF_PRIN_OFFLINE = true; //ПОДДЕРЖИВАТЬ ГЕОЛОКАЦИЮ OFFLINE ТРЕБУЕТСЯ MYSQL ТАБЛИЦЫ
const CLUACCOUNT_CLEAR_THIS_UNIQUE = true; //УДАЛЯТЬ ТЕКУЩИЙ UNIGUE ПРИ ИСПОЛЬЗОВАНИИ EXIT, ЕСЛИ НЕТ ТО CLUACCOUNT СОЗДАЕТ ИСТОРИЮ СОЕДИНЕНИЙ
//помогает держать историю входа через cluAccount, плачевно для маленьких баз
const COMPRESS = false; //РАЗРЕШИТЬ ИСПОЛЬЗОВАТЬ CLUCOMPRESS
const COMPRESS_LIBRARY = 'cluCompress'; //БИБЛИОТЕКА СЖАТИЯ, РАСЖАТИЯ
const COMPRESSTYPE = 'gzip'; //ПО УМОЛЧАНИЮ ЕСЛИ НЕ РАЗОБРАН ТИП
const COMPRESSLVL = 4; //СТЕПЕНЬ СЖАТИЯ, ОТ 0 ДО 9
const DEL_ASCII_IN_HTML_RESULT = true; //РАБОТАЕТ НА VISIBLE ПРОСТРАНСТВЕ, ИСКЛЮЧАЕТ ASCII УПРАВЛЯЮЩИЕ СИМВОЛЫ
const DETECT_IP = false; //АВТОМАТ ОПРЕДЕЛЕНИЕ IP включая прокси, прокси, прокси
const DETECT_IP_HEADER = 'HTTP_CF_CONNECTING_IP'; //ЕСЛИ ОТКЛЮЧЕНО ОПРЕДЕЛЕНИЕ БРАТЬ IP ОТСЮДА
//REMOTE_ADDR - DEFAULT
//HTTP_CF_CONNECTING_IP - CLOUD
//
const DETECT_IP_HEADER_NOTER = true; //ЕСЛИ ЗАГОЛОВКА НЕТУ ИСПОЛЬЗОВАТЬ АВТОМАТ
const GEN_HEADERS_CLUSERVER = false; //создавать дополнительные заголовки, желательно отключить (особенно если вы не желаете использовать version, vversion)
//пример http заголовок cluServer cluServerData cluVersion cluVersionData 
const cluAccountAutoCreate = true; // автоматически подключается к account при присоединении библиотеки cluAccount
//последующие getAccount получат ссылку
//решает проблему с куками (они не сетятся) подробности о http заголовках
//дополнительные плагины или visible плагины не подключатся, это сэкономит время
//создан чтобы в основном избавить проблему с куками
const CLUACCOUNT_KILL_ALL_COOKIE_FOR_EXIT = false;
//уничтожить все куки созданные при использовании cluAccount при выходе
//
const CLUACCOUNT_MIN_TIME_LIFE_COOKIE = 3600*24*365;
//
//
const CLUACCOUNT_MIN_TIME_ONLINE = 160;
//КОДЫ СЕРВЕРА
const ROK = '1'; //ЕСЛИ ВСЕ ОТЛИЧНО И СКРИПТ ВЫПОЛНИЛСЯ
const RBad = ''; //ЕСЛИ ОТВЕТ НЕ ПОЛУЧЕН ИЛИ ПРОИЗОШЛА ОШИБКА
const RARGF = '33'; //НЕДОСТАТОЧНО ПРИСЛАНО АРГУМЕНТОВ
const RNull = '30'; //ОТВЕТ ЕСЛИ ПОЛУЧЕН ПУСТОЙ ОТВЕТ
const RBadLP = '31'; //ОТВЕТ ЕСЛИ СКРИПТ НЕ НАЙДЕН
const RNCM = '40'; //ОТВЕТ ЕСЛИ НЕТ СОЕДИНЕНИЯ С БАЗОЙ ДАННЫХ
const RAUE = '60'; //ОТВЕТ ЕСЛИ ПОЛЬЗОВАТЕЛЬ НЕ АВТОРИЗИРОВАН
const RGE = '61'; //ОТВЕТ ЕСЛИ ПОЛЬЗОВАТЕЛЬ НЕ ИМЕЕЕТ НУЖНЫХ ПРАВ
const RBU = '62'; //ОТВЕТ ЕСЛИ ПОЛЬЗОВАТЕЛЬ ЗАБАНЕН
const PASC_MULTY = 4; //PLUGIN ACTIV SPACE MULTI CONSOLE
const PASV = 3; // PLUGIN ACTIV SPACE VISIBLE
const PASC = 2; // PLUGIN ACTIV SPACE CONSOLE
const PASS = 1; // PLUGIN ACTIV SPACE SERVER
//if(HomeDirectory.$_SERVER['PHP_SELF'] == __FILE__) exit('');
// #Cluster Group 2015-2016
// #ULIN PROJECT
