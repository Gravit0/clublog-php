<?php
//VISIBLE TECH
//
	//
	//#ULIN TECH
//2017
function img_apirequest($imgurl, $apiurl, $styleclass = null, $csstyle = null) {
    $result = '<img src="' . DirImages . $imgurl . '" onclick="apirequest(\'' . str_replace('&', '&amp;', $apiurl) . '\')"';
    if ($styleclass)
        $result = $result . ' class="' . $styleclass . '" ';
    if ($csstyle)
        $result = $result . ' style="' . $csstyle . '" ';
    $result = $result . '>';
    return $result;
}
function link_apirequest($apiurl, $text, $styleclass = null, $csstyle = null) {
    $result = '<a href="#" onclick="apirequest(\'' . str_replace('&', '&amp;', $apiurl) . '\')"';
    if ($styleclass)
        $result = $result . ' class="' . $styleclass . '" ';
    if ($csstyle)
        $result = $result . ' style="' . $csstyle . '" ';
    $result = $result . '>' . $text . '</a>';
    return $result;
}
function link_href($url, $text, $styleclass = null, $csstyle = null) {
    $result = '<a href="' . str_replace('&', '&amp;', $apiurl) . '"';
    if ($styleclass)
        $result = $result . ' class="' . $styleclass . '" ';
    if ($csstyle)
        $result = $result . ' style="' . $csstyle . '" ';
    $result = $result . '>' . $text . '</a>';
    return $result;
}
function img_apirequest_custom($arr) {
    $result = '<img src="' . DirImages . $arr['imgurl'] . '" onclick="' . $arr['func'] . '(' . $arr['param'] . ')"';
    if (isset($arr['class']))
        $result = $result . ' class="' . $arr['class'] . '" ';
    if (isset($arr['style']))
        $result = $result . ' style="' . $arr['style'] . '" ';
    $result = $result . '>';
    return $result;
}
function getBooleanNotificationManager() {
    return isset($GLOBALS['NOTIFICATIONMANAGER']);
}
function getNotificationManager() {
    if (getBooleanNotificationManager()) {
        return $GLOBALS['NOTIFICATIONMANAGER'];
    }
    $GLOBALS['NOTIFICATIONMANAGER'] = new notificationManager();
    return $GLOBALS['NOTIFICATIONMANAGER'];
}
function createMenu($name, $materialIsset, $material, $arguments, $account) {
    //$materialIsset Существование Material, если материалов действительно нет = false
    //если материалы действительно есть = true
    //
		//Дело в том что существующие материалы могут иметь = null
    //А также не собраные материалы могут иметь = null
    //
		//if($name == 'archive'){
    //	loadLibrary('archive', false);
    //	loadLibrary('getRussianMonth', false);
    //	
    //	$addarr = array();
    //	if(isset($arguments['id']) && $arguments['id'])$addarr['id'] = $arguments['id'];
    //	if(isset($arguments['index']) && $arguments['index'])$addarr['index'] = $arguments['index'];
    //	
    //	
    //	//НЕТ ГОДА
    //	if(isset($arguments['year']) == false){ //ТЕКУЩЕЕ МЕНЮ ТРЕБУЕТ ТОЛЬКО YEARS
    //		$years = getUniqueMyYearsArchive($account, $arguments['id']);
    //		if(!$years)return null;
    //		$arr = array();
    //		foreach($years as $v){
    //			$arr[] = array('archive', 'url'=>$addarr+array('year'=>$v['year']), 'str'=>$v['year'].' год');
    //		}
    //		return $arr;
    //	}
    //	
    //	//ЕСТЬ ГОД
    //	if(isset($arguments['m']) == false){
    //		$a = getUniqueMyMonthArchive($account, $arguments['id'], $arguments['year']);
    //		if(!$a)return null;
    //		$arr = array();
    //		
    //		$monthsStrs = getRussianMonthArray();
    //		foreach($a as $v){
    //			$arr[] = array('votmetke', 'url'=>$addarr+array('year'=>$arguments['year'], 'm'=>$v['mesec']), 'str'=>$monthsStrs[$v['mesec']]);
    //		}
    //		return $arr;
    //	}
    //	//ГОД, МЕСЯЦ ЕСТЬ
    //	return null;
    //}
    return null;
}
function print404Error() {
    echo 'Вы попали на несуществующую страницу.<br>Если Вы считаете, что это ошибка - сообщите администратору.';
}
function print403Error() {
    echo 'Вы попали на страницу, к которой у Вас нет доступа.<br>Если Вы считаете, что это ошибка - сообщите администратору.';
}
function createAbolUrl($url, $right = true, $left = false) {
    $abol = getAbolManager();
    $result;
    if ($right != false || $left != false) {
        $arr = array($abol->getAbolHeadersStr());
        if ($right == true) {
            $add = array();
            $add = cluServer::$plugin->args;
            $add[''] = cluServer::$plugin->name;
            $arr[] = encode($add, false);
        } else
        if ($left == true) {
            unset($arr[count($arr) - 1]);
        }
        $result = implode('/', $arr);
    }
    if (is_string($url)) {
        if ($url)
            $url .= '&abol=' . $result;
        else
            $url = '?abol=' . $result;
        return $url;
    }
    if (is_array($url)) {
        $url['abol'] = $result;
        return $url;
    }
    return $url;
}
function printMenuArray(&$result, $array, $abol = false) {
}
function getMenuArray($cache = null, $account = null) {
    return null;
}
function obortDay($number) {
    if (strlen($number) == 1 && $number < 10)
        return '0' . $number;
    return $number;
}
// #ULIN TECH
// ABOL MANAGER
// ABOL TECH
//
	class abolManager {
    private $material;
    public $account;
    public $activMenu;
    private $abolHeaderStr;
    private $abolHeaders;
    //$GLOBALS['ABOLTHISPLUGIN'] -> $this->activMenu
    //$GLOBALS['ABOLTHISMATERIAL'] - > $this->material
    //$menu, $GLOBALS['ABOLTHISPLUGIN'], cluServer::$plugin->name, cluServer::$plugin->args, $account
    function __construct($account = null) {
        cluServer::loadLibrary(LibDecodeEncode, false);
        $this->account = $account;
        if (isset(cluServer::$plugin->args['abol'])) {
            if (cluServer::$plugin->args['abol']) {
                //$GLOBALS['THISABOL'] = $argumentsdef['abol'];
                //unset($argumentsdef['abol']);
                $this->abolHeaderStr = cluServer::$plugin->args['abol'];
                //print_r($this->abolHeaderStr);
                $array = explode('/', $this->abolHeaderStr);
                if ($array) {
                    foreach ($array as $v) {
                        $v = decode($v);
                        if (!$v)
                            break;
                        if (isset($v['']) == false)
                            break;
                        $this->abolHeaders[] = $v;
                    }
                }
            }
            unset(cluServer::$plugin->args['abol']);
        }
        //$temp = $argumentsdef;
        $v = cluServer::$plugin->args;
        //$temp[''] = $pluginactivdef;
        $v[''] = cluServer::$plugin->name;
        //
        $this->abolHeaders[] = $v;
    }
    function getAbolHeaders() {
        return $this->abolHeaders;
    }
    function getAbolHeadersStr() {
        return $this->abolHeaderStr;
    }
    function addMaterial($name, $value) {
        if (isset($this->material[$name]))
            return $this;
        $this->material[$name] = $value;
        return $this;
    }
    function clearMaterial() {
        $this->material = null;
        return $this;
    }
    function setActivMenu($activ) {
        $this->activMenu = $activ;
        return $this;
    }
    function createAbol(&$arrays0) {
        $arrActiv = null;
        $activ = null;
        //ПРЕДЫДУЩИЙ ИДЕТ
        //
			if ($this->abolHeaders) {
            foreach ($this->abolHeaders as $arguments) {
                $namePlugin = $arguments[''];
                unset($arguments['']);
                $boolIssetMaterial = isset($this->material[$namePlugin]);
                if ($boolIssetMaterial) {
                    $v = createMenu($namePlugin, $boolIssetMaterial, $this->material[$namePlugin], $arguments, $this->account);
                } else {
                    $v = createMenu($namePlugin, $boolIssetMaterial, null, $arguments, $this->account);
                }
                if ($v) {
                    //ИЗНАЧАЛЬНО ACTIV == NULL
                    if ($activ == null) { //START
                        $activ = &$this->searchActivForArrayElement($arrays0, $namePlugin, $arguments);
                    } else { //НЕ СТАРТ
                        if (isset($activ['arr'])) {
                            //ПРЕДЫДУЩИЙ ЭЛЕМЕНТ
                            //
									if ($activ['arr']) {
                                $activ = &$this->searchActivForArrayElement($activ['arr'], $namePlugin, $arguments);
                            } else {
                                $activ = null;
                            }
                        } else {
                            $activ = null;
                        }
                    }
                    if ($activ == null) {
                        //ACTIV СБИЛИ, ЗНАЧИТ ЭЛЕМЕНТ НЕ БЫЛ НАЙДЕН
                        //
								break;
                    }
                    if (isset($activ['arr']) && $activ['arr']) {
                        $activ['arr'] = array_merge($activ['arr'], $v);
                    } else {
                        $activ['arr'] = $v;
                    }
                    //СОЕДИНЕНИЕ АКТИВНОГО МЕНЮ
                    //С CREATE MENU
                    //
							$arrActiv[] = &$activ;
                    //ДОБАВЛЯЕМ В МАССИВ АКТИВНЫХ
                //
						} else {
                    //ПОСЛЕДНИЙ ЭЛЕМЕНТ, ОБЯЗАТЕЛЬНО ЕСТЬ ACTIV
                    //НО НЕТ V
                    $activ = &$this->searchActivForArrayElement($activ['arr'], cluServer::$plugin->name, cluServer::$plugin->args);
                    if ($activ) {
                        $arrActiv[] = &$activ;
                    }
                    break;
                    //ОБРЫВ МЕНЮ
                    //НЕЛЬЗЯ...
                }
            }
        }
        if (!$arrActiv) {
            //ЗНАЧИТ ИЗНАЧАЛЬНО МЕНЮ НЕ БЫЛО
            //НЕЛЬЗЯ $activ == NULL, АКТИВ МОГУТ СБИТЬ
            // НАЙТИ НАЧАЛО
            $activ = &$this->searchActivForArrayElement($arrays0, cluServer::$plugin->name, cluServer::$plugin->args);
            if ($activ) {
                $arrActiv[] = &$activ;
            }
            //exit('ERR');
        }
        if (!$arrActiv) {
            return null;
            //АКТИВНОГО МЕНЮ В ДЕФОЛТЕ НЕ НАЙДЕНО...
        }
        $started = true;
        $i = count($arrActiv);
        while ($i > 0) {
            $i = $i - 1;
            $v = &$arrActiv[$i];
            //if(isset($v['noactiv']) == false || $v['noactiv'] == false){
            if ($started && $this->activMenu) {
                $started = false;
                if (isset($v['arr'])) {
                    $v['arr'] = array_merge($v['arr'], $this->activMenu);
                } else {
                    $v['arr'] = $this->activMenu;
                }
                $activ = &$this->searchActivForArrayElement($v['arr'], cluServer::$plugin->name, cluServer::$plugin->args);
                if ($activ) {
                    if (isset($activ['noactiv']) == false || $activ['noactiv'] == false) {
                        //ССЫЛКА НАЙДЕНА И ХОЧЕТ БЫТЬ АКТИВНОЙ
                        $activ['a'] = true;
                    } else {
                        //ССЫЛКА НЕ ХОЧЕТ БЫТЬ АКТИВНОЙ
                        $v['a'] = true;
                    }
                    $arrActiv[] = &$activ;
                    $i = $i + 1;
                    //НЕ СБИВАЙТЕ I
                //
					} else {
                    $v['a'] = true;
                }
            } else {
                if (isset($v['noactiv']) == false || $v['noactiv'] == false) {
                    $v['a'] = true;
                }
            }
        }
        //print_r($arrActiv);
        return $arrActiv;
    }
    private function &searchActivForArrayElement(&$arrays0, $type, $args = null) {
        if (!$arrays0)
            return null;
        $namesNotUrl = null;
        foreach ($arrays0 as &$v) {
            if (isset($v['url']) == false || !$v['url']) {
                if ($namesNotUrl == null) {
                    //МЫ ИЩЕМ ЕДИНСТВЕННОГО НЕ ИМЕЮЩЕГО НАЗВАНИЕ
                    //
						//ЗНАЧЕНИЯ НЕ ИМЕЮЩИЕ URL
                    //
						if ($v[0] == $type) {
                        //СОВПАДЕНИЕ ТЕКУЩЕГО ТИПА, ЧТО-ТО
                        //
							$namesNotUrl = &$v;
                    }
                }
                continue;
            }
            //URL ЕСЬ
            //URL НЕ ПУСТ
            if ($args) {
                //exit('TT');
                if ($v[0] == $type) {
                    //НАПОЛНЯЕМ МАССИВ ТЕКУЩИХ ЭЛЕМЕНТОВ
                    //ПРИОРИТЕТ ВЫШЕ чем у namesNotUrl
                    //
						$ok = true;
                    foreach ($v['url'] as $n => $vv) {
                        if (isset($args[$n]) == false) {
                            $ok = false;
                            break;
                        }
                        if ($args[$n] != $vv) {
                            $ok = false;
                            break;
                        }
                    }
                    if ($ok) {
                        return $v;
                    }
                }
            }
        }
        if ($namesNotUrl) {
            return $namesNotUrl;
        }
        $null = null;
        return $null;
    }
    private function &searchActivForArrayAllElements(&$arrays0, $type, $args = null) {
        $namesNotUrl = null;
        $names = null;
        foreach ($arrays0 as &$v) {
            if (isset($v['url']) == false || !$v['url']) {
                //ЗНАЧЕНИЯ НЕ ИМЕЮЩИЕ URL
                //
					if ($v[0] == $type) {
                    //СОВПАДЕНИЕ ТЕКУЩЕГО ТИПА, ЧТО-ТО
                    //
						//$namesNotUrl[] = &$v;
                    $namesNotUrl[] = &$v;
                }
                continue;
            }
            //URL ЕСЬ
            //URL НЕ ПУСТ
            //if($args){
            if ($v[0] == $type) {
                //НАПОЛНЯЕМ МАССИВ ТЕКУЩИХ ЭЛЕМЕНТОВ
                //ПРИОРИТЕТ ВЫШЕ чем у namesNotUrl
                //
						$ok = true;
                foreach ($v['url'] as $n => $vv) {
                    if (isset($args[$n]) == false) {
                        $ok = false;
                        break;
                    }
                    if ($args[$n] != $vv) {
                        $ok = false;
                        break;
                    }
                }
                if ($ok) {
                    $names[] = &$v;
                }
            }
            //}
        }
        if ($names) {
            return $names;
        }
        if ($namesNotUrl) {
            return $namesNotUrl;
        }
        $null = null;
        return $null;
    }
}
function getAbolManager($account = null) {
    if (isset($GLOBALS['ABOL'])) {
        return $GLOBALS['ABOL'];
    }
    if ($account == null) {
        if (class_exists('cluAccount')) {
            if (cluAccount::getBooleanConnect()) {
                $account = cluAccount::getAccount();
            }
        } else {
            $account = null;
        }
    }
    $GLOBALS['ABOL'] = new abolManager($account);
    return $GLOBALS['ABOL'];
}
class notificationManager {
    public $array;
    //TYPE
    //0 - URL NOTIF
    //1 - STR NOTIF
    //
		function addUrlNotification($url, $header, $value = null, $color = null) {
        $array = array(
            'header' => $header,
            'url' => $url
        );
        if ($color) {
            $array['color'] = $color;
        }
        if ($value) {
            $array['value'] = $value;
        }
        $this->array[] = $array;
        return $this;
        //array('header'=>$str, 'color'=>$color, 'url'=>$url, 'value'=>$arr);
    }
    function addNotification($header, $value = null, $color = null) {
        $array = array(
            'header' => $header,
        );
        if ($color) {
            $array['color'] = $color;
        }
        if ($value) {
            $array['value'] = $value;
        }
        $this->array[] = $array;
        return $this;
    }
    function addImportantNotification($header, $value = null, $color = null) {
        $array = array(
            'header' => $header,
        );
        if ($color) {
            $array['color'] = $color;
        }
        if ($value) {
            $array['value'] = $value;
        }
        if ($this->array) {
            array_unshift($this->array, $array);
        } else {
            $this->array[] = $array;
        }
        return $this;
    }
    function &getArray() {
        return $this->array;
    }
    function clear() {
        $this->array = null;
        return $this;
    }
}
//
	//
	// #ULIN PROJECT
	// #ULIN TECH
	// #ULIN PROJECT ABOL SYSTEM
	// #Cluster Group 2014-2017
