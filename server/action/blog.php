<?php
class blogAction extends Action
{
    static function deleteComment($db, $commentid) {
                    $results = $db->prepare('SELECT * FROM `comments` WHERE `id_comment` = :name LIMIT 1;');
                    $results->bindParam(':name', $commentid, PDO::PARAM_STR);
                    $results->execute();
                    $results = $results->fetchAll(PDO::FETCH_ASSOC);
                    if ($results) {
                        $mysql = $db->prepare('DELETE FROM `comments` WHERE `comments`.`id_comment` = :id');
                        $mysql->bindParam(':id', $commentid, PDO::PARAM_INT);
                        $mysql->execute();
                        if($results['visible'] == 1)
                        {
                            $mysql = $db->prepare('UPDATE `users` SET `comments` = `comments` - 1 WHERE `id` = :iduser');
                            $mysql->bindParam(':iduser', $results[0]['id_user'], PDO::PARAM_INT);
                            $mysql->execute();
                        }
                        return true;
                    }
                    else return false;
    }
    static function acceptComment($db, $commentid) {
                    $results = $db->prepare('SELECT * FROM `comments` WHERE `id_comment` = :name LIMIT 1;');
                    $results->bindParam(':name', $commentid, PDO::PARAM_INT);
                    $results->execute();
                    $results = $results->fetchAll(PDO::FETCH_ASSOC);
                    if ($results) {
                        $mysql = null;
                        $mysql = $db->prepare('UPDATE `comments` SET `visible` = \'1\' WHERE `comments`.`id_comment` = :idcomment');
                        $mysql->bindParam(':idcomment', $commentid['id'], PDO::PARAM_INT);
                        $mysql->execute();
                        $mysql = $db->prepare('UPDATE `users` SET `comments` = `comments` + 1 WHERE `id` = :id');
                        $mysql->bindParam(':id', $results['id_user'], PDO::PARAM_INT);
                        $mysql->execute();
                    }
    }
    static function deletePost($db, $postid) {
                    $mysql = null;
                    $mysql = $db->prepare('DELETE FROM `blog` WHERE `blog`.`id` = :id');
                    $mysql->bindParam(':id', $postid, PDO::PARAM_INT);
                    $mysql->execute();
    }
    static function newComment($db, $pageid, $userid, $commenttext, $visible, $parent = 0) {
                $mysql = $db->prepare("INSERT INTO `comments` (
					`id_comment` ,
					`id_user` ,
					`id_post` ,
					`text` ,
					`visible` ,
					`parent`
					)
					VALUES (
					NULL , :id, :page, :text, :visible, :parent
					);");
                $mysql->bindParam(':text', $commenttext, PDO::PARAM_STR);
                $mysql->bindParam(':page', $pageid, PDO::PARAM_INT);
                $mysql->bindParam(':id', $userid, PDO::PARAM_INT);
                $mysql->bindParam(':visible', $visible, PDO::PARAM_INT);
                $mysql->bindParam(':parent', $parent, PDO::PARAM_INT);
                $mysql->execute();
                if($visible == 1)
                {
                    $mysql = $db->prepare('UPDATE `users` SET `last_comment` = CURRENT_TIMESTAMP WHERE `id` = :id');
                    $mysql->bindParam(':id', $userid, PDO::PARAM_INT);
                    $mysql->execute();
                }
    }
}
