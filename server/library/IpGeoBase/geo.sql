--
-- Структура таблицы `geo__base`
--

CREATE TABLE IF NOT EXISTS `geo__base` (
  `long_ip1` bigint(20) NOT NULL,
  `long_ip2` bigint(20) NOT NULL,
  `ip1` varchar(16) NOT NULL,
  `ip2` varchar(16) NOT NULL,
  `country` varchar(2) NOT NULL,
  `city_id` int(10) NOT NULL,
  KEY `INDEX` (`long_ip1`,`long_ip2`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Структура таблицы `geo__cities`
--

CREATE TABLE IF NOT EXISTS `geo__cities` (
  `city_id` int(10) NOT NULL,
  `city` varchar(128) NOT NULL,
  `region` varchar(128) NOT NULL,
  `district` varchar(128) NOT NULL,
  `lat` float NOT NULL,
  `lng` float NOT NULL,
  PRIMARY KEY (`city_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
