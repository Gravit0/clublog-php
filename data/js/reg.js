function redicretReg()
{
    location.href='?type=index';
}
function reg_reg(rec)
{
    var _login = document.getElementById('reg_login');
    var _name = document.getElementById('reg_name');
    var _pass = document.getElementById('reg_password');
    var _pass2 = document.getElementById('reg_password2');
    var _email = document.getElementById('reg_email');
    if(!rec) return noticeSetup('Проверка CAPTCHA не пройдена', 'red');
    if(!_login.value || !_pass.value || !_pass2.value || !_email.value) return noticeSetup('Заполните все поля!', 'red');
    if(!(_pass.value === _pass2.value)) return noticeSetup('Пароли не совпадают!', 'red');
    if(!(_email.value.indexOf('@') + 1)) return noticeSetup('EMail некорректен!', 'red');
    var url = '?type=reg&login='+_login.value+'&name='+_name.value+'&pass='+_pass.value+'&pass2='+_pass2.value+'&email='+_email.value+'&rec='+rec;
    alert(url);
    apirequest_func(url, redicretReg);
}
global_CAPTCHA = null;
