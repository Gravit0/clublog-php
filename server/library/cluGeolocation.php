<?php
function getGeolocation($clumysql, $ip = IP) {
    if (CF_CACHE == true) {
        if (isset($GLOBALS['geolocation'][$ip]))
            return $GLOBALS['geolocation'][$ip];
    }
    if (CF_HTTP_IP_COUNTRY == true && isset($GLOBALS['HTTP_CF_IPCOUNTRY']) && $GLOBALS['HTTP_CF_IPCOUNTRY']) {
        if (CF_CACHE == true)
            $GLOBALS['geolocation'][$ip] = $GLOBALS['HTTP_CF_IPCOUNTRY'];
        return $GLOBALS['HTTP_CF_IPCOUNTRY'];
    }
    if (CF_PRIN_OFFLINE == true) {
        $long_ip = ip2long($ip);
        if ($long_ip) {
            $temp = $clumysql->prepare('SELECT country FROM `geo__base` WHERE `long_ip1` <= :iplong AND `long_ip2` >= :iplong Limit 1;');
            $temp->bindParam(':iplong', $long_ip, PDO::PARAM_INT);
            $temp->execute();
            $temp = $temp->fetchAll(PDO::FETCH_ASSOC);
            if ($temp) {
                $temp = $temp[0];
                if ($temp) {
                    $temp = $temp['country'];
                    if (CF_CACHE == true)
                        $GLOBALS['geolocation'][$ip] = $temp;
                    return $temp;
                }
            }
        }
    }
    if (CF_PRIN_INTERNET == true) {
        $ch = curl_init('http://ipgeobase.ru:7020/geo?ip=' . $ip);
        curl_setopt($ch, CURLOPT_HEADER, false);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_TIMEOUT, 3);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 3);
        $string = curl_exec($ch);
        curl_close($ch);
        unset($ch);
        if ($string) {
            $string = parse_string($string);
            if ($string && isset($string['country'])) {
                $string = $string['country'];
                if ($string) {
                    if (CF_CACHE == true)
                        $GLOBALS['geolocation'][$ip] = $string;
                    return $string;
                }
            }
        }
    }
    if (CF_CACHE == true && CF_ERROR_CACHE == true)
        $GLOBALS['geolocation'][$ip] = false;
    return false;
}
function parse_string($string) {
    //$params = array('inetnum', 'country', 'city', 'region', 'district', 'lat', 'lng');
    $params = array('country');
    $data = $out = array();
    foreach ($params as $param) {
        if (preg_match('#<' . $param . '>(.*)</' . $param . '>#is', $string, $out)) {
            $data[$param] = trim($out[1]);
        }
    }
    return $data;
}
