<?php
class karma extends Plugin {
    public $library = array(); //ЗАГРУЗКА БИБЛИОТЕК ДЛЯ СКРИПТА
    function run($arguments) {
        $account = null;
        if (cluAccount::getBooleanConnect()) {
            $account = cluAccount::getAccount();
            $mysql = $account->umysql;
        } else {
            returnNotice(RAUE, 'Вы не авторизированы!', 'red');
            cluServer::stop();
        }
        if (isset($arguments['action'])) {
            if ($arguments['action'] == 'up') {
                $accarray = $account->getArray();
                if ($accarray['id'] == $arguments['id']) {
                    returnNotice('0', 'Вы не можете проголосовать сами за себя!', 'red');
                    cluServer::stop();
                }
                $results = $mysql->prepare("SELECT * FROM karma WHERE karma.id_user = :userid and karma.id_giveuser = :giveuserid LIMIT 1;");
                $results->bindParam(':giveuserid', $accarray['id'], PDO::PARAM_STR);
                $results->bindParam(':userid', $arguments['id'], PDO::PARAM_STR);
                $results->execute();
                $results = $results->fetchAll(PDO::FETCH_ASSOC);
                if ($accarray['karmavotes'] <= 0) {
                    returnNotice('0', 'У вас закончились голоса за карму!', 'red');
                    cluServer::stop();
                } else {
                    $mysqld = $account->umysql;
                    $mysql = null;
                    $mysql = $mysqld->prepare('INSERT INTO `karma` (
                        `id_user` ,
                        `id_giveuser` ,
                        `goloc`
                        )
                        VALUES (
                        :id, :giveuser, \'1\'
                        );');
                    //print_r($arguments);
                    $mysql->bindParam(':id', $arguments['id'], PDO::PARAM_INT);
                    $mysql->bindParam(':giveuser', $accarray['id'], PDO::PARAM_INT);
                    $mysql->execute();
                    $mysql = $mysqld->prepare('UPDATE `users` SET `karma` = `karma` + 1,`karmavotes` = `karmavotes` + 1 WHERE `id` = :giveuser');
                    //print_r($arguments);
                    $mysql->bindParam(':giveuser', $arguments['id'], PDO::PARAM_INT);
                    $mysql->execute();
                    $mysql = $mysqld->prepare('UPDATE `users` SET `karmavotes` = `karmavotes` - 1 WHERE `id` = :userid');
                    $mysql->bindParam(':userid', $accarray['id'], PDO::PARAM_INT);
                    $mysql->execute();
                    returnOK();
                }
            } else if ($arguments['action'] == 'down') {
                $accarray = $account->getArray();
                if ($accarray['id'] == $arguments['id']) {
                    returnNotice('0', 'Вы не можете проголосовать сами за себя!', 'red');
                    cluServer::stop();
                }
                $results = $mysql->prepare("SELECT * FROM karma WHERE karma.id_user = :userid and karma.id_giveuser = :giveuserid LIMIT 1;");
                $results->bindParam(':giveuserid', $accarray['id'], PDO::PARAM_STR);
                $results->bindParam(':userid', $arguments['id'], PDO::PARAM_STR);
                $results->execute();
                $results = $results->fetchAll(PDO::FETCH_ASSOC);
                if ($accarray['karma'] < 0) {
                    returnNotice('0', 'Вы не можете голосовать за карму отрицательно', 'red');
                    cluServer::stop();
                }
                if ($accarray['karmavotes'] <= 0) {
                    returnNotice('0', 'У вас закончились голоса за карму!', 'red');
                    cluServer::stop();
                } else {
                    $mysqld = $account->umysql;
                    $mysql = null;
                    $mysql = $mysqld->prepare('INSERT INTO `karma` (
                        `id_user` ,
                        `id_giveuser` ,
                        `goloc`
                        )
                        VALUES (
                        :id, :giveuser, \'0\'
                        );');
                    //print_r($arguments);
                    $mysql->bindParam(':id', $arguments['id'], PDO::PARAM_INT);
                    $mysql->bindParam(':giveuser', $accarray['id'], PDO::PARAM_INT);
                    $mysql->execute();
                    $mysql = $mysqld->prepare('UPDATE `users` SET `karma` = `karma` - 1 WHERE `id` = :giveuser');
                    $mysql->bindParam(':giveuser', $arguments['id'], PDO::PARAM_INT);
                    $mysql->execute();
                    $mysql = $mysqld->prepare('UPDATE `users` SET `karmavotes` = `karmavotes` - 1 WHERE `id` = :userid');
                    $mysql->bindParam(':userid', $accarray['id'], PDO::PARAM_INT);
                    $mysql->execute();
                    returnOK();
                }
            } else if ($arguments['action'] == 'invite') {
                $accarray = $account->getArray();
                if ($accarray['id'] == $arguments['id']) {
                    returnNotice('0', 'Вы не можете пригласить сами себя!', 'red');
                    cluServer::stop();
                }
                $results = $mysql->prepare("SELECT * FROM users WHERE id = :userid LIMIT 1;");
                $results->bindParam(':userid', $arguments['userid'], PDO::PARAM_STR);
                $results->execute();
                $results = $results->fetchAll(PDO::FETCH_ASSOC);
                if ($account->getBooleanGroup('READONLY')) {
                    returnNotice('0', 'ReadOnly запрещено приглашать', 'red');
                    cluServer::stop();
                }
                if ($accarray['invites'] <= 0) {
                    returnNotice('0', 'У вас нет приглашений!', 'red');
                    cluServer::stop();
                }
                if ($accarray['karma'] < 0) {
                    returnNotice('0', 'У вас отрицательная карма!', 'red');
                    cluServer::stop();
                }
                if ($results[0]['karma'] < 0) {
                    returnNotice('0', 'У человека отрицательная карма!', 'red');
                    cluServer::stop();
                } else {
                    $groups = explode(',', $results[0]['group']);
                    $newgroup = 'POSTS,';
                    if (in_array('POSTS', $groups)) {
                        returnNotice(RGE, 'Этот пользователь уже имеет полноценный аккаунт', 'red');
                        cluServer::stop();
                    } else if (in_array('READONLY', $groups)) {
                        returnNotice(RGE, 'Этот пользователь имеет статус "Только Чтение"', 'red');
                        cluServer::stop();
                    }
                    $newgroup = null;
                    if (!in_array('COMMENT', $groups)) {
                        $newgroup = 'POSTS,COMMENT,';
                    } else
                        $newgroup = 'POSTS,';
                    $mysqld = $account->umysql;
                    $mysql = null;
                    $mysql = $mysqld->prepare('UPDATE `users` SET `invites` = `invites` - 1 WHERE `id` = :userid');
                    $mysql->bindParam(':userid', $accarray['id'], PDO::PARAM_INT);
                    $mysql->execute();
                    $results1 = $mysqld->prepare('UPDATE `users` SET `group` = :groupd WHERE `users`.`id` = :name;');
                    $results1->bindParam(':name', $arguments['userid'], PDO::PARAM_STR);
                    $newgroup .= $results[0]['group'];
                    $results1->bindParam(':groupd', $newgroup, PDO::PARAM_STR);
                    $results1->execute();
                    returnOK();
                }
            }
            else {
                returnNotice('0', 'Ошибка при обработке аргументов! Сообщите администратору (Ошибка 1)', 'red');
                cluServer::stop();
            }
        } else {
            returnNotice('0', 'Ошибка при обработке аргументов! Сообщите администратору (Ошибка 2)', 'red');
            cluServer::stop();
        }
    }
}
