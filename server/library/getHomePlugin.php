<?php
function getHomePlugin($account) {
    //if($account->isGuest())return 'guest';
    if ($account->getBooleanGroup('RESPASS')) {
        return 'vresetpass';
    }
    if ($account->getBooleanGroup('EDITC')) {
        return 'veditc';
    }
    if ($account->getBooleanGroup('ADM')) {
        return 'adminpanel';
    }
    return 'index';
}
