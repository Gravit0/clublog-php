<?php
//Кеширование
const cacheEnable = false; //Включить кеширование
const cacheLib = 'libMemcache'; //Библиотека кеширования
const cacheStdTime = 60; //Время хранения кеша по умолчанию
const cacheUserPrefix = 'sql_users_'; //Префикс пользователя
const cacheMainPrefix = 'site_'; //Главный префикс. Ставится перед всеми остальными
//libMemcache
const cacheMemcacheIP = '127.0.0.1'; //IP адрес Memcached сервера
const cacheMemcachePort = 11211; //Порт Memcached сервера
//База данных
const DBLibrary = 'cluMysql'; // выберите библиотеку базы данных, microCluMysql не поддерживает работу с двумя pdo соединениями сразу, также он может подключать только const mysql
//cluMysql - поддерживает несколько соединений сразу и может подключить иную mysql
const MYSQL_HOST = 'localhost'; //ХОСТ
const MYSQL_DB = 'clublog'; //БАЗА
const MYSQL_LOGIN = 'root'; //ЛОГИН
const MYSQL_PASS = ''; //ПАРОЛЬ
//SENDGRID
const SENDGRID = true; //БЛОКИРУЕТ MAIL() ИЗ ПХП, МЕСТО ЭТОГО ИСПОЛЬЗУЕТСЯ АПИ SENDGRID
const SENDGRID_TEST_VALID = true; //ПРОВЕРЯТЬ РЕЗУЛЬТАТ, ВНИМАНИЕ ИДЕТ ПРОСТОЕ СРАВНЕНИЕ С VALID ОТВЕТОМ, В СЛУЧАЕ ОТКЛЮЧЕНИЯ, ЛЮБОЕ СОЕДИНЕНИЕ СЧИТАЕТСЯ ОК
const SENDGRID_USER_NAME = '';
const SENDGRID_USER_PASS =  '';
const DEFAULT_EMAIL_STR = ''; //EMAIL который будет виден в системе
const DEFAULT_EMAIL_NAME_STR = 'cluBlog'; //название сайта который будет стоять рядом с email отправителя
//CF
