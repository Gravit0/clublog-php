<?php
class adminapi extends Plugin {
    public $library = array(cacheLib);
    public $checkArguments = array('action');
    function run($arguments) {
        $account = null;
        if (cluAccount::getBooleanConnect()) {
            $account = cluAccount::getAccount();
            $cache = initCache();
            if (isset($arguments['action'])) {
                if ($account->getBooleanGroup("MODER")) {
                    if ($arguments['action'] == 'givereadonly') {
                        $mysqld = $account->umysql;
                        $results = indexAction::getUserData($mysqld, $arguments['id'], $cache);
                        //print_r($results);
                        if ($results) {
                            $results1 = $mysqld->prepare('UPDATE `users` SET `group` = :groupd WHERE `users`.`id` = :name;');
                            $results1->bindParam(':name', $arguments['id'], PDO::PARAM_STR);
                            $newgroup = 'READONLY,';
                            $newgroup .= $results[0]['group'];
                            $results1->bindParam(':groupd', $newgroup, PDO::PARAM_STR);
                            $results1->execute();
                            indexAction::updateUser($cache, $arguments['id']);
                            indexAction::adminlog($account, 'Выдал ReadOnly', $arguments['id'], 1);
                            returnOK();
                        } else {
                            returnNotice(RARGF, 'Произошла ошибка при обработке SQL запроса. Результаты отсутствуют<br>Сообщите администратору ' . $arguments['action'], 'red');
                        }
                    } else if ($arguments['action'] == 'unreadonly') {
                        $mysqld = $account->umysql;
                        $results = indexAction::getUserData($mysqld, $arguments['id'], $cache);
                        //print_r($results);
                        if ($results) {
                            $results1 = $mysqld->prepare('UPDATE `users` SET `group` = :groupd WHERE `users`.`id` = :name;');
                            $results1->bindParam(':name', $arguments['id'], PDO::PARAM_STR);
                            $newgroup = explode(',', $results[0]['group']);
                            $newgroups = null;
                            foreach ($newgroup as $value) {
                                if ($value != 'READONLY' && $value != '') {
                                    $newgroups .= ',';
                                    $newgroups .= $value;
                                }
                            }
                            $results1->bindParam(':groupd', $newgroups, PDO::PARAM_STR);
                            $results1->execute();
                            indexAction::updateUser($cache, $arguments['id']);
                            indexAction::adminlog($account, 'Снял ReadOnly', $arguments['id'], 2);
                        }
                        returnOK();
                    } else if ($arguments['action'] == 'giveban') {
                        $mysqld = $account->umysql;
                        $results = indexAction::getUserData($mysqld, $arguments['id'], $cache);
                        $accarray = $account->getArray();
                        //print_r($results);
                        if ($results) {
                            $groups = explode(',', $results[0]['group']);
                            if ($account->getBooleanGroup("SUPERUSER") == false) {
                                if ((in_array('ADM', $groups) || in_array('MODER', $groups)) && ( $account->getBooleanGroup("ADM") == false )) {
                                    returnNotice(RGE, 'Модератор не может забанить администратора или модератора', 'red');
                                    cluServer::stop();
                                }
                                if (in_array('SUPERUSER', $groups)) {
                                    returnNotice(RGE, 'Банить суперпользователя запрещено', 'red');
                                    cluServer::stop();
                                }
                            }
                            if (( $results[0]['id'] == $accarray['id'])) {
                                returnNotice(RGE, 'Зачем Вы это делаете?', 'blue');
                                cluServer::stop();
                            }
                            $mysql = $mysqld->prepare("INSERT INTO `banned` (
                                    `id_user` ,
                                    `id_moder` ,
                                    `reason` ,
                                    `permanet`
                                    )
                                    VALUES (
                                    :id, :idmoder, :reason, '1'
                                    );");
                            $mysql->bindParam(':idmoder', $account->getArray()['id'], PDO::PARAM_INT);
                            $mysql->bindParam(':id', $arguments['id'], PDO::PARAM_INT);
                            $mysql->bindParam(':reason', $arguments['reason'], PDO::PARAM_STR);
                            $mysql->execute();
                            $lastId = $mysqld->lastInsertId();
                            $results1 = $mysqld->prepare('UPDATE `users` SET `banid` = :banid WHERE `users`.`id` = :name;');
                            $results1->bindParam(':name', $arguments['id'], PDO::PARAM_STR);
                            $results1->bindParam(':banid', $lastId, PDO::PARAM_STR);
                            $results1->execute();
                            indexAction::updateUser($cache, $arguments['id']);
                            indexAction::adminlog($account, 'Забанил', $arguments['id'], 3);
                        }
                        returnOK();
                    } else if ($arguments['action'] == 'unban') {
                        $mysqld = $account->umysql;
                        $results = indexAction::getUserData($mysqld, $arguments['id'], $cache);
                        //print_r($results);
                        if ($results) {
                            $results1 = $mysqld->prepare('UPDATE `users` SET `banid` = 0 WHERE `users`.`id` = :name;');
                            $results1->bindParam(':name', $arguments['id'], PDO::PARAM_STR);
                            $results1->execute();
                            indexAction::updateUser($cache, $arguments['id']);
                            indexAction::adminlog($account, 'Снял бан', $arguments['id'], 4);
                        }
                        returnOK();
                    } else {
                        //returnNotice(RARGF,'Произошла ошибка при обработке аргументов<br>Сообщите администратору '.$arguments['action'],'red');
                    }
                }
                if ($account->getBooleanGroup("ADM") || $account->getBooleanGroup("SUPERUSER")) {
                    if ($arguments['action'] == 'giveinvites') {
                        $mysqld = $account->umysql;
                        $results = indexAction::getUserData($mysqld, $arguments['id'], $cache);
                        //print_r($results);
                        if ($results) {
                            $results1 = $mysqld->prepare('UPDATE `users` SET `invites` = `invites` + 1 WHERE `users`.`id` = :name;');
                            $results1->bindParam(':name', $arguments['id'], PDO::PARAM_STR);
                            $results1->execute();
                            indexAction::updateUser($cache, $arguments['id']);
                            indexAction::adminlog($account, 'Выдал 1 приглашений', $arguments['id'], 5);
                        }
                        returnOK();
                    } else if ($arguments['action'] == 'giveadmin' && $account->getBooleanGroup("SUPERUSER")) {
                        $mysqld = $account->umysql;
                        $results = indexAction::getUserData($mysqld, $arguments['id'], $cache);
                        //print_r($results);
                        if ($results) {
                            $groups = explode(',', $results[0]['group']);
                            if (in_array('ADM', $groups)) {
                                returnNotice(RGE, 'Этот пользователь уже имеет права модератора', 'red');
                                cluServer::stop();
                            }
                            $results1 = $mysqld->prepare('UPDATE `users` SET `group` = :groupd WHERE `users`.`id` = :name;');
                            $results1->bindParam(':name', $arguments['id'], PDO::PARAM_STR);
                            $newgroup = 'ADM,';
                            $newgroup .= $results[0]['group'];
                            $results1->bindParam(':groupd', $newgroup, PDO::PARAM_STR);
                            $results1->execute();
                            indexAction::updateUser($cache, $arguments['id']);
                            indexAction::adminlog($account, 'Выдал администратора', $arguments['id'], 6);
                        }
                        returnOK();
                    } else if ($arguments['action'] == 'clearcache') {
                        if ($cache) {
                            $cache->clearall();
                            returnOK();
                        } else {
                            returnNotice(RGE, 'Не удалось инициализировать библиотеку кеширования', 'red');
                            cluServer::stop();
                        }
                    } else if ($arguments['action'] == 'givemoder') {
                        $mysqld = $account->umysql;
                        $results = indexAction::getUserData($mysqld, $arguments['id'], $cache);
                        //print_r($results);
                        if ($results) {
                            $groups = explode(',', $results[0]['group']);
                            if (in_array('MODER', $groups)) {
                                returnNotice(RGE, 'Этот пользователь уже имеет права модератора', 'red');
                                cluServer::stop();
                            }
                            $results1 = $mysqld->prepare('UPDATE `users` SET `group` = :groupd WHERE `users`.`id` = :name;');
                            $results1->bindParam(':name', $arguments['id'], PDO::PARAM_STR);
                            $newgroup = 'MODER,';
                            $newgroup .= $results[0]['group'];
                            $results1->bindParam(':groupd', $newgroup, PDO::PARAM_STR);
                            $results1->execute();
                            indexAction::updateUser($cache, $arguments['id']);
                            indexAction::adminlog($account, 'Выдал модератора', $arguments['id'], 7);
                        }
                        returnOK();
                    } else if ($arguments['action'] == 'givecomment') {
                        $mysqld = $account->umysql;
                        $results = indexAction::getUserData($mysqld, $arguments['id'], $cache);
                        //print_r($results);
                        if ($results) {
                            $groups = explode(',', $results[0]['group']);
                            if (in_array('COMMENT', $groups)) {
                                returnNotice(RGE, 'Этот пользователь уже имеет права комментатора', 'red');
                                cluServer::stop();
                            }
                            $results1 = $mysqld->prepare('UPDATE `users` SET `group` = :groupd WHERE `users`.`id` = :name;');
                            $results1->bindParam(':name', $arguments['id'], PDO::PARAM_STR);
                            $newgroup = 'COMMENT,';
                            $newgroup .= $results[0]['group'];
                            $results1->bindParam(':groupd', $newgroup, PDO::PARAM_STR);
                            $results1->execute();
                            indexAction::updateUser($cache, $arguments['id']);
                            indexAction::adminlog($account, 'Выдал Комментатора', $arguments['id'], 8);
                        }
                        returnOK();
                    } else if ($arguments['action'] == 'ainvite') {
                        $mysqld = $account->umysql;
                        $results = indexAction::getUserData($mysqld, $arguments['id'], $cache);
                        //print_r($results);
                        if ($results) {
                            $groups = explode(',', $results[0]['group']);
                            if (in_array('POSTS', $groups)) {
                                returnNotice(RGE, 'Этот пользователь уже имеет полноценный аккаунт', 'red');
                                cluServer::stop();
                            }
                            $results1 = $mysqld->prepare('UPDATE `users` SET `group` = :groupd WHERE `users`.`id` = :name;');
                            $results1->bindParam(':name', $arguments['id'], PDO::PARAM_STR);
                            $newgroup = 'POSTS,';
                            $newgroup .= $results[0]['group'];
                            $results1->bindParam(':groupd', $newgroup, PDO::PARAM_STR);
                            $results1->execute();
                            indexAction::updateUser($cache, $arguments['id']);
                            indexAction::adminlog($account, 'Выдал приглашение', $arguments['id'], 9);
                        }
                        returnOK();
                    } else if ($arguments['action'] == 'uncomment') {
                        $mysqld = $account->umysql;
                        $accarray = $account->getArray();
                        $results = indexAction::getUserData($mysqld, $arguments['id'], $cache);
                        //print_r($results);
                        if ($results) {
                            $results1 = $mysqld->prepare('UPDATE `users` SET `group` = :groupd WHERE `users`.`id` = :name;');
                            $results1->bindParam(':name', $arguments['id'], PDO::PARAM_STR);
                            $newgroup = explode(',', $results[0]['group']);
                            $newgroups = null;
                            if (in_array('SUPERUSER', $newgroup) && ($account->getBooleanGroup("SUPERUSER") == false)) {
                                returnNotice(RGE, 'Снять права с суперпользователя запрещено', 'red');
                                cluServer::stop();
                            }
                            if (( $results[0]['id'] == $accarray['id'])) {
                                returnNotice(RGE, 'Зачем Вы это делаете?', 'blue');
                                cluServer::stop();
                            }
                            foreach ($newgroup as $value) {
                                if ($value != 'COMMENT' && $value != '') {
                                    $newgroups .= ',';
                                    $newgroups .= $value;
                                }
                            }
                            $results1->bindParam(':groupd', $newgroups, PDO::PARAM_STR);
                            $results1->execute();
                            indexAction::updateUser($cache, $arguments['id']);
                            indexAction::adminlog($account, 'Снял комментатора', $arguments['id'], 10);
                        }
                        returnOK();
                    } else if ($arguments['action'] == 'unmoder') {
                        $mysqld = $account->umysql;
                        $accarray = $account->getArray();
                        $results = indexAction::getUserData($mysqld, $arguments['id'], $cache);
                        //print_r($results);
                        if ($results) {
                            $results1 = $mysqld->prepare('UPDATE `users` SET `group` = :groupd WHERE `users`.`id` = :name;');
                            $results1->bindParam(':name', $arguments['id'], PDO::PARAM_STR);
                            $newgroup = explode(',', $results[0]['group']);
                            $newgroups = null;
                            if (in_array('SUPERUSER', $newgroup) && ($account->getBooleanGroup("SUPERUSER") == false)) {
                                returnNotice(RGE, 'Снять права с суперпользователя запрещено', 'red');
                                cluServer::stop();
                            }
                            if (( $results[0]['id'] == $accarray['id'])) {
                                returnNotice(RGE, 'Зачем Вы это делаете?', 'blue');
                                cluServer::stop();
                            }
                            foreach ($newgroup as $value) {
                                if ($value != 'MODER' && $value != '') {
                                    $newgroups .= ',';
                                    $newgroups .= $value;
                                }
                            }
                            $results1->bindParam(':groupd', $newgroups, PDO::PARAM_STR);
                            $results1->execute();
                            indexAction::updateUser($cache, $arguments['id']);
                            indexAction::adminlog($account, 'Снял модератора', $arguments['id'], 11);
                        }
                        returnOK();
                    } else if ($arguments['action'] == 'offadmin' && $account->getBooleanGroup("SUPERUSER")) {
                        $mysqld = $account->umysql;
                        $accarray = $account->getArray();
                        $results = indexAction::getUserData($mysqld, $arguments['id'], $cache);
                        //print_r($results);
                        if ($results) {
                            $results1 = $mysqld->prepare('UPDATE `users` SET `group` = :groupd WHERE `users`.`id` = :name;');
                            $results1->bindParam(':name', $arguments['id'], PDO::PARAM_STR);
                            $newgroup = explode(',', $results[0]['group']);
                            $newgroups = null;
                            if (( $results[0]['id'] == $accarray['id'])) {
                                returnNotice(RGE, 'Зачем Вы это делаете?', 'blue');
                                cluServer::stop();
                            }
                            foreach ($newgroup as $value) {
                                if ($value != 'ADM' && $value != '') {
                                    $newgroups .= ',';
                                    $newgroups .= $value;
                                }
                            }
                            $results1->bindParam(':groupd', $newgroups, PDO::PARAM_STR);
                            $results1->execute();
                            indexAction::updateUser($cache, $arguments['id']);
                            indexAction::adminlog($account, 'Снял администратора', $arguments['id'], 12);
                        }
                        returnOK();
                    } else {
                        //returnNotice(RARGF,'Произошла ошибка при обработке аргументов<br>Сообщите администратору '.$arguments['action'],'red');
                    }
                } else
                    returnNotice(RGE, 'У вас недостаточно прав для выполнения данной комманды', 'red');
            }
            else {
                returnNotice(RARGF, 'Произошла ошибка при обработке аргументов<br>Сообщите администратору', 'red');
            }
        } else {
            returnNotice(RGE, 'У вас недостаточно прав для выполнения данной комманды', 'red');
        }
    }
}
