<?php
class auth extends Plugin {
    public $library = array('cluAccount');
    function run($arguments) {
        $returnOK = true;
        if (isset($arguments['command'])) {
            if ($arguments['command'] == 'exit') {
                if (cluAccount::getBooleanConnect()) {
                    $account = cluAccount::getAccount();
                    if ($account) {
                        $account->closeMyConnect();
                        returnOK();
                        cluServer::stop();
                    }
                }
            } else
            if ($arguments['command'] == 'auth') {
                if (isset($arguments['login']) == false || isset($arguments['pass']) == false || !$arguments['login'] || !$arguments['pass']) {
                    returnNotice('0', 'Заполните все поля.', 'red');
                    cluServer::stop();
                } else {
                    $browser = getUserBrowser();
                    try {
                        $account = cluAccount::createAuthAccount(0, $arguments['login'], $arguments['pass'], $browser[0], $browser[1]);
                    } catch (Exception $e) {
                        $message = $e->getMessage();
                        if ($message == RBU) {
                            returnNotice('0', 'Вы забанены на этом сервере', 'red');
                            cluServer::stop();
                        } else {
                            echo 'Exception: ' . $message;
                        }
                    }
                    if ($account) {
                        returnOK();
                        cluServer::stop();
                    }
                    $returnOK = false;
                    returnNotice('0', 'Неверный логин или пароль', 'red');
                    cluServer::stop();
                }
            }
        }
        returnNotice('0', 'Ошибка аргументов', 'red');
        if (cluAccount::getBooleanConnect() == false) {
            echo Version . '(' . VersionData . ')';
            echo '</br>';
            echo 'This Guest';
            /////////////
            ///////////
        } else {
            $account = cluAccount::getAccount();
            cluServer::loadLibrary('getHomePlugin');
            $home = getHomePlugin($account);
            if ($arguments['value']) {
                createHeaderLocation($home, array('value' => $arguments['value']));
            } else {
                createHeaderLocation($home);
            }
        }
    }
}
